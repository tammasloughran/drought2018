#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 16:44:06 2019

@author: tammas
"""

import netCDF4 as nc
import matplotlib.pyplot as plt
import glob
import numpy as np

outputDir = '/scratch/m/m300719/drought2018/mpiesm-landveg/experiments/spinup_T255/outdata/jsbach'
files = glob.glob(outputDir+'/1900-1909_land.nc')
ncin = nc.MFDataset(files,'r')
time = ncin.variables['time'][:]
dates = nc.num2date(time,ncin.variables['time'].units)


data = ncin.variables['surface_temperature'][:,0,0]
plt.plot(dates,data - 273.15)
plt.xlabel('Time')
plt.ylabel('Surface temperature (°C)')
#plt.savefig('temp_series.png')
plt.show()


data = ncin.variables['transpiration'][:,0,0]
plt.plot(dates,data)#*10**6)
#infile = '/work/mj0060/m300316/TRENDY/TRENDY_v7/post_processing/JSBACH_S3/outdata_land/aggregated/JSBACH_S3_evapotrans.land_mean.nc'
#ncin2 = nc.Dataset(infile)
#data2 = ncin2.variables['evapotrans'][:,0,0]
#plt.plot(dates[0:120:12],data2[0:10]*10**6)
plt.xlabel('Time')
plt.ylabel('Transpiration (kg/m^2s)')# ×10^-6)')
#plt.savefig('trans_series.png')
plt.show()

data = ncin.variables['soil_moisture'][:,0,0]
plt.plot(dates,data)
plt.xlabel('Time')
plt.ylabel('Soil moisture (m)')
#plt.savefig('soil_series.png')
plt.show()

data = ncin.variables['lai'][:,0,0]
plt.plot(dates,data)
plt.xlabel('Time')
plt.ylabel('LAI')
#plt.savefig('lai_series.png')
plt.show()

files = glob.glob(outputDir+'/1900-1909_veg.nc')
ncin = nc.MFDataset(files,'r')

#print(ncin.variables['box_NPP_yDayMean'].shape)
data = ncin.variables['box_NPP_yDayMean'][:,3,0,0]
plt.plot(dates,data)
plt.xlabel('Time')
plt.ylabel('Extratropical Deciduous NPP')
#plt.savefig('npp_series.png')
plt.show()

ncin = nc.MFDataset(outputDir+'/spinup_T255_jsbach_land_190?.nc','r')
data = ncin.variables['surface_temperature'][...].mean(axis=0)
plt.pcolormesh(np.flipud(data - 273.15))
plt.title('Spinup Surface Temperature')
plt.colorbar()
#plt.savefig('temp_map.png')
plt.show()

data = ncin.variables['transpiration'][...].mean(axis=0)
plt.pcolormesh(np.flipud(data))
plt.title('Spinup Transpiration')
plt.colorbar()
#plt.savefig('trand_map.png')
plt.show()

data = ncin.variables['soil_moisture'][...].mean(axis=0)
plt.title('Spinup Soil Moisture')
plt.pcolormesh(np.flipud(data))
plt.colorbar()
#plt.savefig('soil_map.png')
plt.show()

ncin = nc.MFDataset(outputDir+'/spinup_T255_jsbach_veg_190?.nc','r')
data = ncin.variables['box_NPP_yDayMean'][:,3,...].mean(axis=0)
plt.title('Spinup box_NPP_yDayMean')
plt.pcolormesh(np.flipud(data))
plt.colorbar()
plt.title('NPP')
#plt.savefig('soil_map.png')
plt.show()


trendy = '/work/bm0891/m300719/TRENDY/TRENDY_v7/experiments/S3_fstnf_LUH2_crujra_v1_1_TRENDY_v7_rev9525/outdata/jsbach'
files = nc.MFDataset(trendy+'/S3_fstnf_LUH2_crujra_v1_1_TRENDY_v7_rev9525_jsbach_veg_170???.nc','r')
data = files.variables['box_NPP_yDayMean'][:,3,...].mean(axis=0)
plt.pcolormesh(np.flipud(data))
plt.title('TRENDYv7 NPP')
plt.colorbar()
#plt.savefig('trendy_npp_map.png')
plt.show()

#data = files.variables['box_NPP_yDayMean'][:,3,...].mean(axis=0)
#plt.pcolormesh(np.flipud(data))
#plt.colorbar()
#plt.show()

files = nc.MFDataset(trendy+'/S3_fstnf_LUH2_crujra_v1_1_TRENDY_v7_rev9525_jsbach_land_170???.nc','r')
data = files.variables['surface_temperature'][...].mean(axis=0)
plt.title('TRENDYv7 Surface temperature')
plt.pcolormesh(np.flipud(data-273.15))
plt.colorbar()
#plt.savefig('trendy_temp_map.png')
plt.show()

data = files.variables['transpiration'][...].mean(axis=0)
plt.pcolormesh(np.flipud(data))
plt.title('TRENDYv7 transpiration')
plt.colorbar()
#plt.savefig('trendy_trans_map.png')
plt.show()

data = files.variables['soil_moisture'][...].mean(axis=0)
plt.title('TRENDYv7 soil moisture')
plt.pcolormesh(np.flipud(data))
plt.colorbar()
#plt.savefig('trendy_soil_map.png')
plt.show()