#!/bin/bash

# bash script to get landCoverFract per year

tag=JSBACH_S4
cd /work/mj0060/m300316/TRENDY/TRENDY_v7/post_processing/${tag}/outdata_land/
inf_main=../outdata/main.grb
inf_ini=../outdata/jsbach.nc
var=landCoverFrac
[[ ${echam_fractional} != yes ]] && slf=slm

currentDate=$(date +'%m/%d/%Y')
branch="mpiesm-landveg"
revision=9525
TRENDY_version='v7'
Email='julia.nabel@mpimet.mpg.de'
scriptName=visualise_all.ksh

###########-----------------------------------------------------------------------------------

cdo selcode,12        ${inf_main} cover_fract.tmp
cdo selvar,veg_ratio_max  ${inf_ini} veg_ratio_max.tmp
cdo mul veg_ratio_max.tmp cover_fract.tmp  box_cover_fract.tmp

cdo selvar,slm        ${inf_ini}  slm
[[ ${echam_fractional} != yes ]] && slf=slm
cdo invertlat -mul ${slf} -gridarea ${slf} gridareafrac.nc
cdo selvar,cover_type ${inf_ini}  cover_type


cdo -f nc mul -gtc,5.e-5 cover_fract.tmp     cover_fract.tmp      cover_fract
cdo -f nc mul -gtc,5.e-5 veg_ratio_max.tmp   veg_ratio_max.tmp    veg_ratio_max
cdo -f nc mul -gtc,5.e-5 box_cover_fract.tmp box_cover_fract.tmp  box_cover_fract


cdo eqc,1 -sellevel,1 cover_type glac

cdo sub -addc,1 -mulc,-1 veg_ratio_max  glac  bare
cdo sub slm glac land

cdo subc,20 -sellevel,11 cover_type C3C4_crop_mask

cdo -f nc splitlevel -mulc,100 box_cover_fract lct_
cdo setlevel,1 -setvar,var12             -mulc,100. bare bare.tmp
cdo setlevel,2 -setvar,var12 -ifthen slm -mulc,100. glac glac.tmp
cdo setlevel,3  lct_000001.nc                     tropev.tmp
cdo setlevel,4  lct_000002.nc                     tropdec.tmp
cdo setlevel,5  lct_000003.nc                     extrev.tmp
cdo setlevel,6  lct_000004.nc                     extrdec.tmp
cdo setlevel,7  lct_000005.nc                     rgshrub.tmp
cdo setlevel,8  lct_000006.nc                     decshrub.tmp
cdo setlevel,9  lct_000007.nc                     c3grass.tmp 
cdo setlevel,10 lct_000008.nc                     c4grass.tmp
cdo setlevel,11 lct_000009.nc                     c3past.tmp
cdo setlevel,12 lct_000010.nc                     c4past.tmp
cdo setlevel,14 -mul C3C4_crop_mask lct_000011.nc c4crop.tmp
cdo setlevel,13 -sub lct_000011.nc  c4crop.tmp    c3crop.tmp

cdo -mulc,0 tropev.tmp template_with_time_all_zero.tmp
cdo setlevel,2 -add glac.tmp template_with_time_all_zero.tmp glac_with_time.tmp

cdo div -addc,1 glac_with_time.tmp -addc,1 glac_with_time.tmp template_with_time_all_one.tmp
cdo setlevel,1 -add bare.tmp template_with_time_all_zero.tmp bare_with_time_1.tmp
cdo -mul bare_with_time_1.tmp template_with_time_all_one.tmp bare_with_time.tmp

[[ ! -f landCoverFrac.tmp ]] || rm landCoverFrac.tmp
cdo merge bare_with_time.tmp    glac_with_time.tmp     \
          tropev.tmp  tropdec.tmp  \
          extrev.tmp  extrdec.tmp  \
          rgshrub.tmp decshrub.tmp \
          c3grass.tmp c4grass.tmp  \
          c3past.tmp  c4past.tmp   \
          c3crop.tmp  c4crop.tmp     landCoverFrac.tmp1
#cdo setlevel,1  landCoverFrac.tmp    landCoverFrac.tmp1   # sets the first level to 1 (had been 0)
cdo ifthen slm  landCoverFrac.tmp1  landCoverFrac.tmp2
ncrename -h -O -d lev,PFT -v lev,PFT landCoverFrac.tmp2 landCoverFrac.tmp
echo '&parameter name=landCoverFrac long_name="Plant Functional Type Grid Fraction" units="%" /' > landCoverFrac.partab

cdo divc,100. landCoverFrac.tmp landCoverFrac_trendy.tmp
mv landCoverFrac_trendy.tmp landCoverFrac.tmp
echo '&parameter name=landCoverFrac long_name="Plant Functional Type Grid Fraction" units="1" /' > landCoverFrac.partab

cdo -f nc -r setpartabn,${var}.partab -invertlat -setvar,${var} -yearmean ${var}.tmp ${var}.nc


ncatted -h -O -a comment,global,a,c,"Land cover types:" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT1:  Bare land" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT2:  Glacier" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT3:  Tropical evergreen trees" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT4:  Tropical deciduous trees" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT5:  Extra-tropical evergreen trees" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT6:  Extra-tropical deciduous trees" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT7:  Raingreen shrubs" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT8:  Deciduous shrubs" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT9:  C3 grass" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT10: C4 grass" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT11: C3 pasture" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT12: C4 pasture" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT13: C3 Crops" ${var}.nc
ncatted -h -O -a comment,global,a,c,"\nPFT14: C4 Crops" ${var}.nc

# clean up and set some more global attributes
ncatted -O -a institution,global,o,c,"Max Planck Institute for Meteorology" ${var}.nc -h
ncatted -O -a contact,global,o,c,"${Email}" ${var}.nc -h
# replace the history record
ncatted -O -a history,global,o,c,"${currentDate}: JSBACH (${branch} r${revision}) TRENDY${TRENDY_version} runs post-processed with ${scriptName}" ${var}.nc -h
  
cdo mul ${var}.nc -invertlat ${slf} ${var}.slf
echo "&parameter name=$var" 'units="Mio km^2" /' > ${var}.partab
cdo setpartabn,${var}.partab -mulc,5.100656 -fldmean -setmisstoc,0 -yearmonmean ${var}.slf ${tag}_${var}.global_area.nc

mv ${var}.nc ${tag}_${var}.nc

rm *.tmp cover_fract veg_ratio_max box_cover_fract slm gridareafrac.nc cover_type land glac C3C4_crop_mask bare lct_0000??.nc landCoverFrac.tmp1 landCoverFrac.tmp2 
rm landCoverFrac.slf landCoverFrac.partab 
