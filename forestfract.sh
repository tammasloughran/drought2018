#!/bin/ksh
#------------------------------------------------------------------------------
# Calculate cover fractions of several different vegetation types
# This Script only works for the jsbach CMIP5 setup with 11 tiles and 21 lcts:
#
#     tile 1:      PFT 1 (glacier) 
#              and PFT 2 (tropical broadleaf evergreen trees)
#     tile 2:      PFT 3 (tropical broadleaf deciduous trees)
#     tile 3:      PFT 4 (extra-tropical evergreen trees)
#     tile 4:      PFT 5 (extra-tropical deciduous trees)
#     tile 5:      PFT 10 (raingreen shrubs)
#     tile 6:      PFT 11 (deciduous shrubs)
#     tile 7:      PFT 12 (C3 grass)
#     tile 8:      PFT 13 (C4 grass)
#     tile 9:      PFT 15 (C3 pasture)
#     tile 10:     PFT 16 (C4 pasture)
#     tile 11:     PFT 20 (C3 crops)
#              and PFT 21(C4 crops)
#
# based on scripts by Reiner Schnur and Thomas Raddatz
# Veronika Gayler, September 2010
#------------------------------------------------------------------------------
set -e

inf_main=$1    # selected codes from monthly mean jsbach main stream
               #   code 20: veg_ratio_max
               #   code 10: cover_fract_pot
               #   code 12: cover_fract
inf_ini=$2     # jsbach initial file
               #   variable cover_type
if [[ $3 != "" ]]; then
  echam_fractional=yes
  olf=$3       # if echam uses fractional land sea mask: fractional ocean land mask
fi

# find out number of tiles. This section only works with 8 or 11 tiles.

ntiles=$(cdo -s nlevel -selzaxis,3 ${inf_ini} | head -1)
if [[ ${ntiles} != 11 && ${ntiles} != 8 ]]; then
  echo "forestfrac.sh: This script only works with 8 or 11 tiles"
  exit 1
fi

# preparations

cdo selvar,slm   ${inf_ini}  slm
cdo selcode,20   ${inf_main} veg_ratio_max
cdo selcode,12   ${inf_main} cover_fract

# grid cell fractions of forest, shrub, grass, crop and pasture

if [[ ${echam_fractional} = yes ]]; then
  maskstring="mul ${olf}"
else
  maskstring="ifthen slm"
fi
cdo -f nc ${maskstring} -mul veg_ratio_max -vertsum -sellevel,1/4  cover_fract forest
cdo -f nc ${maskstring} -mul veg_ratio_max -vertsum -sellevel,5/6  cover_fract shrub
cdo -f nc ${maskstring} -addc,1. -mulc,-1. veg_ratio_max desert
cdo -f nc ${maskstring} -mul veg_ratio_max -vertsum -sellevel,7/8  cover_fract grass
if [[ ${ntiles} = 11 ]]; then
  cdo -f nc ${maskstring} -mul veg_ratio_max -vertsum -sellevel,9/10 cover_fract pasture
  cdo -f nc ${maskstring} -mul veg_ratio_max -vertsum -sellevel,11   cover_fract crop
fi

# generate ascii output for time series

cdo output -mulc,510. -fldmean -setmisstoc,0. forest > forest.asc
cdo output -mulc,510. -fldmean -setmisstoc,0. shrub  > shrub.asc
cdo output -mulc,510. -fldmean -setmisstoc,0. grass  > grass.asc
cdo output -mulc,510. -fldmean -setmisstoc,0. desert > desert.asc
if [[ ${ntiles} = 11 ]]; then
  cdo output -mulc,510. -fldmean -setmisstoc,0. crop    > crop.asc
  cdo output -mulc,510. -fldmean -setmisstoc,0. pasture > pasture.asc
fi
rm slm veg_ratio_max cover_fract forest shrub grass desert
rm -f crop pasture
