#!/bin/ksh
#------------------------------------------------------------------------------
# CMOR-like postprocessing of monthly jsbach output
#
#    the output files are not exactly cmor complient but have cmor variable 
#    names and units.
#------------------------------------------------------------------------------
# This Script only works for the jsbach CMIP6 setup with 11 tiles and 21 lcts:
#
#     tile 1:      PFT 1 (glacier) 
#              and PFT 2 (tropical broadleaf evergreen trees)
#     tile 2:      PFT 3 (tropical broadleaf deciduous trees)
#     tile 3:      PFT 4 (extra-tropical evergreen trees)
#     tile 4:      PFT 5 (extra-tropical deciduous trees)
#     tile 5:      PFT 10 (raingreen shrubs)
#     tile 6:      PFT 11 (deciduous shrubs)
#     tile 7:      PFT 12 (C3 grass)
#     tile 8:      PFT 13 (C4 grass)
#     tile 9:      PFT 15 (C3 pasture)
#     tile 10:     PFT 16 (C4 pasture)
#     tile 11:     PFT 20 (C3 crops)
#              and PFT 21(C4 crops)
#
# Veronika Gayler, February 2018
#------------------------------------------------------------------------------
set -e
expid=$1       # experiment id

inf_ini=$2     # jsbach initial file
               #   cover_type
               #   lsm
               #   soil_depth

inf_veg=$3     # selected codes from jsbach veg stream (monthly means)
               #   code  50  box_burned_frac_diag_avg  [m2/m2(grid box)]    (with spitfire)
               #   code  53  box_burned_frac_avg       [m2/m2(grid box)]    (without spitfire)
               #   code  56  box_fire_CO2_flux_2_atmos [kg(CO2) m-2(grid box) s-1]
               #   code  62  dist_nitrogen_2_atmos     [mol(N) m-2(grid box)]   per time step! i.e. per day
               #   code  82  LCC_flux_box_C2atmos      [mol(C) m-2(grid box) s-1]
               #   code 143  box_Cflux_herbivory_2_atm [mol(C) m-2(grid box) s-1]
               #   code 158  box_Cflx_2_crop_harvest   [mol(C) m-2(grid box) s-1]
               #   code 160  boxC_green           [mol(C)/m2(grid box)]
               #   code 161  boxC_woods           [mol(C)/m2(grid box)]
               #   code 162  boxC_reserve         [mol(C)/m2(grid box)]
               #   code 169  alpha_yDayMean       []
               #   code 170  box_soil_respiration [mol(C)/m2(grid box) s]   ! including crop harvest flux to the atmosphere
               #   code 173  box_GPP_yDayMean     [mol(C)/m2(grid box) s]
               #   code 176  box_Cpools_total     [mol(C)/m2(grid box)]
               #   code 178  box_NPP_act_yDayMean [mol(C)/m2(grid box) s]
               #   code 211  box_Cflx_crop_harvest_2_atm    [mol(C) m-2(grid box) s-1]  
               #   code 212  redFact_Nlimitation  []
               #   code 218  box_Cpool_onSite_avg_LCC       [mol(C) m-2(grid box)]
               #   code 220  box_Cpool_paper_avg_LCC        [mol(C) m-2(grid_box)]
               #   code 221  box_Cpool_construction_avg_LCC [mol(C) m-2(grid box)]
               #   code 222  box_Cpool_paper_harvest_avg    [mol(C) m-2(grid box)]
               #   code 223  box_Cpool_construction_harvest_avg  [mol(C) m-2(grid box)]
               #   code 224  box_Cpool_onSite_harvest_avg   [mol(C) m-2(grid box)]
               #   code 225  boxC_crop_harvest              [mol(C) m-2(grid box)]
               #   code 228  boxC_flux_2_onSite_LCC         [mol(C)/m2(grid box) s]
               #   code 230  boxC_flux_2_paper_LCC          [mol(C)/m2(grid box) s]
               #   code 231  boxC_flux_2_construction_LCC   [mol(C)/m2(grid box) s]
               #   code 236  boxC_flux_onSite_2_atmos_LCC   [mol(C) m-2(grid box) s-1]
               #   code 238  boxC_flux_paper_2_atmos_LCC    [mol(C) m-2(grid box) s-1]
               #   code 239  boxC_flux_construction_2_atmos_LCC  [mol(C) m-2(grid box) s-1]
               #   code 242  boxC_flux_2_paper_harvest           [mol(C) m-2(grid box) s-1]
               #   code 243  boxC_flux_2_construction_harvest    [mol(C) m-2(grid box) s-1]
               #   code 246  boxC_flux_paper_2_atmos_harvest     [mol(C) m-2(grid box) s-1]
               #   code 247  boxC_flux_construction_2_atmos_harvest  [mol(C) m-2(grid box) s-1]
               #   code 249  boxC_flux_2_onSite_harvest          [mol(C)m-2(grid box)s-1]
               #   code 251  boxC_flux_onsite_2_atmos_harvest    [mol(C)m-2(grid box)s-1]

inf_main=$4    # selected codes jsbach main stream (monthly means)
               #   code  10  cover_fract_pot      []
               #   code  12  cover_fract          []
               #   code  20  veg_ratio_max        []
               #   code  21  swdown_acc           [W m-2]
               #   code  22  swdown_reflect_acc   [W m-2]
               #   code  24  box_veg_ratio        []
               #   code  33  swdown_vis_acc       [W m-2]
               #   code  34  swdown_nir_acc       [W m-2]
               #   code  35  sw_vis_absorb_nosno_acc  [W m-2]
               #   code  36  sw_nir_absorb_nosno_acc  [W m-2]
               #   code  84  layer_moisture       [m]
               #   code 107  lai                  []
               #   code 162  CO2_emission_landcover_change [mol(CO2) m-2(grid box) s-1]
               #   code 163  CO2_emission_harvest [mol(CO2) m-2(grid box) s-1]

inf_land=$5    # selected codes jsbach land stream (monthly means)
               #   code  35  surface_temperature    [K]
               #   code  36  surface_radiative_temp  [K]
               #   code  44  evapotranspiration   [kg/m^2s]
               #   code  47  heat_flx    [W/m^2]
               #   code  49  latent_heat_flx      [W/m^2]
               #   code  60  snow_fract           []
               #   code  63  runoff               [kg/m^2s]
               #   code  68  soil_temperature     [K]
               #   code  76  transpiration        [kg/m2s]
               #   code  79  bare_soil_evaporation  [kg/m2s]
               #   code  80  snow_evaporation     [kg/m^2s]
               #   code 107  lai                  []
               #   code 109  snow_depth_canopy    [m]
               #   code 148  apar_acc             [mol(PHOTONS) m-2(grid box) s-1]
               #   code 149  par_acc              [mol(PHOTONS) m-2(grid box) s-1]

inf_yasso=$6   # selected codes from jsbach yasso stream
               #   code  31  boxYC_acid_ag1       [mol(C) m-2(grid box)]
               #   code  32  boxYC_acid_bg1       [mol(C) m-2(grid box)]
               #   code  33  boxYC_water_ag1      [mol(C) m-2(grid box)]
               #   code  34  boxYC_water_bg1      [mol(C) m-2(grid box)]
               #   code  35  boxYC_ethanol_ag1    [mol(C) m-2(grid box)]
               #   code  36  boxYC_ethanol_bg1    [mol(C) m-2(grid box)]
               #   code  37  boxYC_nonsoluble_ag1 [mol(C) m-2(grid box)]
               #   code  38  boxYC_nonsoluble_bg1 [mol(C) m-2(grid box)]
               #   code  39  boxYC_humus_1        [mol(C) m-2(grid box)]
               #   code  41  boxYC_acid_ag2       [mol(C) m-2(grid box)]
               #   code  42  boxYC_acid_bg2       [mol(C) m-2(grid box)]
               #   code  43  boxYC_water_ag2      [mol(C) m-2(grid box)]
               #   code  44  boxYC_water_bg2      [mol(C) m-2(grid box)]
               #   code  45  boxYC_ethanol_ag2    [mol(C) m-2(grid box)]
               #   code  46  boxYC_ethanol_bg2    [mol(C) m-2(grid box)]
               #   code  47  boxYC_nonsoluble_ag2 [mol(C) m-2(grid box)]
               #   code  48  boxYC_nonsoluble_bg2 [mol(C) m-2(grid box)]
               #   code  49  boxYC_humus_2        [mol(C) m-2(grid box)]

inf_nitro=$7   # selected codes from jsbach nitro stream (monthly means)
               #   code  70  box_minNflux_litter_green  [mol(N) m-2(grid box) s-1]
               #   code  71  box_minNflux_litter_wood   [mol(N) m-2(grid box) s-1]
               #   code  72  box_minNflux_slow    [mol(N) m-2(grid box) s-1]
               #   code  73  box_Nplant_uptake    [mol(N) m-2(grid box) s-1]
               #   code  74  box_Nsoil_uptake     [mol(N) m-2(grid box) s-1]
               #   code  77  box_Nfix_to_sminN    [mol(N) m-2(grid box) s-1]
               #   code  80  box_sminN_to_denit   [mol(N) m-2(grid box) s-1]
               #   code  81  box_sminN_leach      [mol(N) m-2(grid box) s-1]
               #   code 129  N2atmos_LUtrans      [mol(N) m-2(grid box) s-1]
               #   code 130  N2atmos_harvest      [mol(M) m-2(grid box) s-1]
               #   code 158  box_Nflx_2_crop_harvest  [mol(N) m-2(grid box) s-1]  
               #   code 212  box_N2O_total        [mikro-mol(N) m-2(grid box)]
               #   code 222  boxN_crop_harvest    [mol(N) m-2(grid box)]
               #   code 236  Ndep_forc            [kg(N) m-2(grid box) s-1]
               #   code 240  boxN_green           [mol(N) m-2(grid box)]
               #   code 241  boxN_woods           [mol(N) m-2(grid box)]
               #   code 242  boxN_mobile          [mol(N) m-2(grid box)]
               #   code 243  boxN_litter_green_ag [mol(N) m-2(grid box)]
               #   code 244  boxN_litter_green_bg [mol(N) m-2(grid box)]
               #   code 245  boxN_litter_wood_ag  [mol(N) m-2(grid box)]
               #   code 246  boxN_slow            [mol(N) m-2(grid box)]
               #   code 247  box_Npools_total     [mol(N) m-2(grid box)]
               #   code 248  boxN_litter_wood_bg  [mol(N) m-2(grid box)]
               #   code 250  boxN_sminN           [mol(N) m-2(grid box)]

inf_co2=$8     # selected codes from echam6 co2 stream (monthly means)
               #   code   5  co2_flux             [kg m-2 s-1]
               #   code   6  co2_flux_land        [kg m-2 s-1]
               #   code   8  co2_burden           [kg m-2]
               #   code  11  co2_emis             [mg m-2 s-1]
               #   code  20  co2_flx_anthro       [kg m-2 s-1]
               #   code  24  co2_flx_lcc          [kg m-2 s-1]
               #   code  25  co2_flx_harvest      [kg m-2 s-1]

inf_BOT=$9     # selected codes from echam6 BOT stream (monthly means)
               #   code   54  q2m                 []
               #   code   55  rh2m                []
               #   code  140  soil wetness        [m]
               #   code  141  sn                  [m]
               #   code  142  large scale precip  [kg/m**2s]
               #   code  143  convective precip   [kg/m**2s]
               #   code  144  snow fall           [kg/m**2s]
               #   code  146  sensible heat flux  [W/m**2]
               #   code  147  latent heat flux    [W/m**2]
               #   code  164  total cloud cover   []
               #   code  167  2m temperature      [K]
               #   code  169  surface temperature [K]
               #   code  171  10m windspeed       [m/s]
               #   code  176  net surf. sw rad.   [W/m**2]
               #   code  177  net surf. lw rad.   [W/m**2]
               #   code  182  evaporation         [kg/m**2s]
               #   code  204  upward surf. sw rad [W/m**2]
               #   code  205  upward surf. lw rad [W/m**2]

inf_accw=${10} # selected codes from jsbach accw stream (monthly means)
               #   code  160  runoff and drainage [kg/m**2s]
               #   code  161  drainage            [kg/m**2s]
               #   code  218  snow_melt           [kg/m**2s]

inf_surf=${11} # selected codes from jsbach surf stream (monthly means)
               #    203   1 jsswniracc            [W/m**2]
               #    204   1 jsswvisacc            [W/m**2]

inf_forc=${12} # selected codes from jsbach forcing stream (monthly means)
               #   code    1  air_temp            [deg C]
               #   code    2  precip_rain         [kg m-2 s-1]
               #   code    3  precip_snow         [kg m-2 s-1]
               #   code    7  rad_sw_down         [W m-2]
               #   code   12  wind_speed          [m/s]

inf_driv=${13} # selected codes from jsbach forcing stream (monthly means)
               #   code    2  zchl_acc

dynveg=${14}   # experiment with dynamic vegetation
nitrogen=${15} # experiment with nitrogen 
ls3mip=${16}   # also generate CMOR output for ls3mip
specialWish=${17}  # only generate output variables for a specialWish (e.g. trendy / crescendo)
tag=${18}        # name tag for CMOR output files

if [[ ! ${19} = "" ]]; then
  echam_fractional=yes
  slf=${19}       # if echam uses fractional land sea mask: fractional land mask
fi
non_cmor_vars=yes
ccycle=yes               # Simulation with C-cycle (i.e. LR-setup)
lumip=no                 # generate CMOR output needed for LUMIP
monitoring_vars_only=no  # only procesess variables interesting for monitoring
do_tests=no             # generate test arrays for consistency and conservation tests

# info
currentDate=$(date +'%m/%d/%Y')
branch="mpiesm-landveg"
revision=9777
TRENDY_version='v7'
Email='t.loughran@lmu.de'
scriptName=visualise_all.ksh

#------------------------------------------------------------------------------
# CMOR variabes that will be generated
#------------------------------------------------------------------------------ 
var_list="${var_list} clt evspsbl gpp gppGrass gppShrub gppTree hfls hfss huss hurs lai mrro mrros mrso mrsol mrsos npp nppGrass \
                      nppShrub nppTree pr ra raGrass raShrub raTree residualFrac rlds rlus rsds rsus snc snm snw tas tran ts tsl"
[[ ${dynveg} = yes ]] && \
    var_list="${var_list} baresoilFrac c3PftFrac c4PftFrac cropFrac cropFracC3 cropFracC4 grassFrac grassFracC3 \
                          grassFracC4 landCoverFrac pastureFrac pastureFracC3 pastureFracC4 shrubFrac treeFrac vegFrac"
[[ ${ccycle} = yes ]] && \
    var_list="${var_list} cLand cLitter cLitterGrass cLitterShrub cLitterTree co2mass cProduct cSoil cSoilGrass cSoilPools \
                          cSoilShrub cSoilTree cVeg cVegGrass cVegShrub cVegTree fco2antt fco2fos fco2nat fDeforestToProduct fFire \
                          fGrazing fHarvest fHarvestToProduct fLuc fProductDecomp nbp nep netAtmosLandCO2Flux rh rhGrass rhShrub \
                          rhSoil rhTree"
[[ ${nitrogen} = yes ]] && \
    var_list="${var_list} fBNF fN2O fNAnthDisturb fNdep fNgas fNgasFire fNgasNonFire fNleach fNloss fNnetmin fNProduct fNup nLand \
                          nLitter nMineral nSoil nProduct nVeg"
[[ ${lumip} = yes ]] && \
    var_list="${var_list} cLitterLut cSoilLut cVegLut fracInLut fracLut fracOutLut gppLut laiLut nppLut nwdFracLut raLut rhLut"
    #vg var_list="${var_list} fracLut gppLut laiLut nppLut nwdFracLut raLut"  #vg only these make sense with dynveg

[[ ${ls3mip} = yes ]] && \
    var_list="${var_list} cnc es et mrrob prra rss_nir rss_vis rzwc sbl sfcWind snwc tr"
    var_list="${var_list} rsds_nir rsds_vis rss_ns rss_nir_ns rss_vis_ns"  # Only available with   ls3m_type=1
    var_list="${var_list} ares"                # ls3mip variables that need code/namelist changes (set lpost=.true.)

[[ ${non_cmor_vars} = yes ]] && \
    var_list="${var_list} albs alpha burntArea cLitterCrop cLitterPast cSoilCrop cSoilFast cSoilPast cSoilSlow cVegCrop cVegPast \
                          fapar gppCrop gppPast grassFracPot lai_pft Nlimitation nppCrop nppPast nSoilFast nSoilSlow raCrop raPast \
                          rhCrop rhPast shrubFracPot treeFracPot"

[[ ${specialWish} = crescendo ]] && \
    var_list="albs cLitter cProduct cSoil cVeg evapotrans fapar fBNF fFire fGrazing fHarvest fLuc fN2O fNdep fNgas fNgasFire \
              fNleach fNloss fNnetmin fNup gpp hfls lai_pft landCoverFrac mrro mrso msl nbp nLitter npp nProduct nSoil nVeg pr ra \
              rh rsds sfls tas tran tsl"

[[ ${specialWish} = dailyLAI ]] && \
    var_list="dlaipft dgpp dgpppft dra drh dfFire dnbp dlai devapotrans dhfss dhfls dskinT fLuc"
    #var_list="dlai dhfls dskinT"

[[ ${specialWish} = trendy ]] && \
    var_list="albs burntArea cLitter cLitterpft cProduct cSoil cSoilpft cVeg \
              cVegpft evapotrans fapar fBNF fFire fGrazing fHarvest fLuc fN2O fNdep \
              fNleach fNloss fNnetmin fNup gpp gpppft hfls laipft landCoverFrac mrro \
              mrso msl nbp nLitter npp npppft nProduct nSoil nVeg pr ra \
              rh rsds sfls tas tran tsl"

[[ ${monitoring_vars_only} = yes ]] && \
    var_list="burntArea cLand cLitter cSoil cSoilFast cSoilSlow cVeg mrso nLand nLitter nSoil nSoilFast nSoilSlow nVeg gpp npp \
              lai rh ra treeFrac shrubFrac grassFrac c3PftFrac c4PftFrac baresoilFrac snc fBNF fNup"

#var_list="nbp" #vg-test

#------------------------------------------------------------------------------
# Process the variables
#------------------------------------------------------------------------------
# convert from mol(C)/m2 to kg(C)/m2
#  => factor: 0.01201
#
# convert from [kg(CO2)/m2 s] to [kg(C)/m2 s]
# molar Mass CO2: 44.0095 
# molar Mass C:   12.01
#  => factor: 0.272912
#
# convert from mol(N)/m2 to kg(N)/m2
#  => factor: 0.0140068


# variables needed to process other variables
#---------------------------------------------
# preparations

cdo selvar,slm        ${inf_ini}  slm
[[ ${echam_fractional} != yes ]] && slf=slm
cdo invertlat -mul ${slf} -gridarea ${slf} gridareafrac.nc
cdo selvar,cover_type ${inf_ini}  cover_type

# cover fractions and veg_ratio_max
cdo selcode,12        ${inf_main} cover_fract.tmp
if [[ ${dynveg} = yes ]]; then
    cdo selcode,20      ${inf_main} veg_ratio_max.tmp
else
    cdo selvar,veg_ratio_max  ${inf_ini} veg_ratio_max.tmp
fi
cdo mul veg_ratio_max.tmp cover_fract.tmp  box_cover_fract.tmp

#  In grib format 3.05e-5 is the minimum value different from 0 for records with values between 0 and 1
# we set all fractions below 5.e-5 to zero, to avoid errors with variables that need to be scaled.
# It is important to use netCDF for these calculations! 
cdo -f nc mul -gtc,5.e-5 cover_fract.tmp     cover_fract.tmp      cover_fract
cdo -f nc mul -gtc,5.e-5 veg_ratio_max.tmp   veg_ratio_max.tmp    veg_ratio_max
cdo -f nc mul -gtc,5.e-5 box_cover_fract.tmp box_cover_fract.tmp  box_cover_fract
if [[ ${dynveg} = yes ]]; then
    cdo selcode,10      ${inf_main} cover_fract_pot.tmp
    cdo mul veg_ratio_max.tmp cover_fract_pot.tmp  box_cover_fract_pot.tmp
    cdo -f nc mul -gtc,5.e-5 box_cover_fract_pot.tmp box_cover_fract_pot.tmp  box_cover_fract_pot
fi
cdo eqc,1 -sellevel,1 cover_type glac

cdo sub -addc,1 -mulc,-1 veg_ratio_max  glac  bare
cdo sub slm glac land

cdo subc,20 -sellevel,11 cover_type C3C4_crop_mask

# fraction of soil within second soil layer
#  1st layer  (3):  6.5 cm   -> complete      (minimum soil depth in jsbach: 10 cm)
#  2nd layer (19): 25.4 cm
#   => bottom of layer 2: 31.9 cm
cdo selvar,soil_depth ${inf_ini} soil_depth.tmp
cdo min soil_depth.tmp -const,.319,soil_depth.tmp complete_lay2.tmp
cdo -expr,'lay2frac = (soil_depth - 0.065) / 0.254' complete_lay2.tmp lay2frac.tmp
cdo -ifthen slm -ifnotthen glac -expr,'lay2factor = 1./lay2frac' lay2frac.tmp lay2factor.tmp

if [[ ${specialWish} = dailyLAI ]]; then
    var_list_first="dgpp_tiles dnpp_tiles dra_tiles drh_tiles"
    #var_list_first=""
else
    var_list_first="cLitter_tiles cSoil_tiles cVeg_tiles fracLut gpp_tiles landCoverFrac mrsol npp_tiles ra_tiles rh_tiles"
fi
for var in ${var_list_first}; do
    {
    case ${var} in
        cLitter_tiles )  # carbon in litter pools  - only above ground litter
            if [[ ${ccycle} == yes ]]; then
               [[ ! -f cLitter_tiles.tmp ]] || rm cLitter_tiles.tmp
               cdo enssum -selcode,31 ${inf_yasso} -selcode,41 ${inf_yasso} \
                          -selcode,33 ${inf_yasso} -selcode,43 ${inf_yasso} \
                          -selcode,35 ${inf_yasso} -selcode,45 ${inf_yasso} \
                          -selcode,37 ${inf_yasso} -selcode,47 ${inf_yasso}  cLitter_tiles.tmp
            fi
            ;;
        cSoil_tiles )  # carbon in soil pools - including below ground litter
            if [[ ${ccycle} == yes ]]; then
               [[ ! -f cSoil_tiles.tmp ]] || rm cSoil_tiles.tmp
               cdo enssum -selcode,32 ${inf_yasso} -selcode,42 ${inf_yasso} \
                          -selcode,34 ${inf_yasso} -selcode,44 ${inf_yasso} \
                          -selcode,36 ${inf_yasso} -selcode,46 ${inf_yasso} \
                          -selcode,38 ${inf_yasso} -selcode,48 ${inf_yasso} \
                          -selcode,39 ${inf_yasso} -selcode,49 ${inf_yasso}  cSoil_tiles.tmp
            fi
            ;;
        cVeg_tiles )  # carbon in vegetation pools
            if [[ ${ccycle} == yes ]]; then
               [[ ! -f cVeg_tiles.tmp ]] || rm cVeg_tiles.tmp
               cdo enssum -selcode,160 ${inf_veg} \
                          -selcode,161 ${inf_veg} \
                          -selcode,162 ${inf_veg}  cVeg_tiles.tmp
            fi
            ;;
        dgpp_tiles ) # daily GPP 
            cdo selcode,173 outdata/veg_daily.grb dgpp_tiles.tmp
            ;;
        dnpp_tiles )  # daily NPP
            cdo selcode,178 outdata/veg_daily.grb  dnpp_tiles.tmp
            ;;
        dra_tiles )  # daily autotrophic respiration
            cdo sub -selcode,173 outdata/veg_daily.grb \
                    -selcode,178 outdata/veg_daily.grb  dra_tiles.tmp
            ;;
        drh_tiles )  # daily heterotrophic respiration (herbivory and soil respiration, excluding crop harvest)
            if [[ ${ccycle} == yes ]]; then

               # code 170 (box_soil_respiration) includes the flux from the crop harvest pool to the atmosphere. It
               # needs to be subtracted from tile 11 (crop tile) to get the pure soil respiration
               cdo -f nc setlevel,11 -selcode,211 outdata/veg_daily.grb dcrop_harvest.tmp
               cdo -f nc splitlevel  -selcode,170 outdata/veg_daily.grb drhSoil.
               cdo add drhSoil.000011.nc dcrop_harvest.tmp drhSoil.000011.tmp
               mv drhSoil.000011.tmp drhSoil.000011.nc
               [[ ! -f drhSoil_tiles.tmp ]] || rm drhSoil_tiles.tmp
               cdo merge drhSoil.0000??.nc drhSoil_tiles.tmp
               rm drhSoil.0000??.nc

               # add flux from herbivory to soil respiration to get the total heterotrophic respiration
               cdo mulc,-1. -add  drhSoil_tiles.tmp  -selcode,143 outdata/veg_daily.grb  drh_tiles.tmp
            fi
            ;;
        gpp_tiles )  # GPP
            cdo selcode,173 ${inf_veg}  gpp_tiles.tmp
            ;;
        landCoverFrac )  # land cover fractions [%]
            cdo -f nc splitlevel -mulc,100 box_cover_fract lct_
            cdo setlevel,1 -setvar,var12             -mulc,100. bare bare.tmp
            cdo setlevel,2 -setvar,var12 -ifthen slm -mulc,100. glac glac.tmp
            cdo setlevel,3  lct_000001.nc                     tropev.tmp
            cdo setlevel,4  lct_000002.nc                     tropdec.tmp
            cdo setlevel,5  lct_000003.nc                     extrev.tmp
            cdo setlevel,6  lct_000004.nc                     extrdec.tmp
            cdo setlevel,7  lct_000005.nc                     rgshrub.tmp
            cdo setlevel,8  lct_000006.nc                     decshrub.tmp
            cdo setlevel,9  lct_000007.nc                     c3grass.tmp 
            cdo setlevel,10 lct_000008.nc                     c4grass.tmp
            cdo setlevel,11 lct_000009.nc                     c3past.tmp
            cdo setlevel,12 lct_000010.nc                     c4past.tmp
            cdo setlevel,14 -mul C3C4_crop_mask lct_000011.nc c4crop.tmp
            cdo setlevel,13 -sub lct_000011.nc  c4crop.tmp    c3crop.tmp

            # C4MIP sub-grid tile fractions [%]
            #   In contrast to LUMIP natural vegetation tiles here do NOT include bare land.
            [[ ! -f tree.tmp ]] || rm tree.tmp
            cdo enssum  tropev.tmp tropdec.tmp \
                        extrev.tmp extrdec.tmp  tree.tmp
            cdo add  rgshrub.tmp  decshrub.tmp  shrub.tmp
            cdo add  c3grass.tmp  c4grass.tmp   grass.tmp
            cdo add  c3past.tmp   c4past.tmp    past.tmp
            cdo add  c3crop.tmp   c4crop.tmp    crop.tmp

            # scaling factor from grid cell to tile values (1/fraction)
            cdo -f nc div -const,100.,tree.tmp  tree.tmp  divtree
            cdo -f nc div -const,100.,shrub.tmp shrub.tmp divshrub
            cdo -f nc div -const,100.,grass.tmp grass.tmp divgrass
            cdo -f nc div -const,100.,past.tmp  past.tmp  divpast
            cdo -f nc div -const,100.,crop.tmp  crop.tmp  divcrop
            ;;
        fracLut )  # land use tile fractions (LUMIP)
            if [[ ${lumip} = yes ]]; then

                # primary and secondary land (psl) includes the bare soil fraction
                cdo      vertsum -sellevel,1/8  box_cover_fract       nat     # natural vegetation without bare land
                cdo add -vertsum -sellevel,1/8  box_cover_fract bare  psl     # primary and secondary land including bare land
                cdo               sellevel,11   box_cover_fract       crp
                cdo      vertsum -sellevel,9/10 box_cover_fract       pst
                # convert fractions to percent 
                cdo setlevel,1 -mulc,100.   psl      psl.tmp
                cdo setlevel,2 -mulc,100.   crp      crp.tmp
                cdo setlevel,3 -mulc,100.   pst      pst.tmp

                # scaling factor from grid cell to tile values (1/fraction)
                cdo -f nc div -const,1.,psl  psl  divpsl
                cdo -f nc div -const,1.,crp  crp  divcrp
                cdo -f nc div -const,1.,pst  pst  divpst
                cdo -f nc div -const,1.,pst -const,0.,pst  divurb  # dummy array with missing values


                # generate timeseries of input LU-transitions for fracInLut and fracOutLut
                yearlist=$(cdo showyear ${inf_veg})
                LUHdir=/pool/data/JSBACH/input/r0009/T63/New_Hampshire_LCC/hist_dynveg  #vg: needs to be adapted!!
                LUHlist=""
                for yr in ${yearlist}; do
                    LUHlist="${LUHlist} ${LUHdir}/LUH_scaled_transitions_T63_${yr}.nc"
                done
                [[ ! -f LUH_transitions.tmp1 ]] || rm  LUH_transitions.tmp1
                cdo cat ${LUHlist}  LUH_transitions.tmp1
                [[ ! -f LUH_transitions.tmp ]] || rm  LUH_transitions.tmp
                cdo merge -setvar,nat nat -setvar,crp crp -setvar,pst pst LUH_transitions.tmp1 LUH_transitions.tmp
           fi
            ;;
        mrsol )  # water content of soil layer
            cdo mulc,1000. -selcode,84 ${inf_main}  mrsol.tmp
            ;;
        npp_tiles )  # NPP
            cdo selcode,178 ${inf_veg}  npp_tiles.tmp
            ;;
        ra_tiles )  # autotrophic respiration
            cdo sub -selcode,173 ${inf_veg} \
                    -selcode,178 ${inf_veg}  ra_tiles.tmp
            ;;
        rh_tiles )  # heterotrophic respiration (herbivory and soil respiration, excluding crop harvest)
            if [[ ${ccycle} == yes ]]; then

               # code 170 (box_soil_respiration) includes the flux from the crop harvest pool to the atmosphere. It
               # needs to be subtracted from tile 11 (crop tile) to get the pure soil respiration
               cdo -f nc setlevel,11 -selcode,211 ${inf_veg} crop_harvest.tmp
               cdo -f nc splitlevel  -selcode,170 ${inf_veg} rhSoil.
               cdo add rhSoil.000011.nc crop_harvest.tmp rhSoil.000011.tmp
               mv rhSoil.000011.tmp rhSoil.000011.nc
               [[ ! -f rhSoil_tiles.tmp ]] || rm rhSoil_tiles.tmp
               cdo merge rhSoil.0000??.nc rhSoil_tiles.tmp
               rm rhSoil.0000??.nc

               # add flux from herbivory to soil respiration to get the total heterotrophic respiration
               cdo mulc,-1. -add  rhSoil_tiles.tmp  -selcode,143 ${inf_veg}  rh_tiles.tmp
            fi
            ;;
        * )
            echo "variable ${var} not supported"
            exit 1
            ;;
    esac
    } &  # comment out '&' for serial processing
done
wait

# process cmor variables
#------------------------
for var in ${var_list}; do
    {
    case ${var} in
        albs )  # Surface Albedo (CRESCENDO)  - not for CMOR
                # A minimum amount of incoming radiation is needed to avoid albs values greater 1 in the high latitudes.
            cdo -f nc max -const,0.,${inf_main} -selcode,22 ${inf_main} albs.tmp1   # sw_down_reflect should not be negative (grib)
            cdo -f nc gtc,0.1 -selcode,21 ${inf_main} albs.tmp2                     # mask with sw_down greater than a minimum value
            cdo ifthen albs.tmp2 -div albs.tmp1 -selcode,21 ${inf_main} albs.tmp
            echo '&parameter name=albs long_name="Surface Albedo" units="1" /' > albs.partab
            ;;
        alpha )  # water stress coefficient - not for CMOR
            cdo sellevel,1 -selcode,169 ${inf_veg}  alpha.tmp   # all soil levels have identical values
            echo '&parameter name=alpha long_name="Water Stress Coefficient (0: stress; 1: no stress)" units="kg m-2" /' \
                 > alpha.partab
            ;;
        ares )  # aerodynamic resistance   
                #    - surface stream  code 56 (zchl; mo_surface_memory)  is not in the output in standard setup (lpost=false) 
                #    - driving stream  code 2  (zchl_acc; mo_jsbalone)   is not in the output in standard setup (lpost=false) 
            if [[ -f ${inf_BOT} ]]; then
                cdo div -const,1.,slm -mul -selcode,56 ${inf_surf} -selcode,171 ${inf_BOT} ares.tmp
            else
                cdo div -const,1.,slm -mul -selcode,2 ${inf_driv} -selcode,12 ${inf_forc} ares.tmp
            fi
            echo '&parameter name=ares long_name="Aerodynamic resistance" units="s m-1" /' > ares.partab
            ;;
        baresoilFrac )  # bare soil (desert)
            cdo mulc,100. -sub -addc,1 -mulc,-1 veg_ratio_max glac baresoilFrac.tmp
            echo '&parameter name=baresoilFrac long_name="Bare Soil Fraction" units="%" /' > baresoilFrac.partab
            ;;
        burntArea )  # burnt area (CMIP5: burntArea, CMIP6: burntFractionAll)  - not for CMOR
            # Jsbach calculates burnt fraction per time step. CMIP6 asks for the area fraction covered by burnt vegetation 
            # (burntFractionAll). As jsbach does not track re-growth of vegetation on burned area, we cannot provide this 
            # variable.
            cdo mulc,100. -muldpm -vertsum -selcode,50 ${inf_veg} burntArea.tmp     # with spitfire   (burnt area)
            # cdo mulc,100. -muldpm -vertsum -selcode,53 ${inf_veg} burntArea.tmp   # old jsbach fire
            echo '&parameter name=burntArea long_name="Burnt area" units="%/month" /' > burntArea.partab
            ;;
        c3PftFrac )  # C3 plant fraction    (c3PftFrac + c4PftFrac = vegFrac)
            [[ ! -f c3PftFrac.tmp ]] || rm c3PftFrac.tmp
            cdo enssum  tropev.tmp   tropdec.tmp \
                        extrev.tmp   extrdec.tmp \
                       rgshrub.tmp  decshrub.tmp \
                       c3grass.tmp    c3past.tmp  c3crop.tmp   c3PftFrac.tmp
            echo '&parameter name=c3PftFrac long_name="Total C3 PFT Cover Fraction" units="%" /' > c3PftFrac.partab
            ;;
        c4PftFrac )  # C4 plant fraction
            [[ ! -f c4PftFrac.tmp ]] || rm c4PftFrac.tmp
            cdo enssum  c4grass.tmp c4past.tmp c4crop.tmp  c4PftFrac.tmp
            echo '&parameter name=c4PftFrac long_name="Total C4 PFT Cover Fraction" units="%" /' > c4PftFrac.partab
            ;;
        cLand )  # Total land carbon   (cLand = cVeg + cLitter + cSoil + cProduct (Jones at al.))
            cdo mulc,0.01201 -selcode,176 ${inf_veg} cLand.tmp
            echo '&parameter name=cLand long_name="Total Carbon in all Terrestrial Carbon Pools" units="kg m-2" /' > cLand.partab
            ;;
        cLitter )  # carbon in litter  (processed above) 
                   #   cLitter =   cLitterTree*treeFrac + cLitterGrass*grassFrac + cLitterShrub*shrubFrac
                   #             + cLitterCrop*cropFrac + cLitterPast*pastureFrac 
                   #   cLitter = SUM (cLitterLut(:) * fracLut(:))
            cdo mulc,0.01201 -vertsum cLitter_tiles.tmp cLitter.tmp
            echo '&parameter name=cLitter long_name="Carbon Mass in Litter Pools" units="kg m-2" /' > cLitter.partab
            ;;
        cLitterpft )  # cLitter per PFT (TRENDY)  - not for CMOR          -- copied behaviour from LAIpft
            cdo mulc,0.01201 cLitter_tiles.tmp cLitter_tiles_C.tmp
            cdo -f nc splitlevel cLitter_tiles_C.tmp cLitterpft_
            cdo setlevel,1 -ifthen slm -mulc,0. cLitterpft_000001.nc cLitterpft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. cLitterpft_000001.nc cLitterpft02.tmp  # glacier
            cdo setlevel,3                      cLitterpft_000001.nc cLitterpft03.tmp
            cdo setlevel,4                      cLitterpft_000002.nc cLitterpft04.tmp
            cdo setlevel,5                      cLitterpft_000003.nc cLitterpft05.tmp
            cdo setlevel,6                      cLitterpft_000004.nc cLitterpft06.tmp
            cdo setlevel,7                      cLitterpft_000005.nc cLitterpft07.tmp
            cdo setlevel,8                      cLitterpft_000006.nc cLitterpft08.tmp
            cdo setlevel,9                      cLitterpft_000007.nc cLitterpft09.tmp
            cdo setlevel,10                     cLitterpft_000008.nc cLitterpft10.tmp
            cdo setlevel,11                     cLitterpft_000009.nc cLitterpft11.tmp
            cdo setlevel,12                     cLitterpft_000010.nc cLitterpft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask cLitterpft_000011.nc cLitterpft14.tmp  # c4 crop
            cdo setlevel,13 -sub cLitterpft_000011.nc  cLitterpft14.tmp cLitterpft13.tmp  # c3 crop
            [[ ! -f cLitterpft.tmp1 ]] || rm cLitterpft.tmp1
            cdo merge cLitterpft??.tmp  cLitterpft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT cLitterpft.tmp1 cLitterpft.tmp
            echo '&parameter name=cLitterpft long_name="Carbon Mass in Litter Pools" units="kg m-2" /' > cLitterpft.partab
            ;;
        cLitterCrop )  # Litter carbon of Crops  - not for CMOR
            cdo mulc,0.01201 -ifthen divcrop -mul -sellevel,11 cLitter_tiles.tmp divcrop cLitterCrop.tmp
            echo '&parameter name=cLitterCrop long_name="Carbon Mass in Litter of Crop Tiles" units="kg m-2" /' > cLitterCrop.partab
            ;;
        cLitterGrass )  # Litter carbon of Grass
            cdo mulc,0.01201 -ifthen divgrass -mul -vertsum -sellevel,7/8 cLitter_tiles.tmp divgrass cLitterGrass.tmp
            echo '&parameter name=cLitterGrass long_name="Carbon Mass in Litter on Grass tiles" units="kg m-2" /' \
                 > cLitterGrass.partab
            ;;
        cLitterLut )  # Litter carbon on land use tiles
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo -f grb setlevel,1 \
                -ifthen divpsl -mulc,0.01201 -mul -vertsum -sellevel,1/8  cLitter_tiles.tmp divpsl cLitterLut.psl.tmp
            cdo -f grb setlevel,2 \
                -ifthen divcrp -mulc,0.01201 -mul          -sellevel,11   cLitter_tiles.tmp divcrp cLitterLut.crp.tmp
            cdo -f grb setlevel,3 \
                -ifthen divpst -mulc,0.01201 -mul -vertsum -sellevel,9/10 cLitter_tiles.tmp divpst cLitterLut.pst.tmp
            cdo -f grb setlevel,4 \
                -ifthen divurb           cLitterLut.pst.tmp                                        cLitterLut.urb.tmp
            [[ ! -f cLitterLut.tmp1 ]] || rm cLitterLut.tmp1
            cdo -f nc merge  cLitterLut.psl.tmp cLitterLut.crp.tmp cLitterLut.pst.tmp cLitterLut.urb.tmp  cLitterLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile    cLitterLut.tmp1 cLitterLut.tmp
           echo '&parameter name=cLitterLut long_name="carbon in litter pools on land use tiles" units="kg m-2" /' \
                 > cLitterLut.partab
            ;;
        cLitterPast )  # Litter carbon of Pasture  - not for CMOR
            cdo mulc,0.01201 -ifthen divpast -mul -vertsum -sellevel,9/10 cLitter_tiles.tmp divpast  cLitterPast.tmp
            echo '&parameter name=cLitterPast long_name="Carbon Mass in Litter of Pasture Tiles" units="kg m-2" /' \
                 > cLitterPast.partab
            ;;
        cLitterShrub )  # Litter carbon of Shrubs
            cdo mulc,0.01201 -ifthen divshrub -mul -vertsum -sellevel,5/6 cLitter_tiles.tmp divshrub  cLitterShrub.tmp
            echo '&parameter name=cLitterShrub long_name="Carbon Mass in Litter of Shrub Tiles" units="kg m-2" /' \
                 > cLitterShrub.partab
            ;;
        cLitterTree )  # Litter carbon of Trees
            cdo mulc,0.01201 -ifthen divtree -mul -vertsum -sellevel,1/4 cLitter_tiles.tmp divtree  cLitterTree.tmp
            echo '&parameter name=cLitterTree long_name="Carbon Mass in Litter of Tree Tiles" units="kg m-2" /' > cLitterTree.partab
            ;;
        clt )  # cloud cover
            cdo mulc,100. -selcode,164 ${inf_BOT} clt.tmp
            echo '&parameter name=clt long_name="Total Cloud Fraction" units="%" /' > clt.partab
            ;;
        cnc )  # canopy fraction
            cdo mulc,100. -ifthen slm -vertsum -selcode,24 ${inf_main} cnc.tmp
            echo '&parameter name=cnc long_name="Canopy Covered Area Percentage" units="%" /' > cnc.partab
             ;;
        co2mass )
            cdo selcode,8 ${inf_co2} co2mass.tmp  # Note: unit is kg(CO2); generally it is kg(C) 
            echo '&parameter name=co2mass long_name="Total Atmospheric Mass of CO2" units="kg m-2" /' > co2mass.partab
            ;;
        cProduct )  # carbon in Produc pools   (d/dt cProduct = fDeforestToProduct + fHarvestToProduct - fProductDecomp)
            [[ ! -f cProduct.tmp1 ]] || rm cProduct.tmp1
            cdo enssum -selcode,218 ${inf_veg} \
                       -selcode,220 ${inf_veg} \
                       -selcode,221 ${inf_veg} \
                       -selcode,222 ${inf_veg} \
                       -selcode,223 ${inf_veg} \
                       -selcode,224 ${inf_veg} \
                       -vertsum -selcode,225 ${inf_veg}  cProduct.tmp1
            cdo mulc,0.01201 cProduct.tmp1  cProduct.tmp
            echo '&parameter name=cProduct long_name="Carbon Mass in Products of Land Use Change" units="kg m-2" /' \
                 > cProduct.partab
            ;;
        cropFrac )  # crop fraction   (cropFrac = cropFracC3 + cropFracC4)
            cp crop.tmp  cropFrac.tmp
            echo '&parameter name=cropFrac long_name="Crop Fraction" units="%" /' > cropFrac.partab
            ;;
        cropFracC3 )  # C3 crop fraction
            cp c3crop.tmp  cropFracC3.tmp
            echo '&parameter name=cropFracC3 long_name="Percentage Cover by C3 Crops" units="%" /' > cropFracC3.partab
            ;;
        cropFracC4 )  # C4 crop fraction
            cp c4crop.tmp  cropFracC4.tmp
            echo '&parameter name=cropFracC4 long_name="Percentage Cover by C4 Crops" units="%" /' > cropFracC4.partab
            ;;
        cSoil )  # carbon in soil pools   (cSoil = cSoilFast + cSoilSlow (not CMOR))
                 #     cSoil =   cSoilGrass*grassfrac + cSoilShrub*shrubfrac + cSoilTree*treefrac 
                 #             + cSoilCrop*cropFrac + cSoilPast+pastureFrac
                 #     cSoil = SUM(cSoilLut(:) * fracLut(:))
                 #     cSoil = SUM(cSoilPools(:))
            cdo mulc,0.01201 -vertsum cSoil_tiles.tmp  cSoil.tmp
            echo '&parameter name=cSoil long_name="Carbon Mass in Soil Pools" units="kg m-2" /' > cSoil.partab
            ;;
        cSoilpft )  # cSoil per PFT (TRENDY)  - not for CMOR          -- copied behaviour from LAIpft
            cdo mulc,0.01201 cSoil_tiles.tmp cSoil_tiles_C.tmp
            cdo -f nc splitlevel cSoil_tiles_C.tmp cSoilpft_
            cdo setlevel,1 -ifthen slm -mulc,0. cSoilpft_000001.nc cSoilpft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. cSoilpft_000001.nc cSoilpft02.tmp  # glacier
            cdo setlevel,3                      cSoilpft_000001.nc cSoilpft03.tmp
            cdo setlevel,4                      cSoilpft_000002.nc cSoilpft04.tmp
            cdo setlevel,5                      cSoilpft_000003.nc cSoilpft05.tmp
            cdo setlevel,6                      cSoilpft_000004.nc cSoilpft06.tmp
            cdo setlevel,7                      cSoilpft_000005.nc cSoilpft07.tmp
            cdo setlevel,8                      cSoilpft_000006.nc cSoilpft08.tmp
            cdo setlevel,9                      cSoilpft_000007.nc cSoilpft09.tmp
            cdo setlevel,10                     cSoilpft_000008.nc cSoilpft10.tmp
            cdo setlevel,11                     cSoilpft_000009.nc cSoilpft11.tmp
            cdo setlevel,12                     cSoilpft_000010.nc cSoilpft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask cSoilpft_000011.nc cSoilpft14.tmp  # c4 crop
            cdo setlevel,13 -sub cSoilpft_000011.nc  cSoilpft14.tmp cSoilpft13.tmp  # c3 crop
            [[ ! -f cSoilpft.tmp1 ]] || rm cSoilpft.tmp1
            cdo merge cSoilpft??.tmp  cSoilpft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT cSoilpft.tmp1 cSoilpft.tmp
            echo '&parameter name=cSoilpft long_name="Carbon Mass in Soil Pools" units="kg m-2" /' > cSoilpft.partab
            ;;
        cSoilCrop ) # soil carbon of crops  - not for CMOR
            cdo mulc,0.01201 -ifthen divcrop -mul -sellevel,11 cSoil_tiles.tmp divcrop  cSoilCrop.tmp
            echo '&parameter name=cSoilCrop long_name="Carbon Mass in Soil on Crop Tiles" units="kg m-2" /' > cSoilCrop.partab
            ;;
        cSoilFast )  # carbon in fast soil pools (= below ground litter)  - not for CMOR
            [[ ! -f cSoilFast.tmp1 ]] || rm cSoilFast.tmp1
            cdo enssum -selcode,32 ${inf_yasso} -selcode,42 ${inf_yasso} \
                       -selcode,34 ${inf_yasso} -selcode,44 ${inf_yasso} \
                       -selcode,36 ${inf_yasso} -selcode,46 ${inf_yasso} \
                       -selcode,38 ${inf_yasso} -selcode,48 ${inf_yasso}  cSoilFast.tmp1
            cdo mulc,0.01201 -vertsum cSoilFast.tmp1  cSoilFast.tmp
            echo '&parameter name=cSoilFast long_name="Carbon Mass in Fast Soil Pool" units="kg m-2" /' > cSoilFast.partab
            ;;
        cSoilGrass )  # soil carbon of grass
            cdo mulc,0.01201 -ifthen divgrass -mul -vertsum -sellevel,7/8 cSoil_tiles.tmp divgrass  cSoilGrass.tmp
            echo '&parameter name=cSoilGrass long_name="Carbon Mass in Soil on Grass Tiles" units="kg m-2" /' > cSoilGrass.partab
            ;;
        cSoilLut )  # Soil carbon on land use tiles
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo -f grb setlevel,1 -ifthen divpsl -mulc,0.01201 -mul -vertsum -sellevel,1/8  cSoil_tiles.tmp divpsl cSoilLut.psl.tmp
            cdo -f grb setlevel,2 -ifthen divcrp -mulc,0.01201 -mul          -sellevel,11   cSoil_tiles.tmp divcrp cSoilLut.crp.tmp
            cdo -f grb setlevel,3 -ifthen divpst -mulc,0.01201 -mul -vertsum -sellevel,9/10 cSoil_tiles.tmp divpst cSoilLut.pst.tmp
            cdo -f grb setlevel,4 -ifthen divurb              cSoilLut.crp.tmp                                     cSoilLut.urb.tmp
            [[ ! -f cSoilLut.tmp1 ]] || rm cSoilLut.tmp1
            cdo -f nc merge  cSoilLut.psl.tmp cSoilLut.crp.tmp cSoilLut.pst.tmp cSoilLut.urb.tmp  cSoilLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile    cSoilLut.tmp1 cSoilLut.tmp
            echo '&parameter name=cSoilLut long_name="carbon in soil pool on land use tiles" units="kg m-2" /' > cSoilLut.partab
            ;;
        cSoilPast ) # soil carbon of pasture  - not for CMOR
            cdo mulc,0.01201 -ifthen divpast -mul -vertsum -sellevel,9/10 cSoil_tiles.tmp divpast  cSoilPast.tmp
            echo '&parameter name=cSoilPast long_name="Carbon Mass in Soil on Pasture Tiles" units="kg m-2" /' > cSoilPast.partab
            ;;
        cSoilPools ) # carbon in different soil pools
            cdo setcode,32 -setlevel,1  -mulc,0.01201 -vertsum -selcode,32 ${inf_yasso}    cAcidSolubleLeaf.tmp
            cdo setcode,32 -setlevel,2  -mulc,0.01201 -vertsum -selcode,42 ${inf_yasso}    cAcidSolubleWood.tmp
            cdo setcode,32 -setlevel,3  -mulc,0.01201 -vertsum -selcode,34 ${inf_yasso}   cWaterSolubleLeaf.tmp
            cdo setcode,32 -setlevel,4  -mulc,0.01201 -vertsum -selcode,44 ${inf_yasso}   cWaterSolubleWood.tmp
            cdo setcode,32 -setlevel,5  -mulc,0.01201 -vertsum -selcode,36 ${inf_yasso} cEthanolSolubleLeaf.tmp
            cdo setcode,32 -setlevel,6  -mulc,0.01201 -vertsum -selcode,46 ${inf_yasso} cEthanolSolubleWood.tmp
            cdo setcode,32 -setlevel,7  -mulc,0.01201 -vertsum -selcode,38 ${inf_yasso}     cNonSolubleLeaf.tmp
            cdo setcode,32 -setlevel,8  -mulc,0.01201 -vertsum -selcode,48 ${inf_yasso}     cNonSolubleWood.tmp
            cdo setcode,32 -setlevel,9  -mulc,0.01201 -vertsum -selcode,39 ${inf_yasso}          cHumusLeaf.tmp
            cdo setcode,32 -setlevel,10 -mulc,0.01201 -vertsum -selcode,49 ${inf_yasso}          cHumusWood.tmp
            [[ ! -f cSoilPools.tmp1 ]] || rm cSoilPools.tmp1
            cdo -f nc merge   cAcidSolubleLeaf.tmp    cAcidSolubleWood.tmp \
                             cWaterSolubleLeaf.tmp   cWaterSolubleWood.tmp \
                           cEthanolSolubleLeaf.tmp cEthanolSolubleWood.tmp \
                               cNonSolubleLeaf.tmp     cNonSolubleWood.tmp \
                                    cHumusLeaf.tmp          cHumusWood.tmp     cSoilPools.tmp1
            ncrename -h -O -d sfc,Pool -v sfc,Pool cSoilPools.tmp1 cSoilPools.tmp
            echo '&parameter name=cSoilPools long_lame="Carbon Mass in Each Model Soil Pool" units="kg m-2" /' > cSoilPools.partab
            ;;
        cSoilShrub )  # soil carbon of shrubs
            cdo mulc,0.01201 -ifthen divshrub -mul -vertsum -sellevel,5/6 cSoil_tiles.tmp divshrub  cSoilShrub.tmp
            echo '&parameter name=cSoilShrub long_name="Carbon Mass in Soil on Shrub Tiles" units="kg m-2" /' > cSoilShrub.partab
            ;;
        cSoilSlow )  # carbon in slow soil pools (= humus pools)  cSoil = cSoilFast + cSoilSlow
            cdo mulc,0.01201 -vertsum -add -selcode,39 ${inf_yasso} -selcode,49 ${inf_yasso}  cSoilSlow.tmp
            echo '&parameter name=cSoilSlow long_name="Carbon Mass in Slow Soil Pool" units="kg m-2" /' > cSoilSlow.partab
            ;;
        cSoilTree ) # soil carbon of trees
            cdo mulc,0.01201 -ifthen divtree -mul -vertsum -sellevel,1/4 cSoil_tiles.tmp divtree  cSoilTree.tmp
            echo '&parameter name=cSoilTree long_name="Carbon Mass in Soil on Tree Tiles" units="kg m-2" /' > cSoilTree.partab
            ;;
        cVeg )  # total vegetation carbon
                #    cVeg = cVegCrop*cropFrac + cVegPast*pastureFrac + cVegGrass*grassFrac + cVegShrub*shrubFrac + cVegTree*treeFrac
                #    cVeg = SUM(cVegLut(:) * fracLut(:))
            cdo mulc,0.01201 -vertsum cVeg_tiles.tmp  cVeg.tmp
            echo '&parameter name=cVeg long_name="Carbon Mass in Vegetation" units="kg m-2" /' > cVeg.partab
            ;;
        cVegpft )  # cVeg per PFT (TRENDY)  - not for CMOR          -- copied behaviour from LAIpft
            cdo mulc,0.01201 cVeg_tiles.tmp cVeg_tiles_C.tmp
            cdo -f nc splitlevel cVeg_tiles_C.tmp cVegpft_
            cdo setlevel,1 -ifthen slm -mulc,0. cVegpft_000001.nc cVegpft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. cVegpft_000001.nc cVegpft02.tmp  # glacier
            cdo setlevel,3                      cVegpft_000001.nc cVegpft03.tmp
            cdo setlevel,4                      cVegpft_000002.nc cVegpft04.tmp
            cdo setlevel,5                      cVegpft_000003.nc cVegpft05.tmp
            cdo setlevel,6                      cVegpft_000004.nc cVegpft06.tmp
            cdo setlevel,7                      cVegpft_000005.nc cVegpft07.tmp
            cdo setlevel,8                      cVegpft_000006.nc cVegpft08.tmp
            cdo setlevel,9                      cVegpft_000007.nc cVegpft09.tmp
            cdo setlevel,10                     cVegpft_000008.nc cVegpft10.tmp
            cdo setlevel,11                     cVegpft_000009.nc cVegpft11.tmp
            cdo setlevel,12                     cVegpft_000010.nc cVegpft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask cVegpft_000011.nc cVegpft14.tmp  # c4 crop
            cdo setlevel,13 -sub cVegpft_000011.nc  cVegpft14.tmp cVegpft13.tmp  # c3 crop
            [[ ! -f cVegpft.tmp1 ]] || rm cVegpft.tmp1
            cdo merge cVegpft??.tmp  cVegpft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT cVegpft.tmp1 cVegpft.tmp
            echo '&parameter name=cVegpft long_name="Carbon Mass in Vegetation" units="kg m-2" /' > cVegpft.partab
            ;;
        cVegCrop )  # vegetation carbon in crops  - not for CMOR
            cdo mulc,0.01201 -ifthen divcrop -mul -sellevel,11 cVeg_tiles.tmp divcrop  cVegCrop.tmp
            echo '&parameter name=cVegCrop long_name="Carbon Mass in Vegetation on Crop Tiles" units="kg m-2" /' > cVegCrop.partab
            ;;
        cVegGrass )  # vegetation carbon in grass
            cdo mulc,0.01201 -ifthen divgrass -mul -vertsum -sellevel,7/8 cVeg_tiles.tmp divgrass  cVegGrass.tmp
            echo '&parameter name=cVegGrass long_name="Carbon Mass in Vegetation on Grass Tiles" units="kg m-2" /' \
                 > cVegGrass.partab
            ;;
        cVegLut )  # Veg carbon of land use tiles
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo -f grb setlevel,1 -ifthen divpsl -mulc,0.01201 -mul -vertsum -sellevel,1/8  cVeg_tiles.tmp divpsl cVegLut.psl.tmp
            cdo -f grb setlevel,2 -ifthen divcrp -mulc,0.01201 -mul          -sellevel,11   cVeg_tiles.tmp divcrp cVegLut.crp.tmp
            cdo -f grb setlevel,3 -ifthen divpst -mulc,0.01201 -mul -vertsum -sellevel,9/10 cVeg_tiles.tmp divpst cVegLut.pst.tmp
            cdo -f grb setlevel,4 -ifthen divurb              cVegLut.crp.tmp                                     cVegLut.urb.tmp
            [[ ! -f cVegLut.tmp1 ]] || rm cVegLut.tmp1
            cdo -f nc merge  cVegLut.psl.tmp cVegLut.crp.tmp cVegLut.pst.tmp cVegLut.urb.tmp  cVegLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile    cVegLut.tmp1 cVegLut.tmp
           echo '&parameter name=cVegLut long_name="carbon in vegetation on land use tiles" units="kg m-2" /' > cVegLut.partab
            ;;
        cVegPast )  # vegetation carbon in pastures  - not for CMOR
            cdo mulc,0.01201 -ifthen divpast -mul -vertsum -sellevel,9/10 cVeg_tiles.tmp divpast  cVegPast.tmp
            echo '&parameter name=cVegPast long_name="Carbon Mass in Vegetation on Pasture Tiles" units="kg m-2" /' \
                 > cVegPast.partab
            ;;
        cVegShrub )  # vegetation carbon in shrubs
            cdo mulc,0.01201 -ifthen divshrub -mul -vertsum -sellevel,5/6 cVeg_tiles.tmp divshrub  cVegShrub.tmp
            echo '&parameter name=cVegShrub long_name="Carbon Mass in Vegetation on Shrub Tiles" units="kg m-2" /' \
                 > cVegShrub.partab
            ;;
        cVegTree )  # vegetation carbon in trees
            cdo mulc,0.01201 -ifthen divtree -mul -vertsum -sellevel,1/4 cVeg_tiles.tmp divtree  cVegTree.tmp
            echo '&parameter name=cVegTree long_name="Carbon Mass in Vegetation on Tree Tiles" units="kg m-2" /' > cVegTree.partab
            ;;
        devapotrans ) # daily evapotranspiration
            cdo mulc,-1 -selcode,44 outdata/land_daily.grb  devapotrans.tmp
            echo '&parameter name=devapotrans long_name="Total Evapotranspiration" units="kg m-2 s-1" /' > devapotrans.partab
            ;;
        dfFire ) # daily C emissions from fire
            cdo mulc,0.272912 -selcode,56 outdata/veg_daily.grb  dfFire.tmp
            echo '&parameter name=dfFire long_name="Carbon Mass Flux into Atmosphere due to CO2 Emission' \
                 'from Fire" units="kgC m-2 s-1" /' > dfFire.partab
            ;;
        dgpp ) # daily gross primary production
            cdo mulc,0.01201 -vertsum dgpp_tiles.tmp dgpp.tmp
            echo '&parameter name=dgpp long_name="Carbon Mass Flux out of Atmosphere due to' \
                 'Gross Primary Production on Land" units="kgC m-2 s-1" /' > dgpp.partab
            ;;
        dgpppft ) # daily gross primary production per PFT
            cdo mulc,0.01201 dgpp_tiles.tmp dgpp_tiles_C.tmp
            cdo -f nc splitlevel dgpp_tiles_C.tmp dgpppft_
            cdo setlevel,1 -ifthen slm -mulc,0. dgpppft_000001.nc dgpppft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. dgpppft_000001.nc dgpppft02.tmp  # glacier
            cdo setlevel,3                      dgpppft_000001.nc dgpppft03.tmp
            cdo setlevel,4                      dgpppft_000002.nc dgpppft04.tmp
            cdo setlevel,5                      dgpppft_000003.nc dgpppft05.tmp
            cdo setlevel,6                      dgpppft_000004.nc dgpppft06.tmp
            cdo setlevel,7                      dgpppft_000005.nc dgpppft07.tmp
            cdo setlevel,8                      dgpppft_000006.nc dgpppft08.tmp
            cdo setlevel,9                      dgpppft_000007.nc dgpppft09.tmp
            cdo setlevel,10                     dgpppft_000008.nc dgpppft10.tmp
            cdo setlevel,11                     dgpppft_000009.nc dgpppft11.tmp
            cdo setlevel,12                     dgpppft_000010.nc dgpppft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask dgpppft_000011.nc dgpppft14.tmp  # c4 crop
            cdo setlevel,13 -sub dgpppft_000011.nc  dgpppft14.tmp dgpppft13.tmp  # c3 crop
            [[ ! -f dgpppft.tmp1 ]] || rm dgpppft.tmp1
            cdo merge dgpppft??.tmp  dgpppft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT dgpppft.tmp1 dgpppft.tmp
            echo '&parameter name=dgpppft long_name="Carbon Mass Flux out of Atmosphere due to' \
                 'Gross Primary Production on Land" units="kgC m-2 s-1" /' > dgpppft.partab
            ;;
        dhfss ) # daily latent heat flux
            cdo mulc,-1 -selcode,47 outdata/land_daily.grb  dhfss.tmp
            echo '&parameter name=dhfss long_name="Surface Upward Sensible Heat Flux" units="W m-2" /' > dhfss.partab
            ;;
        dlai ) # daily leaf area index
            echo dlai
            cdo vertsum -mul cover_fract -selcode,107 outdata/main_daily.grb dlai.tmp
            echo '&parameter name=dlai long_name="Leaf Area Index" units="1" /' > dlai.partab
            ;;
        dlaipft )  # daily LAI per PFT (TRENDY)
            cdo selcode,107 outdata/main_daily.grb  dlaipft.tmp
            cdo -f nc splitlevel dlaipft.tmp dlaipft_
            cdo setlevel,1 -ifthen slm -mulc,0. dlaipft_000001.nc dlaipft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. dlaipft_000001.nc dlaipft02.tmp  # glacier
            cdo setlevel,3                      dlaipft_000001.nc dlaipft03.tmp
            cdo setlevel,4                      dlaipft_000002.nc dlaipft04.tmp
            cdo setlevel,5                      dlaipft_000003.nc dlaipft05.tmp
            cdo setlevel,6                      dlaipft_000004.nc dlaipft06.tmp
            cdo setlevel,7                      dlaipft_000005.nc dlaipft07.tmp
            cdo setlevel,8                      dlaipft_000006.nc dlaipft08.tmp
            cdo setlevel,9                      dlaipft_000007.nc dlaipft09.tmp
            cdo setlevel,10                     dlaipft_000008.nc dlaipft10.tmp
            cdo setlevel,11                     dlaipft_000009.nc dlaipft11.tmp
            cdo setlevel,12                     dlaipft_000010.nc dlaipft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask dlaipft_000011.nc dlaipft14.tmp  # c4 crop
            cdo setlevel,13 -sub dlaipft_000011.nc  dlaipft14.tmp dlaipft13.tmp  # c3 crop
            [[ ! -f dlaipft.tmp1 ]] || rm dlaipft.tmp1
            cdo merge dlaipft??.tmp  dlaipft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT dlaipft.tmp1 dlaipft.tmp
            echo '&parameter name=dlaipft long_name="Leaf Area Index" units="1" /' > dlaipft.partab
            ;;
        dnbp ) # daily net biosphere production
            # NBP=NPP+RH+fFire+fgrazing+fHarvest+fLcc (fGrazing is included in RH)
            [[ ! -f dnbp.tmp ]] || rm dnbp.tmp
            cdo enssum -mulc,0.01201  -vertsum    dnpp_tiles.tmp        \
                       -mulc,-0.01201 -vertsum    drh_tiles.tmp         \
                       -mulc,-0.272912        -selcode,56  outdata/veg_daily.grb  \
                       -mulc,-0.01201         -selcode,163 outdata/main_daily.grb \
                       -mulc,-0.01201         -selcode,211 outdata/veg_daily.grb  \
                       -mulc,-0.01201        -selcode,162  outdata/main_daily.grb   dnbp.tmp
            echo '&parameter name=dnbp long_name="Carbon Mass Flux out of Atmosphere due to' \
                 'Net Biospheric Production on Land" units="kgC m-2 s-1" /'  > dnbp.partab
            ;;
        dra ) # daily autotrophic respiration
            cdo mulc,0.01201 -vertsum dra_tiles.tmp  dra.tmp
            echo '&parameter name=dra long_name="Carbon Mass Flux into Atmosphere due to' \
                 'Autotrophic (Plant) Respiration on Land" units="kgC m-2 s-1" /' > dra.partab
            ;;
        drh ) # haily heterotrophic respiration
            cdo mulc,0.01201 -vertsum drh_tiles.tmp  drh.tmp
            echo '&parameter name=drh long_name="Carbon Mass Flux into Atmosphere due to Heterotrophic' \
                 'Respiration on Land" units="kgC m-2 s-1" /' > drh.partab
            ;;
        dhfls ) # daily sensible heat flux
            echo dhfls
            cdo mulc,-1 -selcode,49 outdata/land_daily.grb  dhfls.tmp
            echo '&parameter name=dhfls long_name="Surface Upward Latent Heat Flux" units="W m-2" /' > dhfls.partab
            ;;
        dskinT ) # daily skin temperature (land surface temperature here)
            echo dskinT
            cdo selcode,35 outdata/land_daily.grb  dskinT.tmp
            echo '&parameter name=dskinT long_name="Surface Temperature" units="K" /' > dskinT.partab
            ;;
        es )  # bare Soil evaporation
            cdo mulc,-1 -selcode,79 ${inf_land}  es.tmp
            echo '&parameter name=es long_name="Bare Soil Evaporation" units="kg m-2 s-1" /' > es.partab
            ;;
        et )  # evapotranspiration
            cdo mulc,-1 -selcode,44 ${inf_land}  et.tmp
            echo '&parameter name=et long_name="Total Evapotranspiration" units="kg m-2 s-1" /' > et.partab
            ;;
        evapotrans )  # evapotranspiration (CRESCENDO)  - not for CMOR  (same as 'et') 
            cdo mulc,-1 -selcode,44 ${inf_land}  evapotrans.tmp
            echo '&parameter name=evapotrans long_name="Total Evapotranspiration" units="kg m-2 s-1" /' > evapotrans.partab
            ;;
        evspsbl )  # evaporation
            cdo mulc,-1 -selcode,182 ${inf_BOT}  evspsbl.tmp
            echo '&parameter name=evspsbl long_name="Evaporation" units="kg m-2 s-1" /' > evspsbl.partab
            ;;
        fapar )  # fapar (CRESCENDO)  - not for CMOR
            cdo -f nc max -const,0.,${inf_land} -selcode,148 ${inf_land} fapar.tmp1   # APAR should not be negative  (grib)
            cdo -f nc gtc,1.e-10 -selcode,149 ${inf_land} fapar.tmp2                  # mask with PAR greater than 1.e-10  (grib)
            cdo -f nc ifthen fapar.tmp2 -div fapar.tmp1 -selcode,149 ${inf_land} fapar.tmp
            echo '&parameter name=fapar long_name="Fraction of Absorbed Photosynthetically Active Radiation" units="1" /' \
                 > fapar.partab           
            ;;
        fBNF )  # nitrogen fixation
            cdo mulc,0.0140068 -vertsum -selcode,77 ${inf_nitro} fBNF.tmp
            echo '&parameter name=fBNF long_name="Biological Nitrogen Fixation" units="kgN m-2 s-1" /' > fBNF.partab
            ;;
        fco2antt )  # C to the atmosphere due to anthropogenic emissions   (only with emission driven scenarios)
                    #   (code 20: emissions from fossil fuel, land use change, harvest)
            cdo mulc,0.272912 -selcode,20 ${inf_co2} fco2antt.tmp
            echo '&parameter name=fco2antt long_name="Carbon Mass Flux into Atmosphere Due to' \
                 'All Antropogenic Emissions of CO2" units="kgC m-2 s-1" /'                      > fco2antt.partab
           ;;
        fco2fos )  # C to the atmosphere due to fossil fuel emissions (only with emission driven scenarios)
            cdo mulc,0.272912 -selcode,11 ${inf_co2} fco2fos.tmp
            echo '&parameter name=fco2fos long_name="Carbon Mass Flux into Atmosphere Due to' \
                 'Fossil Fuel Emissions of CO2" units="kgC m-2 s-1" /'                          > fco2fos.partab
           ;;
        fco2nat )  # C to the atmosphere from natural sources  (code 5: co2 flux from land and from ocean)
            # Note: the land flux of code 5 contains the (non-natural) crop harvest, which needs to be substracted
            cdo sub  -mulc,0.272912 -selcode,5   ${inf_co2} \
                     -mulc,0.01201  -selcode,211 ${inf_veg}  fco2nat.tmp
            echo '&parameter name=fco2nat long_name="Surface Carbon Mass Flux into the' \
                 'Atmosphere Due to Natural Sources" units="kgC m-2 s-1" /'               > fco2nat.partab
           ;;
        fDeforestToProduct )  # carbon flux to product pool due to Deforestation
            [[ -f fDeforestToProduct.tmp1 ]] && rm fDeforestToProduct.tmp1
            cdo enssum -selcode,228 ${inf_veg} \
                       -selcode,230 ${inf_veg} \
                       -selcode,231 ${inf_veg}  fDeforestToProduct.tmp1
            cdo mulc,0.01201 fDeforestToProduct.tmp1  fDeforestToProduct.tmp
            echo '&parameter name=fDeforestToProduct long_name="Deforested Biomass that goes into' \
                 'Product Pools as a Result of Anthropogenic Land Use Change" units="kgC m-2 s-1" /' > fDeforestToProduct.partab
            ;;
        fFire )  # carbon flux due to fire
                 # fFire includes natural and human ignition fires, but not fires for land use change
            # Note: fire events are shifted by one day in the co2 stream with respect to the veg stream, with the veg stream 
            #       leading. This leads to differences in the monthly output. For consistency you should use the veg stream data. 
            cdo mulc,0.272912 -selcode,56 ${inf_veg}  fFire.tmp
            echo '&parameter name=fFire long_name="Carbon Mass Flux into Atmosphere due to CO2 Emission' \
                 'from Fire" units="kgC m-2 s-1" /'                                                        > fFire.partab
            ;;
        fGrazing )  # carbon flux due to grazing
            cdo mulc,-0.01201 -vertsum -selcode,143 ${inf_veg}  fGrazing.tmp
            echo '&parameter name=fGrazing long_name="Carbon Mass Flux into Atmosphere due to' \
                 'Grazing on Land" units="kgC m-2 s-1" /'                                        > fGrazing.partab
            ;;
        fHarvest )  # carbon flux due to crop harvest
            # Note: This is the flux from the product pools to the atmosphere due to crop AND WOOD harvest. It differs from 
            #       fHarvestToAtmos, which is the harvest flux going directly to the atmosphere. fHarvestToAtmos is zero for jsbach
            #       (with anthropogenic pools, i.e. lcc_scheme == 2), as C from harvest goes to the product pools and later 
            #       decomposes.
            cdo mulc,0.01201 -add -selcode,211 ${inf_veg} -selcode,163 ${inf_main}  fHarvest.tmp
            echo '&parameter name=fHarvest long_name="Carbon Mass Flux into Atmophere due to' \
                 'Crop Harvesting" units="kgC m-2 s-1" /'                                       > fHarvest.partab
            ;;
        fHarvestToProduct )  #  harvested biomas going to produc pools  
            [[ ! -f fHarvestToProduct.tmp1 ]] || rm fHarvestToProduct.tmp1
            cdo enssum -selcode,158 ${inf_veg} \
                       -selcode,242 ${inf_veg} \
                       -selcode,243 ${inf_veg} \
                       -selcode,249 ${inf_veg}  fHarvestToProduct.tmp1
            cdo -mulc,0.01201 fHarvestToProduct.tmp1  fHarvestToProduct.tmp
            echo '&parameter name=fHarvestToProduct long_name="Harvested biomass that goes into' \
                 'product pool" units="kgC m-2 s-1" /'                                             > fHarvestToProduct.partab
            ;;
        fLuc )  # carbon flux due to land use change
            # Note: This is the flux from the product pools to the atmosphere due to land use change. It differs from
            #       fDeforestToAtmos, which is the direct flux from living vegetation to the atmosphere (compare fig.6 in 
            #       Jones et al. 2016). fDeforestToAtmos is zero in simulation with anthropogenic C pools (lcc_schme == 2).
            cdo mulc,0.01201  -selcode,162 ${inf_main}  fLuc.tmp
            echo '&parameter name=fLuc long_name="Net Carbon Mass Flux into Atmosphere due to' \
                 'Land Use Change" units="kgC m-2 s-1" /'                                        > fLuc.partab
            echo 'fLuc.tmp created'
            ;;
        fN2O )  # N2O flux   # Conversion from 'mikro Mol(N)/m2' to 'kg(N)/m2'   => factor: 0.0140068 * 1.e-06
                             # Conversion to netcdf needed already here, as numbers get too small for grib.
            cdo -f nc mulc,0.0140068 -mulc,1.e-06 -vertsum -selcode,212 ${inf_nitro} fN2O.tmp
            echo '&parameter name=fN2O long_name="Total Land N2O Flux" units="kgN m-2 s-1" /' > fN2O.partab
            ;;
        fNAnthDisturb ) # N flux to the atmosphere due to human activities
            # Note: in contrast to the carbon flux fAnthDisturb the corresponding N flux is non zero, as there are no nitrogen 
            #       product pools, except for the crop harvest pool.  
            cdo mulc,0.0140068 -add -selcode,129 ${inf_nitro} -selcode,130 ${inf_nitro}  fNAnthDisturb.tmp
            echo '&parameter name=fNAnthDisturb long_name="nitrogen mass flux out of land due to any human' \
                 'activity" units="kgN m-2 s-1" /'                                                            > fNAnthDisturb.partab
            ;;
        fNdep )  # nitrogen deposition
            cdo selcode,236 ${inf_nitro} fNdep.tmp   # already in [kg(N)/m2s]
            echo '&parameter name=fNdep long_name="Dry and Wet Deposition of Reactive Nitrogen' \
                 'onto Land" units="kgN m-2 s-1" /'                                               > fNdep.partab
            ;;
        fNgas )  # nitrogen lost to the atmosphere  (fNgas = fNgasFire + fNgasNonFire)
            cdo mulc,0.0140068 -add -divc,86400. -selcode,62  ${inf_veg}   \
                               -add     -vertsum -selcode,80  ${inf_nitro} \
                            -mulc,1.e-6 -vertsum -selcode,212 ${inf_nitro} fNgas.tmp
            echo '&parameter name=fNgas long_name="Total Nitrogen lost to the atmosphere" units="kgN m-2 s-1" /' > fNgas.partab
            ;;
        fNgasFire ) # nitrogen flux to the atmosphere due to fire
                    #  unit conversion [mol(N) m-2 day-1] -> [kg(N) m-2 s-1]
            cdo mulc,0.0140068 -divc,86400 -selcode,62 ${inf_veg} fNgasFire.tmp
            echo '&parameter name=fNgasFire long_name="Total N lost to the atmosphere from fire" ' \
            echo 'units="kgN m-2 s-1" /'                                                             > fNgasFire.partab
            ;;
        fNgasNonFire ) # nitrogen flux to the atmosphere without fire
            cdo mulc,0.0140068 -add -vertsum -selcode,80  ${inf_nitro} \
                        -mulc,1.e-6 -vertsum -selcode,212 ${inf_nitro}  fNgasNonFire.tmp
            echo '&parameter name=fNgasNonFire long_name="Total N lost to the atmosphere from' \
                 'all processes except fire" units="kgN m-2 s-1" /'                              > fNgasNonFire.partab
            ;;
        fNleach )  # nitrogen lost by leaching
            cdo mulc,0.0140068 -vertsum -selcode,81 ${inf_nitro} fNleach.tmp
            echo '&parameter name=fNleach long_name="Total N loss to leaching or runoff" units="kgN m-2 s-1" /' > fNleach.partab
            ;;
        fNloss )  # nitrogen loss  (fNloss = fNgas + fNleach)
            [[ ! -f fNloss.tmp1 ]] || rm fNloss.tmp1
            cdo enssum -divc,86400. -selcode,62  ${inf_veg}   \
                           -vertsum -selcode,80  ${inf_nitro} \
                           -vertsum -selcode,81  ${inf_nitro} \
               -mulc,1.e-6 -vertsum -selcode,212 ${inf_nitro}  fNloss.tmp1
            cdo mulc,0.0140068 fNloss.tmp1 fNloss.tmp
            echo '&parameter name=fNloss long_name="Total N lost" units="kgN m-2 s-1" /' > fNloss.partab
            ;;
        fNnetmin )  # net nitrogen release
            [[ ! -f fNnetmin.tmp1 ]] || rm fNnetmin.tmp1
            cdo enssum -vertsum -selcode,70 ${inf_nitro} \
                       -vertsum -selcode,71 ${inf_nitro} \
                       -vertsum -selcode,72 ${inf_nitro} \
                       -mulc,-1 -vertsum -selcode,74 ${inf_nitro}  fNnetmin.tmp1
            cdo mulc,0.0140068 fNnetmin.tmp1 fNnetmin.tmp
            echo '&parameter name=fNnetmin long_name="Net Nitrogen Release from Soil and Litter" ' \
                 'units="kgN m-2 s-1" /'                                                             > fNnetmin.partab
            ;;
        fNProduct ) # N flux to product pool
            cdo mulc,0.0140068 -vertsum -selcode,158 ${inf_nitro}  fNProduct.tmp
            echo '&parameter name=fNProduct long_name="Deforested or harvested biomass as a result of' \
                 'anthropogenic land use or change" units="kgN m-2 s-1" /'                               > fNProduct.partab
            ;;
        fNup )  # plant nitrogen uptake
            cdo mulc,0.0140068 -vertsum -selcode,73 ${inf_nitro}  fNup.tmp
            echo '&parameter name=fNup long_name="Total Plant Nitrogen Uptake" units="kgN m-2 s-1" /' > fNup.partab
            ;;
        fProductDecomp )  # decomposition of product pools
            [[ ! -f fProductDecomp.tmp1 ]] || rm fProductDecomp.tmp1
            cdo enssum -selcode,211 ${inf_veg} \
                       -selcode,236 ${inf_veg} \
                       -selcode,238 ${inf_veg} \
                       -selcode,239 ${inf_veg} \
                       -selcode,246 ${inf_veg} \
                       -selcode,247 ${inf_veg} \
                       -selcode,251 ${inf_veg}  fProductDecomp.tmp1
            cdo mulc,0.01201 fProductDecomp.tmp1 fProductDecomp.tmp
            echo '&parameter name=fProductDecomp long_name="Decomposition out of Product Pools' \
                 'to CO2 in Atmosphere" units="kgC m-2 s-1" /'                                    > fProductDecomp.partab
            ;;
        fracInLut )  # grid cell fraction transferd into LU tile
            cdo -f grb -setlevel,1 -ifthen slm -expr,'fracInLut=100.*(crp*crop2natural+pst*pasture2natural)' \
                LUH_transitions.tmp  fracInLut.psl.tmp
            cdo -f grb -setlevel,2 -ifthen slm -expr,'fracInLut=100.*(pst*pasture2crop+nat*natural2crop)'    \
                LUH_transitions.tmp  fracInLut.crp.tmp
            cdo -f grb -setlevel,3 -ifthen slm -expr,'fracInLut=100.*(crp*crop2pasture+nat*natural2pasture)' \
                LUH_transitions.tmp  fracInLut.pst.tmp
            cdo -f grb -setlevel,4 -ifthen slm -mulc,0. fracInLut.pst.tmp   fracInLut.urb.tmp
            [[ ! -f  fracInLut.tmp1 ]] || rm fracInLut.tmp1
            cdo -f nc merge fracInLut.psl.tmp fracInLut.crp.tmp fracInLut.pst.tmp fracInLut.urb.tmp fracInLut.tmp1
            ncrename -h -O -d plev,Tile -v plev,Tile    fracInLut.tmp1 fracInLut.tmp
            echo '&parameter name=fracInLut long_name="annual gross fraction that was transferred into this' \
                 'tile from other land use tiles" units="%" '                                                 > fracInLut.partab
            ;;
        fracLut )  # fraction of LUMIP land use tiles 
                   # fracLut(psl) + fracLut(crp) + fracLut(pst) + fracLut(urb) + glac (+ lakes) = 1
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo -f grb -setlevel,1  psl.tmp                      fracLut.psl.tmp
            cdo -f grb -setlevel,2  crp.tmp                      fracLut.crp.tmp
            cdo -f grb -setlevel,3  pst.tmp                      fracLut.pst.tmp
            cdo -f grb -setlevel,4 -ifthen slm -mulc,0. pst.tmp  fracLut.urb.tmp
            [[ ! -f  fracLut.tmp1 ]] || rm fracLut.tmp1
            cdo -f nc merge fracLut.psl.tmp fracLut.crp.tmp fracLut.pst.tmp fracLut.urb.tmp fracLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile    fracLut.tmp1 fracLut.tmp
            echo '&parameter name=fracLut long_name="fraction of grid cell for each land use tile" units="%" /' > fracLut.partab
            ;;
        fracOutLut ) # grid cell fraction transferd from LU tile into another tile
            cdo -f grb -setlevel,1 -ifthen slm -expr,'fracOutLut=100.*nat*(natural2crop+natural2pasture)' \
                LUH_transitions.tmp fracOutLut.psl.tmp
            cdo -f grb -setlevel,2 -ifthen slm -expr,'fracOutLut=100.*crp*(crop2pasture+crop2natural)'    \
                LUH_transitions.tmp fracOutLut.crp.tmp
            cdo -f grb -setlevel,3 -ifthen slm -expr,'fracOutLut=100.*pst*(pasture2crop+pasture2natural)' \
                LUH_transitions.tmp fracOutLut.pst.tmp
            cdo -f grb -setlevel,4 -ifthen slm -mulc,0. fracOutLut.pst.tmp   fracOutLut.urb.tmp
            [[ ! -f  fracOutLut.tmp1 ]] || rm fracOutLut.tmp1
            cdo -f nc merge fracOutLut.psl.tmp fracOutLut.crp.tmp fracOutLut.pst.tmp fracOutLut.urb.tmp fracOutLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile    fracOutLut.tmp1 fracOutLut.tmp
            echo '&parameter name=fracOutLut long_name="annual gross fraction of land use tile that was' \
                 ' transferred into other land use tiles" units="%" '                                     > fracOutLut.partab
            ;;
        gpp )  # total GPP gpp = npp + ra 
               #           gpp = gppCrop*cropFrac + gppPast*pastureFrac + gppGrass*grassFrac + gppShrub*shrubFrac + gppTree*treeFrac
               #           gpp = SUM(gppLut(:) * fracLut(:))
            cdo mulc,0.01201 -vertsum gpp_tiles.tmp  gpp.tmp
            echo '&parameter name=gpp long_name="Carbon Mass Flux out of Atmosphere due to' \
                 'Gross Primary Production on Land" units="kgC m-2 s-1" /'                    > gpp.partab
            ;;
        gpppft )  # gpp per PFT (TRENDY)  - not for CMOR          -- copied behaviour from LAIpft
            cdo mulc,0.01201 gpp_tiles.tmp gpp_tiles_C.tmp
            cdo -f nc splitlevel gpp_tiles_C.tmp gpppft_
            cdo setlevel,1 -ifthen slm -mulc,0. gpppft_000001.nc gpppft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. gpppft_000001.nc gpppft02.tmp  # glacier
            cdo setlevel,3                      gpppft_000001.nc gpppft03.tmp
            cdo setlevel,4                      gpppft_000002.nc gpppft04.tmp
            cdo setlevel,5                      gpppft_000003.nc gpppft05.tmp
            cdo setlevel,6                      gpppft_000004.nc gpppft06.tmp
            cdo setlevel,7                      gpppft_000005.nc gpppft07.tmp
            cdo setlevel,8                      gpppft_000006.nc gpppft08.tmp
            cdo setlevel,9                      gpppft_000007.nc gpppft09.tmp
            cdo setlevel,10                     gpppft_000008.nc gpppft10.tmp
            cdo setlevel,11                     gpppft_000009.nc gpppft11.tmp
            cdo setlevel,12                     gpppft_000010.nc gpppft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask gpppft_000011.nc gpppft14.tmp  # c4 crop
            cdo setlevel,13 -sub gpppft_000011.nc  gpppft14.tmp gpppft13.tmp  # c3 crop
            [[ ! -f gpppft.tmp1 ]] || rm gpppft.tmp1
            cdo merge gpppft??.tmp  gpppft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT gpppft.tmp1 gpppft.tmp
            echo '&parameter name=gpppft long_name="Carbon Mass Flux out of Atmosphere due to' \
                 'Gross Primary Production on Land" units="kgC m-2 s-1" /' > gpppft.partab
            ;;
        gppCrop )  # GPP of crops  - not for CMOR
            cdo mulc,0.01201 -ifthen divcrop -mul -sellevel,11 gpp_tiles.tmp divcrop  gppCrop.tmp
            echo '&parameter name=gppCrop long_name="Gross Primary Production on Crop Tiles" units="kgC m-2 s-1" /' > gppCrop.partab
            ;;
        gppGrass )  # GPP of grass
            cdo mulc,0.01201 -ifthen divgrass -mul -vertsum -sellevel,7/8 gpp_tiles.tmp divgrass  gppGrass.tmp
            echo '&parameter name=gppGrass long_name="Gross Primary Production' \
                 'on Grass Tiles" units="kgC m-2 s-1" /'                          > gppGrass.partab
            ;;
        gppLut )  # GPP on land use tiles
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo -f grb setlevel,1 -ifthen divpsl -mulc,0.01201 -mul -vertsum -sellevel,1/8  gpp_tiles.tmp divpsl gppLut.psl.tmp
            cdo -f grb setlevel,2 -ifthen divcrp -mulc,0.01201 -mul          -sellevel,11   gpp_tiles.tmp divcrp gppLut.crp.tmp
            cdo -f grb setlevel,3 -ifthen divpst -mulc,0.01201 -mul -vertsum -sellevel,9/10 gpp_tiles.tmp divpst gppLut.pst.tmp
            cdo -f grb setlevel,4 -ifthen divurb        gppLut.crp.tmp                                           gppLut.urb.tmp
            [[ ! -f gppLut.tmp1 ]] || rm gppLut.tmp1
            cdo -f nc merge  gppLut.psl.tmp gppLut.crp.tmp gppLut.pst.tmp gppLut.urb.tmp  gppLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile    gppLut.tmp1 gppLut.tmp
            echo '&parameter name=gppLut long_name="gross primary production on land use tile" units="kgC m-2 s-1" /' > gppLut.partab
            ;;
        gppPast )  # GPP of pastures  - not for CMOR
            cdo mulc,0.01201 -ifthen divpast -mul -vertsum -sellevel,9/10 gpp_tiles.tmp divpast  gppPast.tmp
            echo '&parameter name=gppPast long_name="Gross Primary Production' \
                 'on Pasture Tiles" units="kgC m-2 s-1" /'                       > gppPast.partab
            ;;
        gppShrub )  # GPP of shrub
            cdo mulc,0.01201 -ifthen divshrub -mul -vertsum -sellevel,5/6 gpp_tiles.tmp divshrub  gppShrub.tmp
            echo '&parameter name=gppShrub long_name="Gross Primary Production' \
                 'on Shrub Tiles" units="kgC m-2 s-1" /'                          > gppShrub.partab
            ;;
        gppTree ) # GPP of trees
            cdo mulc,0.01201 -ifthen divtree -mul -vertsum -sellevel,1/4 gpp_tiles.tmp divtree  gppTree.tmp
            echo '&parameter name=gppTree long_name="Gross Primary Production on Tree Tiles" units="kgC m-2 s-1" /' > gppTree.partab
            ;;
        grassFrac )  # natural grass fraction    (grassFrac = grassFracC3 + grassFracC4)
            cp grass.tmp  grassFrac.tmp
            echo '&parameter name=grassFrac long_name="Natural Grass Fraction" units="%" /' > grassFrac.partab
            ;;
        grassFracC3 )  # C3 grass fraction   (only natural)
            cp c3grass.tmp  grassFracC3.tmp
            echo '&parameter name=grassFracC3 long_name="C3 grass area percentage" units="%" /' > grassFracC3.partab
            ;;
        grassFracC4 )  # C4 grass fraction   (only natural)
            cp c4grass.tmp  grassFracC4.tmp
            echo '&parameter name=grassFracC4 long_name="C4 grass area percentage" units="%" /' > grassFracC4.partab
            ;;
        grassFracPot )  # potential grass fraction  - not for CMOR
            cdo mulc,100. -ifthen slm -vertsum -sellevel,7/8  box_cover_fract_pot grassFracPot.tmp
            echo '&parameter name=grassFracPot long_name="Potential natural Grass Fraction" units="%" /' > grassFracPot.partab
            ;;
        hfls )  # latent heat flux
            if [[ -f ${inf_BOT} ]]; then
                cdo mulc,-1 -selcode,147 ${inf_BOT}  hfls.tmp
            else
                cdo mulc,-1 -selcode,49 ${inf_land}  hfls.tmp
            fi
            echo '&parameter name=hfls long_name="Surface Upward Latent Heat Flux" units="W m-2" /' > hfls.partab
            ;;
        hfss )  # sensible heat flux
            cdo mulc,-1 -selcode,146 ${inf_BOT}  hfss.tmp
            echo '&parameter name=hfss long_name="Surface Upward Sensible Heat Flux" units="W m-2" /' > hfss.partab
            ;;
        hurs )  # 2m relative humidity
            cdo selcode,55 ${inf_BOT}  hurs.tmp
            echo '&parameter name=hurs long_name="Near-Surface Relative Humidity" units="1" /' > hurs.partab
            ;;
        huss )  # 2m specific humidity
            cdo selcode,54 ${inf_BOT}  huss.tmp
            echo '&parameter name=huss long_name="Near-Surface Specific Humidity" units="1" /' > huss.partab
            ;;
        lai )  # LAI
            cdo selcode,107 ${inf_land}  lai.tmp
            echo '&parameter name=lai long_name="Leaf Area Index" units="1" /' > lai.partab
            ;;
        lai_pft )  # LAI per PFT (CRESCENDO)  - not for CMOR
            cdo selcode,107 ${inf_main}  lai_pft.tmp
            cdo -f nc splitlevel lai_pft.tmp lai_pft_
            cdo setlevel,1 -ifthen slm -mulc,0. lai_pft_000001.nc lai_pft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. lai_pft_000001.nc lai_pft02.tmp  # glacier
            cdo setlevel,3                      lai_pft_000001.nc lai_pft03.tmp
            cdo setlevel,4                      lai_pft_000002.nc lai_pft04.tmp
            cdo setlevel,5                      lai_pft_000003.nc lai_pft05.tmp
            cdo setlevel,6                      lai_pft_000004.nc lai_pft06.tmp
            cdo setlevel,7                      lai_pft_000005.nc lai_pft07.tmp
            cdo setlevel,8                      lai_pft_000006.nc lai_pft08.tmp
            cdo setlevel,9                      lai_pft_000007.nc lai_pft09.tmp
            cdo setlevel,10                     lai_pft_000008.nc lai_pft10.tmp
            cdo setlevel,11                     lai_pft_000009.nc lai_pft11.tmp
            cdo setlevel,12                     lai_pft_000010.nc lai_pft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask lai_pft_000011.nc lai_pft14.tmp  # c4 crop
            cdo setlevel,13 -sub lai_pft_000011.nc  lai_pft14.tmp lai_pft13.tmp  # c3 crop
            [[ ! -f lai_pft.tmp1 ]] || rm lai_pft.tmp1
            cdo merge lai_pft??.tmp  lai_pft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT lai_pft.tmp1 lai_pft.tmp
            echo '&parameter name=lai_pft long_name="Leaf Area Index" units="1" /' > lai_pft.partab
            ;;
        laipft )  # LAI per PFT (TRENDY)  - not for CMOR
            cdo selcode,107 ${inf_main}  lai_pft.tmp
            cdo -f nc splitlevel lai_pft.tmp laipft_
            cdo setlevel,1 -ifthen slm -mulc,0. laipft_000001.nc laipft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. laipft_000001.nc laipft02.tmp  # glacier
            cdo setlevel,3                      laipft_000001.nc laipft03.tmp
            cdo setlevel,4                      laipft_000002.nc laipft04.tmp
            cdo setlevel,5                      laipft_000003.nc laipft05.tmp
            cdo setlevel,6                      laipft_000004.nc laipft06.tmp
            cdo setlevel,7                      laipft_000005.nc laipft07.tmp
            cdo setlevel,8                      laipft_000006.nc laipft08.tmp
            cdo setlevel,9                      laipft_000007.nc laipft09.tmp
            cdo setlevel,10                     laipft_000008.nc laipft10.tmp
            cdo setlevel,11                     laipft_000009.nc laipft11.tmp
            cdo setlevel,12                     laipft_000010.nc laipft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask laipft_000011.nc laipft14.tmp  # c4 crop
            cdo setlevel,13 -sub laipft_000011.nc  laipft14.tmp laipft13.tmp  # c3 crop
            [[ ! -f laipft.tmp1 ]] || rm laipft.tmp1
            cdo merge laipft??.tmp  laipft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT laipft.tmp1 laipft.tmp
            echo '&parameter name=laipft long_name="Leaf Area Index" units="1" /' > laipft.partab
            ;;
        laiLut )  # LAI on land use tiles
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo selcode,107 ${inf_main}  laiLut.tmp
            cdo -f grb ifthen divpsl -setlevel,1 -mul -vertsum -sellevel,1/8  -mul laiLut.tmp cover_fract divpsl laiLut.psl.tmp
            cdo -f grb                setlevel,2               -sellevel,11        laiLut.tmp                    laiLut.crp.tmp
            cdo -sellevel,9    laiLut.tmp  laiLut.pc3.tmp
            cdo -sellevel,10   laiLut.tmp  laiLut.pc4.tmp
            cdo div c3past.tmp -add c3past.tmp c4past.tmp laiLut.fc3.tmp
            cdo div c4past.tmp -add c3past.tmp c4past.tmp laiLut.fc4.tmp
            cdo -f grb setlevel,3 -add  -mul laiLut.pc3.tmp laiLut.fc3.tmp  -mul laiLut.pc4.tmp laiLut.fc4.tmp  laiLut.pst.tmp
            cdo -f grb setlevel,4         -ifthen divurb     laiLut.crp.tmp                                     laiLut.urb.tmp
            [[ ! -f laiLut.tmp1 ]] || rm laiLut.tmp1
            cdo -f nc merge  laiLut.psl.tmp laiLut.crp.tmp laiLut.pst.tmp laiLut.urb.tmp  laiLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile    laiLut.tmp1 laiLut.tmp            
            echo '&parameter name=laiLut long_name="Leaf Area Index on Land Use Tile" units="1" /' > laiLut.partab
            ;;
        landCoverFrac )  # land cover fractions  (Sum over all land cover fractions equals 1)
            [[ ! -f landCoverFrac.tmp ]] || rm landCoverFrac.tmp
            cdo merge bare.tmp    glac.tmp     \
                      tropev.tmp  tropdec.tmp  \
                      extrev.tmp  extrdec.tmp  \
                      rgshrub.tmp decshrub.tmp \
                      c3grass.tmp c4grass.tmp  \
                      c3past.tmp  c4past.tmp   \
                      c3crop.tmp  c4crop.tmp     landCoverFrac.tmp
            cdo setlevel,1  landCoverFrac.tmp    landCoverFrac.tmp1   # sets the first level to 1 (had been 0)
            cdo ifthen slm  landCoverFrac.tmp1  landCoverFrac.tmp2
            ncrename -h -O -d sfc,PFT -v sfc,PFT landCoverFrac.tmp2 landCoverFrac.tmp
            echo '&parameter name=landCoverFrac long_name="Plant Functional Type Grid Fraction" units="%" /' > landCoverFrac.partab
            ;;
        mrro )  # total runoff  (mrro = mrrob + mrros)
            if [[ -f ${inf_accw} ]]; then
                cdo selcode,160 ${inf_accw} mrro.tmp
            else
                cdo selcode,63 ${inf_land} mrro.tmp
            fi
            echo '&parameter name=mrro long_name="Total Runoff" units="kg m-2 s-1" /' > mrro.partab
            ;;
        mrrob )  # drainage
            cdo selcode,161 ${inf_accw} mrrob.tmp
            echo '&parameter name=mrrob long_name="Subsurface Runoff" units="kg m-2 s-1" /' > mrrob.partab
            ;;
        mrros )  # surface runoff
            cdo sub -selcode,160 ${inf_accw} -selcode,161 ${inf_accw} mrros.tmp
            echo '&parameter name=mrros long_name="Surface Runoff" units="kg m-2 s-1" /' > mrros.partab
            ;;
        mrso )  # soil moisture content   (CMIP5: echam code 140 - soil moisture within the root zone)
            cdo vertsum  mrsol.tmp  mrso.tmp
            echo '&parameter name=mrso long_name="Total Soil Moisture Content" units="kg m-2" /' > mrso.partab
            ;;
        mrsol )  # water content of soil layer  (generated above)
            echo '&parameter name=mrsol long_name="Total Water Content of Soil Layer" units="kg m-2" /' > mrsol.partab
            ;;
        mrsos )  # water content of uppermost 10 cm
            #  1st layer  (3):  6.5 cm   -> complete      (minimum soil depth in jsbach: 10 cm)
            #  2nd layer (19): 25.4 cm   -> uppermost 3.5 cm  -> 13.78%
            cdo add -sellevel,3 mrsol.tmp -mulc,0.1378 -mul lay2factor.tmp -sellevel,19 mrsol.tmp  mrsos.tmp
            echo '&parameter name=mrsos long_name="Moisture in Upper Portion of Soil Column" units="kg m-2" /' > mrsos.partab
            ;;
        msl )  # soil moisture of each layer (CRESCENDO)  - not for CMOR  (same as 'mrsol')
            cp  mrsol.tmp  msl.tmp 
            echo '&parameter name=msl long_name="Moisture of Soil" units="kg m-2" /' > msl.partab            
            ;;
        nbp )  # net biospheric production    # netAtmosLandCO2Flux has identical standard name!  
               # nbp = gpp - ra - rh - fFire - fHarvest - fLcc   (fGrazing is included in rh)
            [[ ! -f nbp.tmp ]] || rm nbp.tmp
            cdo enssum -mulc,0.01201  -vertsum    npp_tiles.tmp        \
                       -mulc,-0.01201 -vertsum    rh_tiles.tmp         \
                       -mulc,-0.272912        -selcode,56  ${inf_veg}  \
                       -mulc,-0.01201         -selcode,163 ${inf_main} \
                       -mulc,-0.01201         -selcode,211 ${inf_veg}  \
                       -mulc,-0.01201         -selcode,162  ${inf_main}   nbp.tmp
            echo '&parameter name=nbp long_name="Carbon Mass Flux out of Atmosphere due to' \
                 'Net Biospheric Production on Land" units="kgC m-2 s-1" /'                   > nbp.partab
            ;;
        nep )  # net ecosystem productivity  (i.e. natural fluxes from the atmosphere to the land)
               # nep = gpp - ra - rh - fFire  (fGrazing is included in rh) 
               # nep = fco2nat (on land)
            [[ ! -f nep.tmp ]] || rm nep.tmp
                cdo enssum -mulc,0.01201  -vertsum    npp_tiles.tmp \
                           -mulc,-0.01201 -vertsum    rh_tiles.tmp  \
                           -mulc,-0.272912  -selcode,56  ${inf_veg}  nep.tmp
            echo '&parameter name=nep long_name="Net Carbon Mass Flux out of Atmosphere due to' \
                 'Net Ecosystem Productivity on Land" units="kgC m-2 s-1" /'                      > nep.partab
            ;;
        netAtmosLandCO2Flux ) # net biospheric production  (d/dt(cLand) = netAtmosLandCO2Flux)
                              # nbp has identical standard name!
            # Note: due to a 1 day time shift between veg stream and co2 stream results are not identical, whether or not 
            #       netAtmosLandCO2Flux is calculated from co2 stream variables.
            if [[ -f ${inf_co2} ]]; then
                [[ -f netAtmosLandCO2Flux.tmp1 ]] && rm netAtmosLandCO2Flux.tmp1
                cdo enssum -selcode,6  ${inf_co2} \
                           -selcode,24 ${inf_co2} \
                           -selcode,25 ${inf_co2}  netAtmosLandCO2Flux.tmp1
                cdo ifthen slm -mulc,-0.272912 netAtmosLandCO2Flux.tmp1 netAtmosLandCO2Flux.tmp
            else
                [[ -f netAtmosLandCO2Flux.tmp ]] && rm netAtmosLandCO2Flux.tmp
                cdo enssum -mulc,0.01201  -vertsum   npp_tiles.tmp     \
                           -mulc,-0.01201 -vertsum   rh_tiles.tmp      \
                           -mulc,-0.01201     -selcode,82  ${inf_veg}  \
                           -mulc,-0.01201     -selcode,163 ${inf_main} \
                           -mulc,-0.01201     -selcode,211 ${inf_veg}  \
                           -mulc,-0.272912    -selcode,56  ${inf_veg}   netAtmosLandCO2Flux.tmp
            fi
            echo '&parameter name=netAtmosLandCO2Flux long_name="Net Flux of CO2 between Atmosphere' \
                 'and Land (positive into land) as a Result of All Processes" units="kgC m-2 s-1" /'   > netAtmosLandCO2Flux.partab
            ;;
        nLand )  # total land nitrogen  (nLand = nVeg + nLitter + nSoil + nProduct + nMineral)
            cdo mulc,0.0140068 -selcode,247 ${inf_nitro}  nLand.tmp
            echo '&parameter name=nLand long_name="Total Nitrogen in all Terrestrial Nitrogen Pools" ' \
                 'units="kgN m-2" /'                                                                     > nLand.partab
            ;;
        Nlimitation ) # nitrogen limitation  - not for CMOR 
                      #vg: weiss nicht, ob das sinnvoll ueber die tiles gemittelt werden kann
            cdo selcode,212 ${inf_veg} Nlimitation.tmp
            echo '&parameter name=Nlimitation long_name="Nitrogen Limitation" units="1" /' > Nlimitation.partab
            ;;
        nLitter )  # nitrogen in litter pools  (only above ground pools; below ground litter is part of soil)
            cdo mulc,0.0140068 -add -vertsum -selcode,243 ${inf_nitro} -vertsum -selcode,245 ${inf_nitro}  nLitter.tmp
            echo '&parameter name=nLitter long_name="Nitrogen Mass in Litter Pools" units="kgN m-2" /' > nLitter.partab
            ;;
        nMineral )  # mineral nitrogen
            cdo mulc,0.0140068 -vertsum -selcode,250 ${inf_nitro} nMineral.tmp 
            echo '&parameter name=nMineral long_name="Mineral Nitrogen in the Soil" units="kgN m-2" /' > nMineral.partab
            ;;
        npp )  # total NPP npp = gpp − ra
               #           npp = nppCrop*cropFrac + nppPast*pastureFrac + nppGrass*grassFrac + nppShrub*shrubFrac + nppTree*treeFrac
               #           npp = SUM (nppLut(:) * fracLut(:))
            cdo mulc,0.01201 -vertsum npp_tiles.tmp  npp.tmp
            echo '&parameter name=npp long_name="Carbon Mass Flux out of Atmosphere due to' \
                 'Net Primary Production on Land" units="kgC m-2 s-1" /'                      > npp.partab
            ;;
        npppft )  # npp per PFT (TRENDY)  - not for CMOR          -- copied behaviour from LAIpft
            cdo mulc,0.01201 npp_tiles.tmp npp_tiles_C.tmp
            cdo -f nc splitlevel npp_tiles_C.tmp npppft_
            cdo setlevel,1 -ifthen slm -mulc,0. npppft_000001.nc npppft01.tmp  # bare FPC
            cdo setlevel,2 -ifthen slm -mulc,0. npppft_000001.nc npppft02.tmp  # glacier
            cdo setlevel,3                      npppft_000001.nc npppft03.tmp
            cdo setlevel,4                      npppft_000002.nc npppft04.tmp
            cdo setlevel,5                      npppft_000003.nc npppft05.tmp
            cdo setlevel,6                      npppft_000004.nc npppft06.tmp
            cdo setlevel,7                      npppft_000005.nc npppft07.tmp
            cdo setlevel,8                      npppft_000006.nc npppft08.tmp
            cdo setlevel,9                      npppft_000007.nc npppft09.tmp
            cdo setlevel,10                     npppft_000008.nc npppft10.tmp
            cdo setlevel,11                     npppft_000009.nc npppft11.tmp
            cdo setlevel,12                     npppft_000010.nc npppft12.tmp
            cdo setlevel,14 -mul C3C4_crop_mask npppft_000011.nc npppft14.tmp  # c4 crop
            cdo setlevel,13 -sub npppft_000011.nc  npppft14.tmp npppft13.tmp  # c3 crop
            [[ ! -f npppft.tmp1 ]] || rm npppft.tmp1
            cdo merge npppft??.tmp  npppft.tmp1
            ncrename -h -O -d lev,PFT -v lev,PFT npppft.tmp1 npppft.tmp
            echo '&parameter name=npppft long_name="Carbon Mass Flux out of Atmosphere due to' \
                 'Net Primary Production on Land" units="kgC m-2 s-1" /' > npppft.partab
            ;;
        nppCrop )  # NPP of crops  - not for CMOR
            cdo mulc,0.01201 -ifthen divcrop -mul -sellevel,11 npp_tiles.tmp divcrop  nppCrop.tmp
            echo '&parameter name=nppCrop long_name="Net Primary Production on Crop Tiles" units="kgC m-2 s-1" /' > nppCrop.partab
            ;;
        nppGrass )  # NPP of grass
            cdo mulc,0.01201 -ifthen divgrass -mul -vertsum -sellevel,7/8 npp_tiles.tmp divgrass  nppGrass.tmp
            echo '&parameter name=nppGrass long_name="Net Primary Production on Grass Tiles" units="kgC m-2 s-1" /' > nppGrass.partab
            ;;
        nppLut )  # NPP of land use tiles
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo -f grb setlevel,1 -ifthen divpsl -mulc,0.01201 -mul -vertsum -sellevel,1/8  npp_tiles.tmp divpsl nppLut.psl.tmp
            cdo -f grb setlevel,2 -ifthen divcrp -mulc,0.01201 -mul          -sellevel,11   npp_tiles.tmp divcrp nppLut.crp.tmp
            cdo -f grb setlevel,3 -ifthen divpst -mulc,0.01201 -mul -vertsum -sellevel,9/10 npp_tiles.tmp divpst nppLut.pst.tmp
            cdo -f grb setlevel,4 -ifthen divurb   nppLut.crp.tmp                                                nppLut.urb.tmp
            [[ ! -f nppLut.tmp1 ]] || rm nppLut.tmp1
            cdo -f nc merge  nppLut.psl.tmp nppLut.crp.tmp nppLut.pst.tmp nppLut.urb.tmp  nppLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile    nppLut.tmp1 nppLut.tmp
            echo '&parameter name=nppLut long_name="net primary productivity on land use tile" units="kgC m-2 s-1" /' > nppLut.partab
            ;;
        nppPast )  # NPP of pastures  - not for CMOR
            cdo mulc,0.01201 -ifthen divpast -mul -vertsum -sellevel,9/10 npp_tiles.tmp divpast  nppPast.tmp
            echo '&parameter name=nppPast long_name="Net Primary Production on Pasture Tiles" units="kgC m-2 s-1" /' > nppPast.partab
            ;;
        nppShrub )  # NPP of shrubs
            cdo mulc,0.01201 -ifthen divshrub -mul -vertsum -sellevel,5/6 npp_tiles.tmp divshrub  nppShrub.tmp
            echo '&parameter name=nppShrub long_name="Net Primary Production on Shrub Tiles" units="kgC m-2 s-1" /' > nppShrub.partab
            ;;
        nppTree )  # NPP of trees
            cdo mulc,0.01201 -ifthen divtree -mul -vertsum -sellevel,1/4 npp_tiles.tmp divtree  nppTree.tmp
            echo '&parameter name=nppTree long_name="Net Primary Production on Tree Tiles" units="kgC m-2 s-1" /' > nppTree.partab
            ;;
        nProduct )  # nitrogen in Product pools
            cdo mulc,0.0140068 -vertsum -selcode,222 ${inf_nitro} nProduct.tmp 
            echo '&parameter name=nProduct long_name="Nitrogen Mass in Products of Land Use Change"' \
                 'units="kgN m-2" /'                                                                   > nProduct.partab
            ;;
        nSoil )  # nitrogen in soil pools - inlcudes N from below ground litter pools  (nSoil = nSoilFast + nSoilSlow (not CMOR))
            [[ ! -f nSoil.tmp1 ]] || rm nSoil.tmp1
            cdo enssum -vertsum -selcode,244 ${inf_nitro} \
                       -vertsum -selcode,246 ${inf_nitro} \
                       -vertsum -selcode,248 ${inf_nitro}  nSoil.tmp1 
            cdo mulc,0.0140068 nSoil.tmp1  nSoil.tmp
            echo '&parameter name=nSoil long_name="Nitrogen Mass in Soil Pool" units="kgN m-2" /' > nSoil.partab
            ;;
        nSoilFast )  # nitrogen in below ground litter (fast soil pools)  - not for CMOR
            cdo mulc,0.0140068 -add -vertsum -selcode,244 ${inf_nitro} \
                                    -vertsum -selcode,248 ${inf_nitro}  nSoilFast.tmp 
            echo '&parameter name=nSoilFast long_name="Nitrogen Mass in Fast Soil Pool" units="kgN m-2" /' > nSoilFast.partab
            ;;
        nSoilSlow )  # nitrogen in slow soil pool  - not for CMOR
            cdo mulc,0.0140068 -vertsum -selcode,246 ${inf_nitro}  nSoilSlow.tmp 
            echo '&parameter name=nSoilSlow long_name="Nitrogen Mass in Slow Soil Pool" units="kgN m-2" /' > nSoilSlow.partab
            ;;
        nVeg )  # nitrogen in vegetation pools
            [[ -f nVeg.tmp1 ]] && rm nVeg.tmp1
            cdo enssum -vertsum -selcode,240 ${inf_nitro} \
                       -vertsum -selcode,241 ${inf_nitro} \
                       -vertsum -selcode,242 ${inf_nitro}  nVeg.tmp1
            cdo mulc,0.0140068 nVeg.tmp1 nVeg.tmp
            echo '&parameter name=nVeg long_name="Nitrogen Mass in Vegetation" units="kgN m-2" /' > nVeg.partab
            ;;
        nwdFracLut ) # non woody fraction of land use tile  (bare land is part of woody AND non woody psl vegetation)
            # note: cdo setlevel for some reason only works with grib files
            [[ ! -f nwdFracLut.natveg.tmp ]] || rm nwdFracLut.natveg.tmp
            cdo enssum  tree.tmp shrub.tmp grass.tmp  nwdFracLut.natveg.tmp
            cdo -f grb setlevel,1 -div grass.tmp nwdFracLut.natveg.tmp      nwdFracLut.psl.tmp
            cdo -f grb setlevel,2 -ifthen land -addc,1. -mulc,0. grass.tmp  nwdFracLut.crp.tmp  # dummy 1
            cdo -f grb setlevel,3 -ifthen land -addc,1. -mulc,0. grass.tmp  nwdFracLut.pst.tmp  # dummy 1
            cdo -f grb setlevel,4 -ifthen divurb                 grass.tmp  nwdFracLut.urb.tmp  # dummy missval
            [[ ! -f nwdFracLut.tmp1 ]] || rm nwdFracLut.tmp1
            cdo -f nc merge  nwdFracLut.psl.tmp nwdFracLut.crp.tmp nwdFracLut.pst.tmp nwdFracLut.urb.tmp  nwdFracLut.tmp1
            ncrename -h -O -d plev,Tile -v plev,Tile   nwdFracLut.tmp1 nwdFracLut.tmp
            echo '&parameter name=nwdFracLut long_name="fraction of land use tile tile that is non-woody' \
                 'vegetation (e.g. herbaceous crops)" units="1" /'                                         > nwdFracLut.partab
            ;;
        pastureFrac )  # pasture fraction  (pastureFrac = pastureFracC3 + pastureFracC4)
            cp past.tmp  pastureFrac.tmp
            echo '&parameter name=pastureFrac long_name="Anthropogenic Pasture Fraction" units="%" /' > pastureFrac.partab
            ;;
        pastureFracC3 )  # C3 pasture fraction
            cp c3past.tmp  pastureFracC3.tmp
            echo '&parameter name=pastureFracC3 long_name="C3 Pasture Area Percentage" units="%" /' > pastureFracC3.partab
            ;;
        pastureFracC4 )  # C4 pasture fraction
            cp c4past.tmp  pastureFracC4.tmp
            echo '&parameter name=pastureFracC4 long_name="C4 Pasture Area Percentage" units="%" /' > pastureFracC4.partab
            ;;
        pr )   # precipitation
            if [[ -f ${inf_BOT} ]]; then
                cdo add -selcode,142 ${inf_BOT} -selcode,143 ${inf_BOT} pr.tmp
            else
                cdo add  -selcode,2 ${inf_forc}  -selcode,3 ${inf_forc} pr.tmp
            fi
            echo '&parameter name=pr long_name="Precipitation" units="kg m-2 s-1" /' > pr.partab
            ;;
        prra )  # Rainfall rate
            if [[ -f ${inf_BOT} ]]; then
                cdo expr,"prra=var142+var143-var144" ${inf_BOT} prra.tmp
            else
                cdo selcode,2 ${inf_forc} prra.tmp
            fi
            echo '&parameter name=prra long_name="Rainfall rate" units="kg m-2 s-1" /' > prra.partab
            ;; 
        ra )  # total autotrophic respiration  (npp = gpp − ra)
              #            ra = raCrop*fracCrop + raPast*fracPast + raGrass*fracGrass + raShrub*fracShrub + raTree*fracTree
              #            ra = SUM(raLut(:) * fracLut(:))
            cdo mulc,0.01201 -vertsum ra_tiles.tmp  ra.tmp
            echo '&parameter name=ra long_name="Carbon Mass Flux into Atmosphere due to' \
                 'Autotrophic (Plant) Respiration on Land" units="kgC m-2 s-1" /'          > ra.partab
            ;;
        raCrop )  # autotrophic respiration of crops  - not for CMOR
            cdo mulc,0.01201 -ifthen divcrop -mul -sellevel,11 ra_tiles.tmp divcrop  raCrop.tmp
            echo '&parameter name=raCrop long_name="Autotrophic Respiration on Grass Tiles" units="kgC m-2 s-1" /' > raCrop.partab
            ;;
        raGrass )  # autotrophic respiration of grass
            cdo mulc,0.01201 -ifthen divgrass -mul -vertsum -sellevel,7/8 ra_tiles.tmp divgrass raGrass.tmp
            echo '&parameter name=raGrass long_name="Autotrophic Respiration on Grass Tiles" units="kgC m-2 s-1" /' > raGrass.partab
            ;;
        raLut )  # autotrophic respiration of land use tiles
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo -f grb setlevel,1 -ifthen divpsl -mulc,0.01201 -mul -vertsum -sellevel,1/8  ra_tiles.tmp divpsl raLut.psl.tmp
            cdo -f grb setlevel,2 -ifthen divcrp -mulc,0.01201 -mul          -sellevel,11   ra_tiles.tmp divcrp raLut.crp.tmp
            cdo -f grb setlevel,3 -ifthen divpst -mulc,0.01201 -mul -vertsum -sellevel,9/10 ra_tiles.tmp divpst raLut.pst.tmp
            cdo -f grb setlevel,4 -ifthen divurb            raLut.crp.tmp                                       raLut.urb.tmp
            [[ ! -f raLut.tmp1 ]] || rm raLut.tmp1
            cdo -f nc merge  raLut.psl.tmp raLut.crp.tmp raLut.pst.tmp raLut.urb.tmp  raLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile   raLut.tmp1 raLut.tmp
            echo '&parameter name=raLut long_name="plant respiration on land use tile" units="kgC m-2 s-1" /' > raLut.partab
            ;;
        raPast )  # autotrophic respiration of pastures  - not for CMOR
            cdo mulc,0.01201 -ifthen divpast -mul -vertsum -sellevel,9/10 ra_tiles.tmp divpast raPast.tmp
            echo '&parameter name=raPast long_name="Autotrophic Respiration on Pasture Tiles" units="kgC m-2 s-1" /' > raPast.partab
            ;;
        raShrub )  # autotrophic respiration of shrubs
            cdo mulc,0.01201 -ifthen divshrub -mul -vertsum -sellevel,5/6 ra_tiles.tmp divshrub  raShrub.tmp
            echo '&parameter name=raShrub long_name="Autotrophic Respiration on Shrub Tiles" units="kgC m-2 s-1" /' > raShrub.partab
            ;;
        raTree )  # autotrophic respiration of trees
            cdo mulc,0.01201 -ifthen divtree -mul -vertsum -sellevel,1/4 ra_tiles.tmp divtree  raTree.tmp
            echo '&parameter name=raTree long_name="Autotrophic Respiration on Tree Tiles" units="kgC m-2 s-1" /' > raTree.partab
            ;;
        residualFrac )  # residual fraction (glaciers) 
            cdo mulc,100. -ifthen slm glac residualFrac.tmp
            echo '&parameter name=residualFrac long_name="Fraction of Grid Cell that is Land but Neither' \
                 'Vegetation-Covered nor Bare Soil" units="%" /'                                           > residualFrac.partab
            ;;
        rh )  # heterotrophic respiration from ALL consumers, including herbivory  
              #    (CMIP5: cdo vertsum -selcode,170 ${inf_veg} -> without herbivory flux to the atmosphere)
              #    rh = rhCrop*cropFrac + rhPast*pastureFrac + rhGrass*grassFrac + rhShrub*shrubFrac + rhTree*treeFrac
              #    rh = SUM(rhLut(:) * fracLut(:))
            cdo mulc,0.01201 -vertsum rh_tiles.tmp  rh.tmp
            echo '&parameter name=rh long_name="Carbon Mass Flux into Atmosphere due to Heterotrophic' \
                 'Respiration on Land" units="kgC m-2 s-1" /'                                            > rh.partab
            ;;
        rhCrop ) # heterotrophic respiration of crops  - not for CMOR
            cdo mulc,0.01201 -ifthen divcrop -mul -sellevel,11 rh_tiles.tmp divcrop  rhCrop.tmp
            echo '&parameter name=rhCrop long_name="heterotrophic respiration on crop tiles" units="kgC m-2 s-1" ' > rhCrop.partab
            ;;
        rhGrass ) # heterotrophic respiration of grass
            cdo mulc,0.01201 -ifthen divgrass -mul -vertsum -sellevel,7/8 rh_tiles.tmp divgrass  rhGrass.tmp
            echo '&parameter name=rhGrass long_name="heterotrophic respiration on grass tiles" units="kgC m-2 s-1" ' > rhGrass.partab
            ;;
        rhLut )  # soil heterotrophic respiration of land use tiles  (note: herbivory included as in other rh variables)
            # note: cdo setlevel for some reason only works with grib files
            #       ifthen div??? is needed as 0.* missval = 0. (at least in the current cdo version)
            cdo -f grb setlevel,1 -ifthen divpsl -mulc,0.01201 -mul -vertsum -sellevel,1/8  rh_tiles.tmp divpsl rhLut.psl.tmp
            cdo -f grb setlevel,2 -ifthen divcrp -mulc,0.01201 -mul          -sellevel,11   rh_tiles.tmp divcrp rhLut.crp.tmp
            cdo -f grb setlevel,3 -ifthen divpst -mulc,0.01201 -mul -vertsum -sellevel,9/10 rh_tiles.tmp divpst rhLut.pst.tmp
            cdo -f grb setlevel,4 -ifthen divurb       rhLut.crp.tmp                                            rhLut.urb.tmp
            [[ ! -f rhLut.tmp1 ]] || rm rhLut.tmp1
            cdo -f nc merge  rhLut.psl.tmp rhLut.crp.tmp rhLut.pst.tmp rhLut.urb.tmp  rhLut.tmp1
            ncrename -h -O -d sfc,Tile -v sfc,Tile   rhLut.tmp1 rhLut.tmp
            echo '&parameter name=rhLut long_name="soil heterotrophic respiration on land use tile" units="kgC m-2 s-1" /' \
                 > rhLut.partab
            ;;
        rhPast ) # heterotrophic respiration of pastures  - not for CMOR
            cdo mulc,0.01201 -ifthen divpast -mul -vertsum -sellevel,9/10 rh_tiles.tmp divpast  rhPast.tmp
            echo '&parameter name=rhPast long_name="heterotrophic respiration on pasture tiles" units="kgC m-2 s-1" ' > rhPast.partab
            ;;
        rhShrub ) # heterotrophic respiration of shrubs
            cdo mulc,0.01201 -ifthen divshrub -mul -vertsum -sellevel,5/6 rh_tiles.tmp divshrub  rhShrub.tmp
            echo '&parameter name=rhShrub long_name="heterotrophic respiration on Shrub tiles" units="kgC m-2 s-1" ' > rhShrub.partab
            ;;
        rhSoil ) # heterotrophic soil respiration
            cdo mulc,-0.01201 -vertsum rhSoil_tiles.tmp rhSoil.tmp
            echo '&parameter name=rhSoil long_name="Carbon Mass Flux into Atmosphere due to Heterotrophic' \
                 'Respiration from Soil on Land" units="kgC m-2 s-1" '                                       > rhSoil.partab
            ;;
        rhTree ) # heterotrophic respiration of tree
            cdo mulc,0.01201 -ifthen divtree -mul -vertsum -sellevel,1/4 rh_tiles.tmp divtree  rhTree.tmp
            echo '&parameter name=rhTree long_name="heterotrophic respiration on tree tiles" units="kgC m-2 s-1" ' > rhTree.partab
            ;;
        rlds )  # downward longwave radiation
            cdo sub -selcode,177 ${inf_BOT} -selcode,205 ${inf_BOT} rlds.tmp
            echo '&parameter name=rlds long_name="Surface Downwelling Longwave Radiation" units="W m-2" /' > rlds.partab
            ;;
        rlus )  # upward longwave radiation
            cdo mulc,-1 -selcode,205 ${inf_BOT} rlus.tmp
            echo '&parameter name=rlus long_name="Surface Upwelling Longwave Radiation" units="W m-2" /' > rlus.partab
            ;;
        rsds )  # downward shortwave radiation
            if [[ -f ${inf_BOT} ]]; then
                cdo sub -selcode,176 ${inf_BOT} -selcode,204 ${inf_BOT} rsds.tmp
            else
                cdo selcode,7 ${inf_forc} rsds.tmp
            fi
            echo '&parameter name=rsds long_name="Surface Downwelling Shortwave Radiation" units="W m-2" /' > rsds.partab
            ;;
        rsds_nir )  # surface downwelling near-IR radiation  - Only available with LS3M_TYPE=1 (namelist jsbach_ctl)
            cdo selcode,34 ${inf_main} rsds_nir.tmp
            echo '&parameter name=rsds_nir long_name="surface downwelling near-IR irradiance" units="W m-2" /' > rsds_nir.partab
            ;;
        rsds_vis )  # surface downwelling visible radiation  - Only available with LS3M_TYPE=1 (namelist jsbach_ctl)
            cdo selcode,33 ${inf_main} rsds_vis.tmp
            echo '&parameter name=rsds_vis long_name="surface downwelling visible irradiance" units="W m-2" /' > rsds_vis.partab
            ;;
        rss_nir )  # net surface near-IR radiation
            cdo selcode,203 ${inf_surf} rss_nir.tmp
            echo '&parameter name=rss_nir long_name="net surface near-IR irradiance" units="W m-2" /' > rss_nir.partab
            ;;
        rss_nir_ns )  # net surface near-IR radiation without snow  - Only available with LS3M_TYPE=1 (namelist jsbach_ctl)
            cdo selcode,36 ${inf_main} rss_nir_ns.tmp
            echo '&parameter name=rss_nir_ns long_name="net surface near-IR irradiance calculated without snow" units="W m-2" /' \
                 > rss_nir_ns.partab
            ;;
        rss_ns )  # net surface solar radiation without snow  - Only available with LS3M_TYPE=1 (namelist jsbach_ctl)
            cdo add -selcode,35 ${inf_main} -selcode,36 ${inf_main} rss_ns.tmp
            echo '&parameter name=rss_ns long_name="net surface solar radiative flux calculated without snow" units="W m-2" /' \
                 > rss_ns.partab
            ;;
        rss_vis )  # net surface visible radiation
            cdo selcode,204 ${inf_surf} rss_vis.tmp
            echo '&parameter name=rss_vis long_name="net surface visible irradiance" units="W m-2" /' > rss_vis.partab
            ;;
        rss_vis_ns )  # net surface visible radiation without snow  - Only available with LS3M_TYPE=1 (namelist jsbach_ctl)
            cdo selcode,35 ${inf_main} rss_vis_ns.tmp
            echo '&parameter name=rss_vis_ns long_name="net surface visible irradiance calculated without snow" units="W m-2" /' \
                 > rss_vis_ns.partab
            ;;
        rsus )  # upward shortwave radiation
            cdo mulc,-1 -selcode,204 ${inf_BOT} rsus.tmp
            echo '&parameter name=rsus long_name="Surface Upwelling Shortwave Radiation" units="W m-2" /' > rsus.partab
            ;; 
        rzwc )  # root zone soil moisture
            cdo ifthen slm -mulc,1000. -selcode,140 ${inf_BOT} rzwc.tmp
            echo '&parameter name=rzwc long_name="Root Zone Soil Moisture" units="kg m-2" /' > rzwc.partab
            ;;
        sbl )  # snow and ice sublimation
            cdo mulc,-1 -selcode,80 ${inf_land}  sbl.tmp
            echo '&parameter name=sbl long_name="Surface Snow and Ice Sublimation Flux" units="kg m-2 s-1" /' > sbl.partab
            ;;
        sfcWind )  # 10m wind speed
            if [[ -f ${inf_BOT} ]]; then
                cdo selcode,171 ${inf_BOT}  sfcWind.tmp
            else
                cdo selcode,12 ${inf_forc}  sfcWind.tmp
            fi
            echo '&parameter name=sfcWind long_name="Near-Surface Wind Speed" units="m s-1" /' > sfcWind.partab
            ;;
        sfls )  # sensible heat flux (CRESCENDO)  - not for CMOR  (same as 'hfss')
            if [[ -f ${inf_BOT} ]]; then
                cdo mulc,-1 -selcode,146 ${inf_BOT}  sfls.tmp
            else
                cdo mulc,-1 -selcode,47 ${inf_land}  sfls.tmp
            fi
            echo '&parameter name=sfls long_name="Surface Upward Sensible Heat Flux" units="W m-2" /' > sfls.partab
            ;;
        shrubFrac )  # shrub fraction
            cp shrub.tmp  shrubFrac.tmp
            echo '&parameter name=shrubFrac long_name="Shrub Fraction" units="%" /' > shrubFrac.partab
            ;;
        shrubFracPot )  # potential shrub fraction  - not for CMOR
            cdo mulc,100 -ifthen slm -vertsum -sellevel,5/6  box_cover_fract_pot shrubFracPot.tmp
            echo '&parameter name=shrubFracPot long_name="Potential Shrub Fraction" units="%" /' > shrubFracPot.partab
            ;;
        snc )  # snow fraction
            cdo mulc,100 -selcode,60 ${inf_land} snc.tmp
            echo '&parameter name=snc long_name="Snow Cover Fraction" units="%" /' > snc.partab
            ;;
        snm )  # snow melt
            cdo selcode,218 ${inf_accw} snm.tmp
            echo '&parameter name=snm long_name="Surface Snow Melt" units="kg m-2 s-1" /' > snm.partab
            ;;
        snw )  # snow amount
            cdo mulc,1000. -selcode,141 ${inf_BOT} snw.tmp
            echo '&parameter name=snw long_name="Surface Snow Amount" units="kg m-2" /' > snw.partab
            ;;
        snwc ) # snow on vegetation
            cdo mulc,1000. -selcode,109 ${inf_land} snwc.tmp
            echo '&parameter name=snwc long_name="SWE intercepted by the vegetation" units="kg m-2" /' > snwc.partab
            ;;
        tas )  # 2m temperature
            if [[ -f ${inf_BOT} ]]; then
                cdo selcode,167 ${inf_BOT}  tas.tmp
            else
                cdo addc,273.15 -selcode,1 ${inf_forc}  tas.tmp
            fi
            echo '&parameter name=tas long_name="Near-Surface Air Temperature" units="K" /' > tas.partab
            ;;
        tr )  # radiative temperature
            cdo selcode,36 ${inf_land}  tr.tmp
            echo '&parameter name=tr long_name="Surface Radiative Temperature" units="K" /' > tr.partab
            ;;
        tran )  # transpiration
            cdo mulc,-1 -selcode,76 ${inf_land}  tran.tmp
            echo '&parameter name=tran long_name="Transpiration" units="kg m-2 s-1" /' > tran.partab
            ;;
        treeFrac )  # forest fraction
            cp tree.tmp  treeFrac.tmp
            echo '&parameter name=treeFrac long_name="Tree Cover Fraction" units="%" /' > treeFrac.partab
            ;;
        treeFracPot )  # potential forest fraction  - not for CMOR
            cdo mulc,100. -ifthen slm -vertsum -sellevel,1/4  box_cover_fract_pot treeFracPot.tmp
            echo '&parameter name=treeFracPot long_name="Potential Tree Cover Fraction" units="%" /' > treeFracPot.partab
            ;;
        ts )   # surface temperature
            cdo selcode,169 ${inf_BOT}  ts.tmp
            echo '&parameter name=ts long_name="Surface Temperature" units="K" /' > ts.partab
            ;;
        tsl )  # soil temperature     #CMOR-Table not clear: near surface soil temperature or all soil layers?
                                      # We provide the Temperature of each soil layer.
            cdo selcode,68 ${inf_land}  tsl.tmp
            echo '&parameter name=tsl long_name="Temperature of Soil" units="K" /' > tsl.partab
            ;;
        vegFrac )  # vegetated fraction  (vegFrac + baresoilFrac + residualFrac = slm)
                   #                 vegFrac = treeFrac + shrubFrac + grassFrac + cropFrac + pastureFrac
                   #                 vegfrac = c3PftFrac + c4PftFrac
            cdo mulc,100. -ifthen slm -selcode,20 ${inf_main} vegFrac.tmp
            echo '&parameter name=vegFrac long_name="Total vegetated Fraction" units="%" /' > vegFrac.partab
            ;;
        * )
            echo "variable ${var} not supported"
            exit 1
            ;;
    esac

    if [[ ${specialWish} = trendy ]]; then
        case ${var} in
            landCoverFrac )
                # TRENDY wants landCoverFrac in [0,1] instead of %
                cdo divc,100. landCoverFrac.tmp landCoverFrac_trendy.tmp
                mv landCoverFrac_trendy.tmp landCoverFrac.tmp
                echo '&parameter name=landCoverFrac long_name="Plant Functional Type Grid Fraction" units="1" /' > landCoverFrac.partab
            ;;
            burntArea )
                # TRENDY wants some files annually: burntArea  
                cdo -yearsum burntArea.tmp burntArea_trendy.tmp
                mv burntArea_trendy.tmp burntArea.tmp
                echo '&parameter name=burntArea long_name="Burnt area" units="%" /' > burntArea.partab
            ;;
            c[A-Z]* | n[A-Z]* )
                # TRENDY wants some files annually: carbon and nitrogen stocks
                cdo -yearmonmean ${var}.tmp ${var}_trendy.tmp
                mv ${var}_trendy.tmp ${var}.tmp
            ;;
        esac
    fi

    cdo -f nc -r setpartabn,${var}.partab -invertlat -setvar,${var} ${var}.tmp ${var}.nc

    # define global attributes for dimensions (pfts, pools)
    case ${var} in
        landCoverFrac | *pft )
            ncatted -h -O -a comment,global,a,c,"Land cover types:\nPFT1:  Bare land\nPFT2:  Glacier\nPFT3:  Tropical evergreen trees\nPFT4:  Tropical deciduous trees\nPFT5:  Extra-tropical evergreen trees\nPFT6:  Extra-tropical deciduous trees\nPFT7:  Raingreen shrubs\nPFT8:  Deciduous shrubs\nPFT9:  C3 grass\nPFT10: C4 grass\nPFT11: C3 pasture\nPFT12: C4 pasture\nPFT13: C3 Crops\nPFT14: C4 Crops" ${var}.nc
        ;;
        cSoilPools )
            ncatted -h -O -a comment,global,a,c,"Yasso soil carbon pools:" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool1:  Acid-soluble below ground leaf litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool2:  Acid-soluble below ground woody litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool3:  Water-soluble below ground leaf litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool4:  Water-soluble below ground woody litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool5:  Ethanol-soluble below ground leaf litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool6:  Ethanol-soluble below ground woody litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool7:  Non-soluble below ground leaf litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool8:  Non-soluble below ground woody litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool9:  Humus from leaf litter" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nPool10: Humus from woody litter" ${var}.nc
        ;;
        *Lut )
            ncatted -h -O -a comment,global,a,c,"LUMIP Land Use Tiles:" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nTile1:  psl (primary and secondary land)" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nTile2:  crp (cropland)" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nTile3:  pst (pastureland)" ${var}.nc
            ncatted -h -O -a comment,global,a,c,"\nTile4:  urb (urban settlement)" ${var}.nc
        ;;
    esac  

    } & # comment out '&' for serial processing
done
wait

#------------------------------------------------------------------------------
# generate global time series and move the 2d netcdf output to outdata_land
#------------------------------------------------------------------------------
[[ -d outdata_land ]] || mkdir outdata_land

for f in ${var_list}; do
    {
      # generate global time series
      #   global surface area: 5.100656e14 m^2
      #   seconds per year: 31557600
      if [[ -f ${f}.nc ]]; then
        case $f in
            *Lut )
                # mean value of non-missing-value land cells
                cdo setgridarea,gridareafrac.nc ${f}.nc ${f}.slf
                cdo fldmean -yearmonmean ${f}.slf outdata_land/${tag}_${f}.mean.nc
                ;;
            burntArea )
                # %/month -> Mio km^2/yr
                cdo mul ${f}.nc -invertlat ${slf} ${f}.slf
                echo "&parameter name=$f" 'units="Mio km^2/yr" /' > ${f}.partab
                cdo setpartabn,${f}.partab -mulc,5.100656 -fldmean -setmisstoc,0 -yearsum ${f}.slf \
                    outdata_land/${tag}_${f}.global_area.nc
                ;;
            *Frac | *Frac[A-Z]* | snc )
                # % -> Mio km^2
                cdo mul ${f}.nc -invertlat ${slf} ${f}.slf
                echo "&parameter name=$f" 'units="Mio km^2" /' > ${f}.partab
                cdo setpartabn,${f}.partab -mulc,5.100656 -fldmean -setmisstoc,0 -yearmonmean ${f}.slf \
                    outdata_land/${tag}_${f}.global_area.nc
                ;;
            c[A-Z]* | n[A-Z]* | co2mass | snwc | snw )
                # kg m-2 -> Gt
                cdo mul ${f}.nc -invertlat ${slf} ${f}.slf
                echo "&parameter name=$f" 'units="Gt" /' > ${f}.partab
                cdo setpartabn,${f}.partab -mulc,5.100656e02 -fldmean -setmisstoc,0 -yearmonmean ${f}.slf \
                    outdata_land/${tag}_${f}.global_mass.nc
                ;;
            f[A-Z]* | fco2* | gpp | gpp[A-Z]* | nbp | nep | netAtmosLandCO2Flux | npp | npp[A-Z]* | ra | ra[A-Z]* | rh \
                    | rh[A-Z]* | snm )
                # kg m-2 s-1 -> Gt yr-1
                cdo mul ${f}.nc -invertlat ${slf} ${f}.slf
                echo "&parameter name=$f" 'units="Gt yr-1" /' > ${f}.partab
                cdo setpartabn,${f}.partab -mulc,5.100656e02 -mulc,31557600 -fldmean -setmisstoc,0 -yearmonmean ${f}.slf \
                    outdata_land/${tag}_${f}.global_flux.nc
                ;;
            albs | alpha | cnc | es | et | fapar | lai | mrro* | mrso* | Nlimitation | rzwc | sbl | tr | tran | tsl )
                # global land mean
                cdo setgridarea,gridareafrac.nc ${f}.nc ${f}.slf
                cdo fldmean -yearmonmean ${f}.slf outdata_land/${tag}_${f}.land_mean.nc
                ;;
            * )
                if [[ ${specialWish} != dailyLAI ]]; then
                if [[ -f ${inf_BOT} ]]; then                
                    # global mean
                    cdo fldmean -setmisstoc,0 -yearmonmean ${f}.nc outdata_land/${tag}_${f}.global_mean.nc
                else
                    # global land mean
                    cdo setgridarea,gridareafrac.nc ${f}.nc ${f}.slf
                    cdo fldmean -yearmonmean ${f}.slf outdata_land/${tag}_${f}.land_mean.nc
                fi
                fi
                ;;
        esac

        case ${var} in
          laipft )
            # TRENDY wants it to be named lai
            cdo -setvar,lai laipft.nc outdata_land/${tag}_lai.nc
            rm laipft.nc
          ;;
          dlaipft )
            # TRENDY wants it to be named lai
            cdo -setvar,dlai dlaipft.nc outdata_land/${tag}_dlai.nc
            rm dlaipft.nc
          ;;
          * )
            mv ${f}.nc outdata_land/${tag}_${f}.nc
          ;;
        esac
      fi
    }  &  # comment out '&' for serial processing
done
wait

#------------------------------------------------------------------------------
# consitency tests
#    All test arrays should be close to zero.
#------------------------------------------------------------------------------
if [[ ${do_tests} == yes ]]; then
    cd outdata_land

    # Test 01: cLand = cVeg + cLitter + cSoil + cProduct
    [[ -f ${tag}_cLand.test ]] && rm ${tag}_cLand.test
    if [[ -f ${tag}_cLand.nc && -f ${tag}_cVeg.nc && -f ${tag}_cLitter.nc && -f ${tag}_cSoil.nc \
                && -f ${tag}_cProduct.nc ]]; then
        cdo enssum ${tag}_cVeg.nc     \
                   ${tag}_cLitter.nc  \
                   ${tag}_cSoil.nc    \
                   ${tag}_cProduct.nc  ${tag}_cLand.test
        cdo sub ${tag}_cLand.nc ${tag}_cLand.test test01.nc
fi

    # Test 02: cLitter =  cLitterGrass*grassFrac + cLitterShrub*shrubFrac + cLitterTree*treeFracc
    #                   + cLitterCrop*cropFrac + cLitterPast*pastureFrac
    [[ -f ${tag}_cLitter.test ]] && rm ${tag}_cLitter.test
    if [[ -f ${tag}_cropFrac.nc && -f ${tag}_pastureFrac.nc && -f ${tag}_grassFrac.nc && -f ${tag}_shrubFrac.nc \
                && -f ${tag}_treeFrac.nc && -f ${tag}_cLitter.nc && -f ${tag}_cLitterCrop.nc && -f ${tag}_cLitterPast.nc \
                && -f ${tag}_cLitterGrass.nc && -f ${tag}_cLitterShrub.nc  && ${tag}_cLitterTree.nc ]]; then
        cdo enssum -mul ${tag}_cLitterCrop.nc  ${tag}_cropFrac.nc    \
                   -mul ${tag}_cLitterPast.nc  ${tag}_pastureFrac.nc \
                   -mul ${tag}_cLitterGrass.nc ${tag}_grassFrac.nc   \
                   -mul ${tag}_cLitterShrub.nc ${tag}_shrubFrac.nc   \
                   -mul ${tag}_cLitterTree.nc  ${tag}_treeFrac.nc   ${tag}_cLitter.test
        cdo sub -mulc,100. ${tag}_cLitter.nc ${tag}_cLitter.test test02.nc    # *100 as fraction arrays are in %
    fi

    # Test 03: cropFrac = cropFracC3 + cropFracC4
    if [[ -f ${tag}_cropFrac.nc && ${tag}_cropFracC3.nc && ${tag}_cropFracC4.nc ]]; then
        cdo add ${tag}_cropFracC3.nc \
                ${tag}_cropFracC4.nc  ${tag}_cropFrac.test
        cdo sub ${tag}_cropFrac.nc ${tag}_cropFrac.test test03.nc
    fi

    # Test 04: cSoil = cSoilFast + cSoilSlow
    if [[ -f ${tag}_cSoil.nc && -f ${tag}_cSoilFast.nc && -f ${tag}_cSoilSlow.nc ]]; then
        cdo add ${tag}_cSoilFast.nc \
                ${tag}_cSoilSlow.nc  ${tag}_cSoil.test
        cdo sub ${tag}_cSoil.nc ${tag}_cSoil.test test04.nc
    fi

    # Test 05: cSoil =  cSoilGrass*grassFrac + cSoilShrub*shrubFrac + cSoilTree*treeFracc + cSoilCrop*cropFrac 
    #                 + cSoilPast*pastureFrac
    [[ -f ${tag}_cSoil.test ]] && rm ${tag}_cSoil.test
    if [[ -f ${tag}_cropFrac.nc && -f ${tag}_pastureFrac.nc && -f ${tag}_grassFrac.nc && -f ${tag}_shrubFrac.nc \
                && -f ${tag}_treeFrac.nc && -f ${tag}_cSoil.nc && -f ${tag}_cSoilCrop.nc && -f ${tag}_cSoilPast.nc \
                && -f ${tag}_cSoilGrass.nc && -f ${tag}_cSoilShrub.nc  && -f ${tag}_cSoilTree.nc ]]; then
        cdo enssum -mul ${tag}_cSoilCrop.nc  ${tag}_cropFrac.nc    \
                   -mul ${tag}_cSoilPast.nc  ${tag}_pastureFrac.nc \
                   -mul ${tag}_cSoilGrass.nc ${tag}_grassFrac.nc   \
                   -mul ${tag}_cSoilShrub.nc ${tag}_shrubFrac.nc   \
                   -mul ${tag}_cSoilTree.nc  ${tag}_treeFrac.nc   ${tag}_cSoil.test
        cdo sub -mulc,100. ${tag}_cSoil.nc ${tag}_cSoil.test test05.nc        # *100 as fraction arrays are in %
    fi

    # Test 06: cSoil =  SUM(csoilPools(:))
    if [[ -f ${tag}_cSoil.nc && -f ${tag}_cSoilPools.nc ]]; then
        cdo vertsum  ${tag}_cSoilPools.nc   ${tag}_cSoil.test
        cdo sub ${tag}_cSoil.nc ${tag}_cSoil.test test06.nc
    fi

    # Test 07: cVeg = cVegGrass*grassFrac + cVegShrub*shrubFrac + cVegTree*treeFrac + cVegCrop*cropFrac + cVegPast*pastureFrac
    [[ -f ${tag}_cVeg.test ]] && rm ${tag}_cVeg.test
    if [[ -f ${tag}_cropFrac.nc && -f ${tag}_pastureFrac.nc && -f ${tag}_grassFrac.nc && -f ${tag}_shrubFrac.nc \
                && -f ${tag}_treeFrac.nc && -f ${tag}_cVeg.nc && -f ${tag}_cVegCrop.nc && -f ${tag}_cVegPast.nc \
                && -f ${tag}_cVegGrass.nc && -f ${tag}_cVegShrub.nc  && ${tag}_cVegTree.nc ]]; then
        cdo enssum -mul ${tag}_cVegCrop.nc  ${tag}_cropFrac.nc    \
                   -mul ${tag}_cVegPast.nc  ${tag}_pastureFrac.nc \
                   -mul ${tag}_cVegGrass.nc ${tag}_grassFrac.nc   \
                   -mul ${tag}_cVegShrub.nc ${tag}_shrubFrac.nc   \
                   -mul ${tag}_cVegTree.nc  ${tag}_treeFrac.nc   ${tag}_cVeg.test
        cdo sub -mulc,100. ${tag}_cVeg.nc ${tag}_cVeg.test test07.nc        # *100 as fraction arrays are in %
    fi

    # Test 08: fNgas = fNgasFire + fNgasNonFire
    if [[ -f ${tag}_fNgas.nc && -f ${tag}_fNgasFire.nc && -f ${tag}_fNgasNonFire.nc ]]; then
        cdo add ${tag}_fNgasFire.nc   \
                ${tag}_fNgasNonFire.nc  ${tag}_fNgas.test
        cdo sub ${tag}_fNgas.nc ${tag}_fNgas.test test08.nc
    fi

    # Test 09: fNloss = fNgas + fNleach
    if [[ -f ${tag}_fNloss.nc && -f ${tag}_fNgas.nc && -f ${tag}_fNleach.nc ]]; then
        cdo add ${tag}_fNgas.nc  \
                ${tag}_fNleach.nc  ${tag}_fNloss.test
        cdo sub ${tag}_fNloss.nc ${tag}_fNloss.test test09.nc
    fi

    # Test 10: gpp = npp + ra
    if [[ -f ${tag}_fNloss.nc && -f ${tag}_npp.nc && -f ${tag}_ra.nc ]]; then
        cdo add ${tag}_npp.nc \
                ${tag}_ra.nc   ${tag}_gpp.test
        cdo sub ${tag}_gpp.nc ${tag}_gpp.test test10.nc
    fi

    # Test 11: gpp =  gppGrass*grassFrac + gppShrub*shrubFrac + gppTree*treeFracc + gppCrop*cropFrac + gppPast*pastureFrac
    [[ -f ${tag}_gpp.test ]] && rm ${tag}_gpp.test
    if [[ -f ${tag}_cropFrac.nc && -f ${tag}_pastureFrac.nc && -f ${tag}_grassFrac.nc && -f ${tag}_shrubFrac.nc \
                && -f ${tag}_treeFrac.nc && -f ${tag}_gpp.nc && -f ${tag}_gppCrop.nc && -f ${tag}_gppPast.nc \
                && -f ${tag}_gppGrass.nc && -f ${tag}_gppShrub.nc && -f ${tag}_gppTree.nc ]]; then
        cdo enssum -mul ${tag}_gppCrop.nc  ${tag}_cropFrac.nc    \
                   -mul ${tag}_gppPast.nc  ${tag}_pastureFrac.nc \
                   -mul ${tag}_gppGrass.nc ${tag}_grassFrac.nc   \
                   -mul ${tag}_gppShrub.nc ${tag}_shrubFrac.nc   \
                   -mul ${tag}_gppTree.nc  ${tag}_treeFrac.nc   ${tag}_gpp.test
        cdo sub -mulc,100. ${tag}_gpp.nc ${tag}_gpp.test test11.nc        # *100 as fraction arrays are in %
    fi

    # Test 12: grassFrac = grassFracC3 + grassFracC4
    if [[ -f ${tag}_grassFrac.nc && -f ${tag}_grassFracC3.nc && -f ${tag}_grassFracC4.nc ]]; then
        cdo add ${tag}_grassFracC3.nc \
                ${tag}_grassFracC4.nc  ${tag}_grassFrac.test
        cdo sub ${tag}_grassFrac.nc ${tag}_grassFrac.test test12.nc
    fi

    # Test 13: mrro = mrrob + mrros
    if [[ -f ${tag}_mrro.nc && -f ${tag}_mrrob.nc && -f ${tag}_mrros.nc ]]; then
        cdo add ${tag}_mrrob.nc \
                ${tag}_mrros.nc  ${tag}_mrro.test
        cdo sub ${tag}_mrro.nc ${tag}_mrro.test test13.nc
    fi

    # Test 14: nLand = nVeg + nLitter + nSoil + nProduct + nMineral
    [[ -f ${tag}_nLand.test ]] && rm ${tag}_nLand.test
    if [[ -f ${tag}_nLand.nc && -f ${tag}_nVeg.nc && -f ${tag}_nLitter.nc && -f ${tag}_nSoil.nc \
                && -f ${tag}_nProduct.nc && -${tag}_nMineral.nc ]]; then
        cdo enssum ${tag}_nVeg.nc     \
                   ${tag}_nLitter.nc  \
                   ${tag}_nSoil.nc    \
                   ${tag}_nProduct.nc \
                   ${tag}_nMineral.nc  ${tag}_nLand.test
        cdo sub ${tag}_nLand.nc ${tag}_nLand.test test14.nc
    fi

    # Test 15: nSoil = nSoilFast + nSoilSlow
    if [[ -f ${tag}_nSoil.nc && -f ${tag}_nSoilFast.nc && -f ${tag}_nSoilSlow.nc ]]; then
        cdo add ${tag}_nSoilFast.nc \
                ${tag}_nSoilSlow.nc  ${tag}_nSoil.test
        cdo sub ${tag}_nSoil.nc ${tag}_nSoil.test test15.nc
    fi

    # Test 16: npp =  nppGrass*grassFrac + nppShrub*shrubFrac + nppTree*treeFracc + nppCrop*cropFrac + nppPast*pastureFrac
    [[ -f ${tag}_npp.test ]] && rm ${tag}_npp.test
    if [[ -f ${tag}_cropFrac.nc && -f ${tag}_pastureFrac.nc && -f ${tag}_grassFrac.nc && -f ${tag}_shrubFrac.nc \
                && -f ${tag}_treeFrac.nc && -f ${tag}_npp.nc && -f ${tag}_nppCrop.nc && -f ${tag}_nppPast.nc \
                && -f ${tag}_nppGrass.nc && -f ${tag}_nppShrub.nc  && -f ${tag}_nppTree.nc ]]; then
        cdo enssum -mul ${tag}_nppCrop.nc  ${tag}_cropFrac.nc    \
                   -mul ${tag}_nppPast.nc  ${tag}_pastureFrac.nc \
                   -mul ${tag}_nppGrass.nc ${tag}_grassFrac.nc   \
                   -mul ${tag}_nppShrub.nc ${tag}_shrubFrac.nc   \
                   -mul ${tag}_nppTree.nc  ${tag}_treeFrac.nc  ${tag}_npp.test
        cdo sub -mulc,100. ${tag}_npp.nc ${tag}_npp.test test16.nc        # *100 as fraction arrays are in %
    fi

    # Test 17: pastureFrac = pastureFracC3 + pastureFracC4
    if [[ -f ${tag}_pastureFrac.nc && -f ${tag}_pastureFracC3.nc && -f ${tag}_pastureFracC4.nc ]]; then
        cdo add ${tag}_pastureFracC3.nc \
                ${tag}_pastureFracC4.nc  ${tag}_pastureFrac.test
        cdo sub ${tag}_pastureFrac.nc ${tag}_pastureFrac.test test17.nc
    fi

    # Test 18: ra = raGrass*grassFrac + raShrub*shrubFrac + raTree*treeFrac + raCrop*cropFrac + raPast*pastureFrac
    [[ -f ${tag}_ra.test ]] && rm ${tag}_ra.test
    if [[ -f ${tag}_cropFrac.nc && -f ${tag}_pastureFrac.nc && -f ${tag}_grassFrac.nc && -f ${tag}_shrubFrac.nc \
                && -f ${tag}_treeFrac.nc && -f ${tag}_ra.nc && -f ${tag}_raCrop.nc && -f ${tag}_raPast.nc && -f ${tag}_raGrass.nc \
                && -f ${tag}_raShrub.nc  && -f ${tag}_raTree.nc ]]; then
        cdo enssum -mul ${tag}_raCrop.nc  ${tag}_cropFrac.nc    \
                   -mul ${tag}_raPast.nc  ${tag}_pastureFrac.nc \
                   -mul ${tag}_raGrass.nc ${tag}_grassFrac.nc   \
                   -mul ${tag}_raShrub.nc ${tag}_shrubFrac.nc   \
                   -mul ${tag}_raTree.nc  ${tag}_treeFrac.nc  ${tag}_ra.test
        cdo sub -mulc,100. ${tag}_ra.nc ${tag}_ra.test test18.nc        # *100 as fraction arrays are in %
    fi

    # Test 19: rh = rhGrass*grassFrac + rhShrub*shrubFrac + rhTree*treeFracc + rhCrop*cropFrac + rhPast*pastureFrac
    [[ -f ${tag}_rh.test ]] && rm ${tag}_rh.test
    if [[ -f ${tag}_cropFrac.nc && -f ${tag}_pastureFrac.nc && -f ${tag}_grassFrac.nc && -f ${tag}_shrubFrac.nc \
                && -f ${tag}_treeFrac.nc && -f ${tag}_rh.nc && -f ${tag}_rhCrop.nc && -f ${tag}_rhPast.nc && -f ${tag}_rhGrass.nc \
                && -f ${tag}_rhShrub.nc  && -f ${tag}_rhTree.nc ]]; then
        cdo enssum -mul ${tag}_rhCrop.nc  ${tag}_cropFrac.nc    \
                   -mul ${tag}_rhPast.nc  ${tag}_pastureFrac.nc \
                   -mul ${tag}_rhGrass.nc ${tag}_grassFrac.nc   \
                   -mul ${tag}_rhShrub.nc ${tag}_shrubFrac.nc   \
                   -mul ${tag}_rhTree.nc  ${tag}_treeFrac.nc  ${tag}_rh.test
        cdo sub -mulc,100. ${tag}_rh.nc ${tag}_rh.test test19.nc        # *100 as fraction arrays are in %
    fi

    # Test 20: landCoverFrac: sum of all cover types == 100 %
    if [[ -f ${tag}_landCoverFrac.nc ]]; then
        if [[ ${specialWish} = trendy ]]; then
          cdo subc,1. -vertsum ${tag}_landCoverFrac.nc test20.nc         
        else
          cdo subc,100. -vertsum ${tag}_landCoverFrac.nc test20.nc
        fi
    fi

    # Test 21: landCoverFrac = vegFrac + baresoilFrac + residualFrac
    [[ -f ${tag}_landCoverFrac.test ]] && rm ${tag}_landCoverFrac.test
    if [[ -f ${tag}_landCoverFrac.nc && -f ${tag}_vegFrac.nc && -f ${tag}_baresoilFrac.nc \
                && -f ${tag}_residualFrac.nc ]]; then
        cdo enssum ${tag}_vegFrac.nc      \
            ${tag}_baresoilFrac.nc \
            ${tag}_residualFrac.nc  ${tag}_landCoverFrac.test
        cdo sub -vertsum ${tag}_landCoverFrac.nc ${tag}_landCoverFrac.test test21.nc
    fi

    # Test 22: vegFrac = treeFrac + shrubFrac + grassFrac + cropFrac + pastureFrac
    [[ -f ${tag}_vegFrac.test ]] && rm ${tag}_vegFrac.test
    if [[ -f ${tag}_vegFrac.nc && -f ${tag}_treeFrac.nc && -f ${tag}_shrubFrac.nc && -f ${tag}_grassFrac.nc \
          && -f ${tag}_cropFrac.nc && -f ${tag}_pastureFrac.nc ]]; then
        cdo enssum ${tag}_treeFrac.nc   \
            ${tag}_shrubFrac.nc  \
            ${tag}_grassFrac.nc  \
            ${tag}_cropFrac.nc   \
            ${tag}_pastureFrac.nc  ${tag}_vegFrac.test
        cdo sub ${tag}_vegFrac.nc ${tag}_vegFrac.test test22.nc
    fi

    # Test 23: vegfrac = c3PftFrac + c4PftFrac
    if [[ -f ${tag}_vegFrac.nc && -f ${tag}_c3PftFrac.nc && -f ${tag}_c4PftFrac.nc ]]; then
        cdo add ${tag}_c3PftFrac.nc \
                 ${tag}_c4PftFrac.nc  ${tag}_vegFrac.test
        cdo sub ${tag}_vegFrac.nc ${tag}_vegFrac.test test23.nc
    fi

    # Test 24: fProductDecomp = fHarvest + fLuc  
    if [[ -f ${tag}_fProductDecomp.nc && -f ${tag}_fHarvest.nc && -f ${tag}_fLuc.nc ]]; then
       cdo sub ${tag}_fProductDecomp.nc -add ${tag}_fHarvest.nc ${tag}_fLuc.nc test24.nc
    fi

    # Test 25: nbp = gpp - ra - rh - fFire - fHarvest - fLuc    (fGrazing is included in rh)
    [[ -f ${tag}_nbp.test ]] && rm ${tag}_nbp.test
    if [[ -f ${tag}_nbp.nc && -f ${tag}_gpp.nc && -f ${tag}_ra.nc && -f ${tag}_rh.nc && -f ${tag}_fFire.nc \
                && -f ${tag}_fHarvest.nc && -f ${tag}_fLuc.nc ]]; then
        cdo enssum           ${tag}_gpp.nc      \
                   -mulc,-1. ${tag}_ra.nc       \
                   -mulc,-1. ${tag}_rh.nc       \
                   -mulc,-1. ${tag}_fFire.nc    \
                   -mulc,-1. ${tag}_fHarvest.nc \
                   -mulc,-1. ${tag}_fLuc.nc      ${tag}_nbp.test
        cdo sub ${tag}_nbp.nc ${tag}_nbp.test test25.nc
    fi

    # Test 26: nep = gpp - ra - rh - fFire
    [[ -f ${tag}_nep.test ]] && rm ${tag}_nep.test
    if [[ -f ${tag}_nep.nc && -f ${tag}_gpp.nc && -f ${tag}_ra.nc && -f ${tag}_rh.nc && -f ${tag}_fFire.nc ]]; then
        cdo enssum            ${tag}_gpp.nc   \
                    -mulc,-1. ${tag}_ra.nc    \
                    -mulc,-1. ${tag}_rh.nc    \
                    -mulc,-1. ${tag}_fFire.nc  ${tag}_nep.test
        cdo sub ${tag}_nep.nc ${tag}_nep.test test26.nc
    fi

    # Test 27: d/dt(cProduct) = fDeforestToProduct + fHarvestToProduct - fProductDecomp
    #   Note: All d/dt tests do not lead to zero-arrays, due to the time mismatch of monthly mean fluxes and monthly mean pool
    #   values. The test arrays need to be checked individually. I do not see an easy setup for an automatic test.
    [[ -f ${tag}_fluxes_cProduct.test ]] && rm ${tag}_fluxes_cProduct.test
    if [[ -f ${tag}_cProduct.nc && -f ${tag}_fDeforestToProduct.nc && -f ${tag}_fHarvestToProduct.nc \
                && -f ${tag}_fProductDecomp.nc ]]; then
        cdo enssum ${tag}_fDeforestToProduct.nc \
                   ${tag}_fHarvestToProduct.nc  \
            -mulc,-1. ${tag}_fProductDecomp.nc   ${tag}_fluxes_cProduct.test
        cdo sub -deltat ${tag}_cProduct.nc  -mulc,86400. -muldpm ${tag}_fluxes_cProduct.test  test27.nc
    fi

    # Test 28: d/dt(cLand) = npp - fFire - rh - fCLandToOcean - fAnthDisturb - fProductDecomp    (fCLandTocean=0., fAnthDisturb=0.)
    #  Differences due to 15 day time shift between monthly mean fluxes and monthly mean state 
    [[ -f ${tag}_fluxes_cLand.test ]] && rm ${tag}_fluxes_cLand.test
    if [[ -f ${tag}_cLand.nc && -f ${tag}_npp.nc && -f ${tag}_fFire.nc && -f ${tag}_rh.nc && -f ${tag}_fProductDecomp.nc ]]; then
        cdo enssum           ${tag}_npp.nc           \
                   -mulc,-1. ${tag}_fFire.nc         \
                   -mulc,-1. ${tag}_rh.nc            \
                   -mulc,-1. ${tag}_fProductDecomp.nc  ${tag}_fluxes_cLand.test
        cdo sub  -deltat ${tag}_cLand.nc  -mulc,86400. -muldpm ${tag}_fluxes_cLand.test  test28.nc
    fi

    # Test 29: d/dt(cLand) = npp - fFire - rh - fHarvest - fLuc    (for CRESCENDO; fgrazing included in rh)
    #  Differences due to 15 day time shift between monthly mean fluxes and monthly mean state 
    [[ -f ${tag}_fluxes_cLand.test ]] && rm ${tag}_fluxes_cLand.test
    if [[ -f ${tag}_cLand.nc && -f ${tag}_npp.nc && -f ${tag}_fFire.nc && -f ${tag}_rh.nc && -f ${tag}_fHarvest.nc \
                && -f ${tag}_fLuc.nc ]]; then
        cdo enssum           ${tag}_npp.nc      \
                   -mulc,-1. ${tag}_fFire.nc    \
                   -mulc,-1. ${tag}_rh.nc       \
                   -mulc,-1. ${tag}_fHarvest.nc \
                   -mulc,-1. ${tag}_fLuc.nc      ${tag}_fluxes_cLand.test
        cdo sub  -deltat ${tag}_cLand.nc  -mulc,86400. -muldpm ${tag}_fluxes_cLand.test  test29.nc
    fi

    # Test 30: d/dt(nLand) = fBNF + fNdep + fNfert - fNloss - fNAnthDisturb                    (fNfert = 0.)
    #   This test does not work on glacier points, where we have a small N deposition flux but no N pools.
    #   Besides, with monthly mean output there is a 15 day shift between fluxes and states. This might lead to huge "errors" if
    #   fluxes have a high variablility (as e.g. the fire flux) or with long time trends.
    [[ -f ${tag}_fluxes_nLand.test ]] && rm ${tag}_fluxes_nLand.test
    if [[ -f ${tag}_nLand.nc && -f ${tag}_fBNF.nc && -f ${tag}_fNdep.nc && -f ${tag}_fNAnthDisturb.nc ]]; then
        cdo enssum           ${tag}_fBNF.nc        \
                             ${tag}_fNdep.nc       \
                   -mulc,-1. ${tag}_fNloss.nc      \
                   -mulc,-1. ${tag}_fNAnthDisturb.nc  ${tag}_fluxes_nLand.test
        cdo sub  -deltat ${tag}_nLand.nc  -mulc,86400 -muldpm ${tag}_fluxes_nLand.test  test30.nc
    fi

    # Test 31: d/dt(cLand) = netAtmosLandCO2Flux
    #  Differences due to 15 day time shift between monthly mean fluxes and monthly mean state 
    if [[ -f ${tag}_cLand.nc && -f ${tag}_netAtmosLandCO2Flux.nc ]]; then
        cdo sub  -deltat ${tag}_cLand.nc  -muldpm -mulc,86400. ${tag}_netAtmosLandCO2Flux.nc  test31.nc
    fi
 
    # Test 32: fracLuc: sum of all tiles + residual fraction == 100 %
    if [[ -f ${tag}_fracLut.nc && -f ${tag}_residualFrac.nc ]]; then
        cdo subc,100. -add -vertsum ${tag}_fracLut.nc ${tag}_residualFrac.nc  test32.nc
    fi

    # Test 33: cVeg = SUM(cVegLut(:) * fracLut(:))
    if [[ -f ${tag}_cVeg.nc && -f ${tag}_cVegLut.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_cVeg.nc -vertsum -mul ${tag}_cVegLut.nc -mulc,0.01 ${tag}_fracLut.nc  test33.nc
    fi

    # Test 34: cLitter = SUM(cLitterLut(:) * fracLut(:))
    if [[ -f ${tag}_cLitter.nc && -f ${tag}_cLitterLut.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_cLitter.nc -vertsum -mul ${tag}_cLitterLut.nc -mulc,0.01 ${tag}_fracLut.nc  test34.nc
    fi

    # Test 35: cSoil = SUM(cSoilLut(:) * fracLut(:))
    if [[ -f ${tag}_cSoil.nc && -f ${tag}_cSoilLut.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_cSoil.nc -vertsum -mul ${tag}_cSoilLut.nc -mulc,0.01 ${tag}_fracLut.nc  test35.nc
    fi

    # Test 36: gpp = SUM(gppLut(:) * fracLut(:))
    if [[ -f ${tag}_gpp.nc && -f ${tag}_gppLut.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_gpp.nc -vertsum -mul ${tag}_gppLut.nc -mulc,0.01 ${tag}_fracLut.nc  test36.nc
    fi

    # Test 37: npp = SUM(nppLut(:) * fracLut(:))
    if [[ -f ${tag}_npp.nc && -f ${tag}_nppLut.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_npp.nc -vertsum -mul ${tag}_nppLut.nc -mulc,0.01 ${tag}_fracLut.nc  test37.nc
    fi

    # Test 38: ra = SUM(raLut(:) * fracLut(:))
    if [[ -f ${tag}_ra.nc && -f ${tag}_raLut.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_ra.nc -vertsum -mul ${tag}_raLut.nc -mulc,0.01 ${tag}_fracLut.nc  test38.nc
    fi

    # Test 39: rh = SUM(rhLut(:) * fracLut(:))
    if [[ -f ${tag}_rh.nc && -f ${tag}_rhLut.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_rh.nc -vertsum -mul ${tag}_rhLut.nc -mulc,0.01 ${tag}_fracLut.nc  test39.nc
    fi

    # Test 40: fracLut(crp) = cropFrac
    if [[ -f ${tag}_cropFrac.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_cropFrac.nc -sellevel,2 ${tag}_fracLut.nc test40.nc
    fi

    # Test 41: fracLut(pst) = pastureFrac
    if [[ -f ${tag}_pastureFrac.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub ${tag}_pastureFrac.nc -sellevel,3 ${tag}_fracLut.nc test41.nc
    fi

    # Test 42: d/dt fracLut = fracInLut - fracOutLut
    #      test is not exactly fullfilled, as transitions from crop/pasture to natural vegetation are sometimes
    #      ignored in jsbach, to avoid spontaneous forest regrowth, in case there is not enough natural grass land.
    if [[ -f ${tag}_fracInLut.nc && -f ${tag}_fracOutLut.nc && -f ${tag}_fracLut.nc ]]; then
        cdo sub -deltat -selmon,1 ${tag}_fracLut.nc -sub ${tag}_fracInLut.nc ${tag}_fracOutLut.nc test42.nc
    fi

    # Test 43: gppLut(crp) = gppCrop
    if [[ -f ${tag}_gppCrop.nc && -f ${tag}_gppLut.nc ]]; then
        cdo sub ${tag}_gppCrop.nc -sellevel,2 ${tag}_gppLut.nc test43.nc
    fi

    # Test 44: gppLut(pst) = gppPast
    if [[ -f ${tag}_gppPast.nc && -f ${tag}_gppLut.nc ]]; then
        cdo sub ${tag}_gppPast.nc -sellevel,3 ${tag}_gppLut.nc test44.nc
    fi

    # Test 45: nppLut(crp) = nppCrop
    if [[ -f ${tag}_nppCrop.nc && -f ${tag}_nppLut.nc ]]; then
        cdo sub ${tag}_nppCrop.nc -sellevel,2 ${tag}_nppLut.nc test45.nc
    fi

    # Test 46: nppLut(pst) = nppPast
    if [[ -f ${tag}_nppPast.nc && -f ${tag}_nppLut.nc ]]; then
        cdo sub ${tag}_nppPast.nc -sellevel,3 ${tag}_nppLut.nc test46.nc
    fi

    # Test 47: raLut(crp) = raCrop
    if [[ -f ${tag}_raCrop.nc && -f ${tag}_raLut.nc ]]; then
        cdo sub ${tag}_raCrop.nc -sellevel,2 ${tag}_raLut.nc test47.nc
    fi

    # Test 48: raLut(pst) = raPast
    if [[ -f ${tag}_raPast.nc && -f ${tag}_raLut.nc ]]; then
        cdo sub ${tag}_raPast.nc -sellevel,3 ${tag}_raLut.nc test48.nc
    fi

    # Test 49: netAtmosLandCO2Flux = fco2nat + fco2antt - fco2fos
    if [[ -f ${tag}_fco2nat.nc && -f ${tag}_fco2antt.nc && -f ${tag}_fco2fos.nc && -f ${tag}_netAtmosLandCO2Flux.nc ]]; then
        cdo sub ${tag}_netAtmosLandCO2Flux.nc -add ${tag}_fco2nat.nc -sub ${tag}_fco2antt.nc  ${tag}_fco2fos.nc test49.nc
    fi

    # Test 50: fco2antt = fco2fos + fProductDecomp   (vg: ???)
    if [[ -f ${tag}_fco2antt.nc && -f ${tag}_fco2fos.nc && -f ${tag}_fProductDecomp.nc ]]; then
        cdo sub ${tag}_fco2antt.nc -add ${tag}_fco2fos.nc ${tag}_fProductDecomp.nc test50.nc
    fi

    # Test 51: fco2nat (on land) = -1 * nep
    #   Note: There is a one day time shift between veg stream and co2 stream, with the veg stream leading.
    if [[ -f ${tag}_fco2nat.nc && -f ${tag}_nep.nc ]]; then
        cdo add ${tag}_fco2nat.nc ${tag}_nep.nc test51.nc
    fi

    # Test 52: nbp = netAtmosLandCO2Flux
    #   This test only makes sense in simulations with echam, as nbp is calculated from jsbach variables whereas netAtmosLandCO2flux
    #   is calculated from the co2 stream variable co2_flux_land. In jsbach offline simulations both variables are calculated 
    #   identically.
    #   Note: There is a one day time shift between veg stream and co2 stream, with the veg stream leading.
    if [[ -f ../${inf_co2} && -f ${tag}_nbp.nc && -f ${tag}_netAtmosLandCO2Flux.nc ]]; then
        cdo sub ${tag}_nbp.nc ${tag}_netAtmosLandCO2Flux.nc test52.nc
    fi

    # Test  : d/dt(nMineral) = fNdep + fNfert + fNnetmin - fNloss - fNup                      (fNfert = 0.)
    #   In jsbach there is a N-flux going from the nProduct pool to the nMineral pool (Nflx_crop_harvest_2_SMINN). This
    #   flux is not in the output. In longer equilibrium simulation, this flux corresponds to FNProduct, the flux going 
    #   into the nProduct pool.
    #   Besides, in jsbach we have a N fluxes to the mineral N pool associated with herbivory, with fire, and with wood
    #   harvest. Thus this test does not work with jsbach.

    # Test 53: rh = rhSoil + fGrazing
    if [[ -f ${tag}_rh.nc && -f ${tag}_rhSoil.nc && -f ${tag}_fGrazing.nc ]]; then
        cdo sub ${tag}_rh.nc -add ${tag}_rhSoil.nc ${tag}_fGrazing.nc test53.nc
    fi

    # move test arrays to tests directory
    if [[ $(ls test??.nc | wc -l) > 0 ]]; then
        [[ -d ../tests ]] || mkdir ../tests
        mv test??.nc ../tests
    fi

    cd ..
fi

# clean up
if [[ ${specialWish} != dailyLAI ]]; then
    rm  *_0000??.nc
    rm  cover_type cover_fract veg_ratio_max box_cover_fract slm bare gridareafrac.nc C3C4_crop_mask glac land
    rm  divcrop  divgrass  divpast  divshrub  divtree
    rm -f nat psl crp pst divpsl divcrp divpst divurb box_cover_fract_pot
    rm *.tmp *.partab
    rm -f *.tmp1 *.tmp2 *.slf lai_pft_0000??.nc
    rm -f outdata_land/*.test outdata_land/*.test1 outdata_land/*.test2
fi
exit
