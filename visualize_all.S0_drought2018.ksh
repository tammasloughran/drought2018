#!/bin/ksh
#
# Script for ECHAM and CO2 inventory visualization of cosmos-asob experiment.
# Script retrieves millennium model data for storage on squall (for this
#  purpose you have to generate the necessary directories).
#
# Edit the parameters in the next section. The script requires the following 
# files in the same directory:
# - fixcode_${RES}, fixcode_${RES}.ctl and fixcode_${RES}.gmp
#   -- These files depend on the atmosphere and ocean grid resolutions --
# - co2inventory.sh
# - plot_lines_co2.sh
# - plot_enso.sh
# - carbon_land.sh
# - forestfract.sh   (with option dynveg=yes)
# - cmor_land.sh     (with option cmor=yes)
# 
# The script is called without parameters and then automatically gets the data 
# from the specified location and creates the following time series plots:
# - years_${EXP}_[1-10].ps      : plots of several atmospheric variables
# - years_${EXP}_co2_[1-3].ps   : plots of CO2 burden and CO2 fluxes
# - invent_${EXP}.pdf           : plots of Carbon inventory
# - cland_${EXP}.pdf            : plots of land carbon pools
# - dynveg_${EXP}.pdf           : plots of vegetation fractions
# With option cmor=yes monthly mean jsbach and echam output is converted to
# files that have the same variable names and units as the data in the cmip5
# archive.  
#
# You can run the script repeatedly for a running experiment ... it then 
# automatically checks for new data and updates the plots.
#
# IMPORTANT:
# ==========
# If you specify a remote data location you must set up access to the remote 
# machine so that ssh works without password.
#
# The script creates the subdirectories "outdata" and "outdata_land" in the 
# current directory, so you should run this in a separate directory for each
# experiment.
#
# The script needs the R software package. 
# The R packages "igraph" is needed ... if it is not 
# available for your R version you can either use a newer R version which has it 
# installed, use a different machine where it is installed for R (e.g. debian etch),
# or you can install it yourself: in an interactive R session, issue the command: 
# install.packages("igraph")
#
# The script also requires the cdo package (http://www.mpimet.mpg.de/~cdo) and 
# the seq program.
#
# Author: Reiner Schnur with changes by Veronika Gayler July 2008
#         changes by Stephan Lorenz August 2008 (annual .sz files from thumper)
#   Further changes: Monika Esch, MPI, 2008
#   Monika Esch, Dec 2008
#     Automatic retrieval of Millennium data
#   Veronika Gayler, June 2009
#     - Merge with Reiners/Stephans version (contrib/plots/echam5j/tsplot.tar)
#     - removed millennium specific hard coded directory names
#     - corrections for local and monthly usage
#     - removed ZMAW path to R
#   Veronika Gayler, April 2012
#     - adapt the script for jsbach or echam/jsbach experiments
#     - added cmor-like processing of jsbach and selected echam variables
#     - added plot of different land carbon pools 
#   Veronika Gayler, October 2014
#     - adaptation to yasso
#===============================================================================
#
###############################################################################
### Batch Queuing System is SLURM
#SBATCH --job-name=visualize_all.S0_drought2018
#SBATCH --output=visualize_all.S0_drought2018.o%j
#SBATCH --error=visualize_all.S0_drought2018.o%j
#SBATCH --partition=prepost
#SBATCH --mem-per-cpu=5120
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=20
#SBATCH --account bm0891
#SBATCH --mail-type=FAIL       # Notify user by email in case of job failure
#SBATCH --mail-user=t.loughran@lmu.de # Set your e-mail address
cd /work/bm0891/m300719/drought2018/output/postprocessed/S0

set -e

banner () {
    echo "+-$(echo $* | sed 's/./-/g')-+"
    echo "| $* |"
    echo "+-$(echo $* | sed 's/./-/g')-+"
}
trap 'banner $0:$LINENO: command returned $?' ERR

# --------------------------------------------------------------------
# For interactive usage: Edit the following parameters:
EXP=S0_drought2018                # Experiment name
remote=          # Data are on remote machine, e.g. cross.dkrz.de
                        # (empty if local)
datadir=/scratch/m/m300719/drought2018/mpiesm-landveg/experiments/${EXP}
                        # Directory containing data (root directory of "outdata")
plotdir=/work/bm0891/m300719/drought2018/mpiesm-landveg/experiments/${EXP}/plots  # Directory for visualization plots
year1=1979              # First year of the Experiment
file_period=year        # month or year: ECHAM and JSBACH data are stored in one 
                        # file for each month or one file per year
fext=grb                # sz/grb: File extension szip to be converted to grib
fext1=grb               # sz/grb: File format of veg stream
rmsc=no                # yes/no: Source files are deleted
RES=T255           # grid acronym, defining the ECHAM land sea mask
jsbach_init_file=/work/bm0891/m300719/drought2018/indata/jsbach_T255TP04_11tiles_5layers_2010_no-dynveg_ocean.nc
                        # jsbach initial file used in the experiment
echam_init_file=/pool/data/ECHAM6/input/r0007/T63/${RES}_jan_surf.nc
                        # echam initial file with surface data
echam_fractional=no     # echam uses fractional land sea mask
accw_dir=echam6         # echam6/jsbach: directory of accw stream
accw_tag=accw           # depending on the setup accw or accw_mm
dynveg=no              # Plot vegetation fractions for dynveg runs (yes/no)
yasso=yes               # Simulation with yasso soil carbon scheme
nitrogen=yes            # Simulation with nitrogen cycle
cmor=yes                # cmor-like monthly jsbach and echam output (yes/no)
ls3mip=no               # cmor-like output including ls3mip variables
specialWish=dailyLAI      # cmor-like output only for certain/extra variables -- (crescendo/trendy/dailyLAI)
output_tag=JSBACH_S0    # e.g. JSBACH_<scenario>
echam_ts=no             # plot echam time series (also in standard echam pp)
rebuild_plots=yes       # Rebuild plots even if there are no new files
#---------------------------------------------------------------------
#
# Check for environment overrides
#
if [[ -n "$EXP_NAME" && "$EXP_NAME" != "$EXP" ]]; then
    datadir=$(echo "$datadir" | sed "s,\<$EXP\>,$EXP_NAME,g")
    plotdir=$(echo "$plotdir" | sed "s,\<$EXP\>,$EXP_NAME,g")
    EXP=$EXP_NAME
fi
remote="${REMOTE_HOST-$remote}" # may be set to empty
DATA_DIR=$DATA_DIR${DATA_DIR:+/$EXP}
datadir=${DATA_DIR:-$datadir}
plotdir=${PLOT_DIR:-$plotdir}
year1=${YEAR_START:-$year1}
rmsc=${VISALL_CLEAN:-$rmsc}
RES=${MODEL_RES:-$RES}
echam_fractional=${VISALL_SLF:-$echam_fractional}
cmor=${VISALL_CMOR:-$cmor}
echam_ts=${VISALL_ECHAM:-$echam_ts}
case ${REBUILD:-false} in true|yes|on|1) rebuild_plots=yes;; esac
echam_init_file=${VISALL_ECHAM_SURF:-$echam_init_file}
jsbach_init_file=${VISALL_JSBACH_INI:-$jsbach_init_file}
interval_months=${INTERVAL_MONTHS:-12}
#---------------------------------------------------------------------

# Look up installation directory
#BIN_DIR=$(dirname $0)
BIN_DIR=$(pwd)

# Create plot directory if needed
[[ -d ${plotdir} ]] || mkdir ${plotdir}

# Load cdo module if needed
. $BIN_DIR/use_module.ksh
use cdo cdo
use grads grads IGES
use ncatted nco NCO

# Load R if needed
if type R > /dev/null 2>&1
then
    R=R
else
    module load r
fi

# Check cdo version: some 
if [[ $(which cdo | grep 1.9.1-gccsys ) != "" || $(which cdo | grep 1.9.2-gccsys ) != "" ]]; then
  echo "change cdo version: 1.9.1-gccsys and 1.9.2-gccsys have memory problems with enssum"
  exit 1
fi

if [[ $remote != "" ]]; then
  CP_RAW="scp -p ${remote}:"
else
  CP_RAW="cp -p "
fi
CP="${CP_RAW}${datadir}/"

[[ ${file_period} = month ]] && fext=grb

# check for model components

echam6=false;  jsbach=false;  hamocc=false
if [[ $remote != "" ]]; then
  $(ssh $remote test -d ${datadir}/outdata/echam6) && echam6=true
  $(ssh $remote test -d ${datadir}/outdata/jsbach) && jsbach=true
  $(ssh $remote test -d ${datadir}/outdata/hamocc) && hamocc=true
else
  [[ -d ${datadir}/outdata/echam6 ]] && echam6=true
  [[ -d ${datadir}/outdata/jsbach ]] && jsbach=true
  [[ -d ${datadir}/outdata/hamocc ]] && hamocc=true
fi
#

print -- "--- Visualizing experiment $EXP"
print -- "       `date`            "
print -- "--- check processed years"
#
# Check which years are already processed
years=`cdo showyear outdata/veg.grb 2>/dev/null` || years="$year1 $(( year1-1 ))"
years=${years##+([ ])}   # Get rid of leading blanks
yr1=${years%% *}         # First year
yr2=${years##* }         # Last year

# Check for updates in data location
if [[ $remote != "" ]]; then
  if [[ ${file_period} = year ]]; then
    years=`ssh $remote ls ${datadir}/outdata/jsbach/\*veg_mm_????.${fext1} 2>/dev/null`
  else
    years=`ssh $remote ls ${datadir}/outdata/jsbach/\*veg_mm_????12.grb 2>/dev/null`
  fi
else
  if [[ ${file_period} = year ]]; then
    years=`ls ${datadir}/outdata/jsbach/*veg_mm_????.${fext1} 2>/dev/null`
  else
    years=`ls ${datadir}/outdata/jsbach/*veg_mm_????12.grb 2>/dev/null`
  fi
fi
yr3=${years##*_}
if  [[ ${file_period} = year ]]; then
  yr3=${yr3%.${fext1}}   # Last year available
else
  yr3=${yr3%12.grb}     # Last year available
fi
yr3=${yr3##+([0])}      # Remove leading zeros

#vg>>
#yr3=1709
#vg<<

new_files=true
if [[ ! ${yr3} -gt ${yr2} ]]; then
   print -- "--- No new files"
   print -- "-----------------------"
   new_files=false
fi

(( yr2=yr2+1 ))

if [[ $new_files = "true" ]]; then

  print -- "--- Getting years $yr2 to $yr3 from $remote:$datadir"

  [[ -d outdata ]] || mkdir outdata
  cd outdata

  if [[ ${hamocc} == true ]]; then
    for y in `seq -f %04g $yr2 $((interval_months/12)) $yr3`
    do
      ((y2=y+interval_months/12-1))
      print -- "  --> Copy (remote) hamocc files of year $y to $y2 <-- "
      # Get HAMOCC output file
      ${CP}outdata/hamocc/${EXP}_hamocc_co2_${y}0101_${y2}1231.nc ${EXP}_hamocc_${y}.nc 2>/dev/null
      cdo -Q -s monmean -selcode,203,301,305,306,307,308,309,310,311,312,313,314 ${EXP}_hamocc_${y}.nc z$$.nc
      cdo -Q -s cat z$$.nc hamocc.nc && rm z$$.nc
      [[ ${rmsc} == "yes" ]] && rm ${EXP}_hamocc_${y}.nc
    done
  fi

  for y in `seq -f %04g $yr2 $yr3`
  do
    print -- "  --> Copy (remote) echam and jsbach files of year $y <-- "

    if [[ ${echam6} = true ]]; then

      # get echam BOT_mm file

      if [[ ${file_period} = year ]]; then
        ${CP}outdata/echam6/${EXP}_echam6_BOT_mm_${y}.${fext} ${EXP}_BOT_${y}.${fext} 2>/dev/null
      else
        if [[ $remote != "" ]]; then
          ${CP}outdata/echam6/${EXP}_echam6_BOT_mm_${y}\?\?.grb . 2>/dev/null
        else
          ${CP}outdata/echam6/${EXP}_echam6_BOT_mm_${y}??.grb . 2>/dev/null
        fi
        [[ $(ls -l ${EXP}_echam6_BOT_mm_${y}??.grb | wc -l) -eq 12 ]] || exit 1
        cat ${EXP}_echam6_BOT_mm_${y}??.grb > ${EXP}_BOT_${y}.grb && rm ${EXP}_echam6_BOT_mm_${y}??.grb
      fi
      cdo -s selcode,54,55,97,103,140,141,142,143,144,146,147,150,164,167,169,171,176,177,178,179,182,187,188,203,204,205,210,211,230,231 ${EXP}_BOT_${y}.${fext} z$$ 
      cat z$$ >> BOT_MON ; rm z$$
      [[ ${rmsc} == "yes" ]] && rm ${EXP}_BOT_${y}.${fext}

      # get echam co2_mm file
      if [[ ${file_period} = year ]]; then
        ${CP}outdata/echam6/${EXP}_echam6_co2_mm_${y}.${fext} ${EXP}_co2_${y}.${fext} 2>/dev/null
      else
        if [[ $remote != "" ]]; then
          ${CP}outdata/echam6/${EXP}_echam6_co2_mm_${y}\?\?.grb . 2>/dev/null
        else
          ${CP}outdata/echam6/${EXP}_echam6_co2_mm_${y}??.grb . 2>/dev/null
        fi
        [[ $(ls -l ${EXP}_echam6_co2_mm_${y}??.grb | wc -l) -eq 12 ]] || exit 1
        cat ${EXP}_echam6_co2_mm_${y}??.grb > ${EXP}_co2_${y}.grb && rm ${EXP}_echam6_co2_mm_${y}??.grb
      fi
      cdo -s selcode,5,6,8,11,20,24,25 ${EXP}_co2_${y}.${fext} z$$
      cat z$$ >> CO2_MON
      [[ ${rmsc} == "yes" ]] && rm  ${EXP}_co2_${y}.${fext}

      # get jsbach accw mm file
      if [[ ${echam6} == true ]]; then
      if [[ ${file_period} = year ]]; then
        ${CP}outdata/${accw_dir}/${EXP}_${accw_dir}_${accw_tag}_${y}.${fext1} . 2>/dev/null
      else
        if [[ $remote != "" ]]; then
          ${CP}outdata/${accw_dir}/${EXP}_${accw_dir}_${accw_tag}_${y}\?\?.grb . 2>/dev/null
        else
          ${CP}outdata/${accw_dir}/${EXP}_${accw_dir}_${accw_tag}_${y}??.grb . 2>/dev/null
        fi
        [[ $(ls -l ${EXP}_${accw_dir}_${accw_tag}_${y}??.grb | wc -l) -eq 12 ]] || exit 1
        cat ${EXP}_${accw_dir}_${accw_tag}_${y}??.grb > ${EXP}_${accw_dir}_${accw_tag}_${y}.${fext1}
        rm -f ${EXP}_${accw_dir}_${accw_tag}_${y}??.grb
      fi
      cdo -s selcode,160,161,218 ${EXP}_${accw_dir}_${accw_tag}_${y}.${fext1} z$$
      cat z$$ >> accw.grb && rm z$$
      [[ ${rmsc} == "yes" ]] && rm ${EXP}_${accw_dir}_${accw_tag}_${y}.${fext1}
      fi
    else   # jsbach stand alone simulation

      # get jsbach forcing mm file

      if [[ ${cmor} = "yes" ]]; then
        if [[ ${file_period} = year ]]; then
          ${CP}outdata/jsbach/${EXP}_jsbach_forcing_${y}.${fext1} . 2>/dev/null
        else
          ${CP}outdata/jsbach/${EXP}_jsbach_forcing_${y}\?\?.grb  . 2>/dev/null
          cdo cat ${EXP}_jsbach_forcing_${y}??.grb ${EXP}_jsbach_forcing_${y}.${fext1}
          rm -f ${EXP}_jsbach_forcing_${y}??.grb
        fi
        cdo -s monmean -selcode,1,2,3,7 ${EXP}_jsbach_forcing_${y}.${fext1} z$$
        cat z$$ >> forcing.${fext1} && rm z$$
        [[ ${rmsc} == "yes" ]] && rm ${EXP}_jsbach_forcing_${y}.${fext1}
      fi

      # get jsbach driving mm file

      if [[ ${cmor} = "yes" && ${ls3mip} = "yes" ]]; then
        if [[ ${file_period} = year ]]; then
          ${CP}outdata/jsbach/${EXP}_jsbach_driving_mm_${y}.${fext1} . 2>/dev/null
        else
          ${CP}outdata/jsbach/${EXP}_jsbach_driving_mm_${y}\?\?.grb  . 2>/dev/null
          cdo cat ${EXP}_jsbach_driving_mm_${y}??.grb ${EXP}_jsbach_driving_mm_${y}.${fext1}
          rm -f ${EXP}_jsbach_driving_mm_${y}??.grb
        fi
        cdo -s selcode,2 ${EXP}_jsbach_driving_mm_${y}.${fext1} z$$
        cat z$$ >> driving.${fext1} && rm z$$
        [[ ${rmsc} == "yes" ]] && rm ${EXP}_jsbach_driving_mm_${y}.${fext1}
      fi
    fi

    if [[ ${specialWish} = "dailyLAI" ]]; then
        #only get jsbach main veg and land file        
        if [[ ${dynveg} = "yes" || ${cmor} = "yes" ]]; then
          if [[ ${file_period} = year ]]; then
            ${CP}outdata/jsbach/${EXP}_jsbach_jsbach_${y}.${fext1} . 2>/dev/null
            ${CP}outdata/jsbach/${EXP}_jsbach_veg_${y}.${fext1} . 2>/dev/null
            ${CP}outdata/jsbach/${EXP}_jsbach_land_${y}.${fext1} . 2>/dev/null
          else
            ${CP}outdata/jsbach/${EXP}_jsbach_jsbach_${y}\?\?.grb  . 2>/dev/null
            cdo cat ${EXP}_jsbach_jsbach_${y}??.grb ${EXP}_jsbach_jsbach_${y}.${fext1}
            rm -f ${EXP}_jsbach_jsbach_${y}??.grb
          fi
          cdo -s selcode,107,162,163 ${EXP}_jsbach_jsbach_${y}.${fext1} zmain$$
          cdo -s selcode,56,143,170,173,178,211 ${EXP}_jsbach_veg_${y}.${fext1} zveg$$
          cdo -s selcode,35,44,47,49 ${EXP}_jsbach_land_${y}.${fext1} zland$$
          cat zmain$$ >> main_daily.${fext1} && rm zmain$$
          cat zveg$$ >> veg_daily.${fext1} && rm zveg$$
          cat zland$$ >> land_daily.${fext1} && rm zland$$
          [[ ${rmsc} == "yes" ]] && rm ${EXP}_jsbach_jsbach_${y}.${fext1} ${EXP}_jsbach_veg_${y}.${fext1} ${EXP}_jsbach_land_${y}.${fext1}
        fi
    fi

    if [[ ${yasso} = yes ]]; then

      # get jsbach yasso mm file

      if [[ ${file_period} = year ]]; then
        ${CP}outdata/jsbach/${EXP}_jsbach_yasso_mm_${y}.${fext1} ${EXP}_yasso_${y}.${fext1} 2>/dev/null
      else
        if [[ $remote != "" ]]; then
          ${CP}outdata/jsbach/${EXP}_jsbach_yasso_mm_${y}\?\?.grb . 2>/dev/null
        else
          ${CP}outdata/jsbach/${EXP}_jsbach_yasso_mm_${y}??.grb . 2>/dev/null
        fi
        [[ $(ls -l ${EXP}_jsbach_yasso_mm_${y}??.grb | wc -l) -eq 12 ]] || exit 1
        cat ${EXP}_jsbach_yasso_mm_${y}??.grb > ${EXP}_yasso_${y}.grb && rm ${EXP}_jsbach_yasso_mm_${y}??.grb
      fi
      cdo -s selcode,31,32,33,34,35,36,37,38,39,41,42,43,44,45,46,47,48,49 ${EXP}_yasso_${y}.${fext1} z$$
      cat z$$ >> yasso.grb
      rm z$$
      [[ ${rmsc} == "yes" ]] && rm ${EXP}_yasso_${y}.${fext1}
    fi

    if [[ ${nitrogen} = yes ]]; then

      # get jsbach nitro mm file

      if [[ ${file_period} = year ]]; then
        ${CP}outdata/jsbach/${EXP}_jsbach_nitro_mm_${y}.${fext1} ${EXP}_nitro_${y}.${fext1} 2>/dev/null
      else
        if [[ $remote != "" ]]; then
          ${CP}outdata/jsbach/${EXP}_jsbach_nitro_mm_${y}\?\?.grb . 2>/dev/null
        else
          ${CP}outdata/jsbach/${EXP}_jsbach_nitro_mm_${y}??.grb . 2>/dev/null
        fi
        [[ $(ls -l ${EXP}_jsbach_nitro_mm_${y}??.grb | wc -l) -eq 12 ]] || exit 1
        cat ${EXP}_jsbach_nitro_mm_${y}??.grb > ${EXP}_nitro_${y}.grb && rm ${EXP}_jsbach_nitro_mm_${y}??.grb
      fi
      cdo -s selcode,70,71,72,73,74,77,80,81,129,130,158,212,222,236,240,241,242,243,244,245,246,247,248,250 ${EXP}_nitro_${y}.${fext1} z$$
      cat z$$ >> nitro.grb
      rm z$$
      [[ ${rmsc} == "yes" ]] && rm ${EXP}_nitro_${y}.${fext1}
    fi

    # get jsbach main mm file

    if [[ ${dynveg} = "yes" || ${cmor} = "yes" ]]; then
      if [[ ${file_period} = year ]]; then
        ${CP}outdata/jsbach/${EXP}_jsbach_jsbach_mm_${y}.${fext1} . 2>/dev/null
      else
        ${CP}outdata/jsbach/${EXP}_jsbach_jsbach_mm_${y}\?\?.grb  . 2>/dev/null
        cdo cat ${EXP}_jsbach_jsbach_mm_${y}??.grb ${EXP}_jsbach_jsbach_mm_${y}.${fext1}
        rm -f ${EXP}_jsbach_jsbach_mm_${y}??.grb
      fi
      cdo -s selcode,10,12,20,21,22,24,84,107,162,163 ${EXP}_jsbach_jsbach_mm_${y}.${fext1} z$$
      cat z$$ >> main.${fext1} && rm z$$
      [[ ${rmsc} == "yes" ]] && rm ${EXP}_jsbach_jsbach_mm_${y}.${fext1}
    fi

    # get jsbach surf mm file

    if [[ ${cmor} = "yes" && ${ls3mip} = "yes" ]]; then
      if [[ ${file_period} = year ]]; then
        ${CP}outdata/jsbach/${EXP}_jsbach_surf_mm_${y}.${fext1} . 2>/dev/null
      else
        ${CP}outdata/jsbach/${EXP}_jsbach_surf_mm_${y}\?\?.grb  . 2>/dev/null
        cdo cat ${EXP}_jsbach_surf_mm_${y}??.grb ${EXP}_jsbach_surf_mm_${y}.${fext1}
        rm -f ${EXP}_jsbach_surf_mm_${y}??.grb
      fi
      cdo -s selcode,56,203,204 ${EXP}_jsbach_surf_mm_${y}.${fext1} z$$
      cat z$$ >> surf.${fext1} && rm z$$
      [[ ${rmsc} == "yes" ]] && rm ${EXP}_jsbach_surf_mm_${y}.${fext1}
    fi

    # get jsbach land mm file

    if [[ ${cmor} = "yes" ]]; then
      if [[ ${file_period} = year ]]; then
        ${CP}outdata/jsbach/${EXP}_jsbach_land_mm_${y}.${fext1} . 2>/dev/null
      else
        ${CP}outdata/jsbach/${EXP}_jsbach_land_mm_${y}\?\?.grb  . 2>/dev/null
        cdo cat ${EXP}_jsbach_land_mm_${y}??.grb ${EXP}_jsbach_land_mm_${y}.${fext1}
        rm -f ${EXP}_jsbach_land_mm_${y}??.grb
      fi
      cdo -s selcode,36,44,47,49,60,63,68,76,79,80,107,109,148,149 ${EXP}_jsbach_land_mm_${y}.${fext1} z$$
      cat z$$ >> land.${fext1} && rm z$$
      [[ ${rmsc} == "yes" ]] && rm ${EXP}_jsbach_land_mm_${y}.${fext1}
    fi

    # get jsbach veg mm file

    if [[ ${file_period} = year ]]; then
      ${CP}outdata/jsbach/${EXP}_jsbach_veg_mm_${y}.${fext1} ${EXP}_veg_${y}.${fext1} 2>/dev/null
    else
      if [[ $remote != "" ]]; then
        ${CP}outdata/jsbach/${EXP}_jsbach_veg_mm_${y}\?\?.grb . 2>/dev/null
      else
        ${CP}outdata/jsbach/${EXP}_jsbach_veg_mm_${y}??.grb . 2>/dev/null
      fi
      [[ $(ls -l ${EXP}_jsbach_veg_mm_${y}??.grb | wc -l) -eq 12 ]] || exit 1
      cat ${EXP}_jsbach_veg_mm_${y}??.grb > ${EXP}_veg_${y}.grb && rm ${EXP}_jsbach_veg_mm_${y}??.grb
    fi

    clist="50,56,62,82,143,158,160,161,162,169,170,173,176,178,211,212,218,220,221,222,223,224,225,228,230,231,236,238,239,242,243,246,247,249,251"
    [[ ${nitrogen} == yes ]] && clist="${clist},62,212"
    cdo -s selcode,${clist} ${EXP}_veg_${y}.${fext1} z$$
    cat z$$ >> veg.grb
    rm z$$
    [[ ${rmsc} == "yes" ]] && rm ${EXP}_veg_${y}.${fext1}

  done

  # decompress files
  if [[ ${fext} = "sz" && ${echam6} = true ]]; then
    cdo -f grb copy CO2_MON CO2$$  &&  mv CO2$$ CO2_MON
    cdo -f grb copy BOT_MON BOT$$  &&  mv BOT$$ BOT_MON
  fi
  if [[ ${fext1} = "sz" && ${jsbach} = true ]]; then
    grib -sunzip veg.sz z$$ && mv z$$ veg.grb
    grib -sunzip main.sz z$$ && ( mv z$$ main.grb; rm main.sz )
    grib -sunzip land.sz z$$ && ( mv z$$ land.grb; rm land.sz )
  fi

  cd ..

fi # new_files

if [[ $new_files = true || $rebuild_plots == yes ]]; then

  if [[ ( ${dynveg} = yes || ${cmor} = yes ) && ! -f outdata/jsbach.nc ]]; then
    ${CP_RAW}${jsbach_init_file} outdata/jsbach.nc 2>/dev/null
  fi
  if [[ ${echam_fractional} = yes ]]; then
    ${CP_RAW}${echam_init_file} outdata/jan_surf.nc 2>/dev/null
    optional_slf="outdata/slf.nc"
    cdo -selvar,SLF outdata/jan_surf.nc ${optional_slf}
  else
    optional_slf=""
  fi

  if [[ ${echam6} = true && ${hamocc} = true ]]; then
    echo " -- run co2inventory.sh --"
    if [[ ${echam_fractional} =  yes ]]; then
      cdo mul -selcode,176 outdata/veg.grb ${optional_slf} outdata/veg176.grb
    else
      cdo selcode,176 outdata/veg.grb outdata/veg176.grb
    fi
    $BIN_DIR/co2inventory.sh outdata/hamocc.nc outdata/CO2_MON outdata/veg176.grb
  fi

  echo " -- run carbon_land.sh --"
  #$BIN_DIR/carbon_land.sh outdata/veg.grb outdata/yasso.grb ${yasso} ${optional_slf}

  if [[ ${nitrogen} == yes ]]; then
    echo " -- run nitrogen_land.sh --"
    $BIN_DIR/nitrogen_land.sh outdata/nitro.grb ${optional_slf}
  fi

  if [[ ${dynveg} = "yes" ]]; then
    echo " -- run forestfrac.sh --"
    $BIN_DIR/forestfract.sh outdata/main.grb outdata/jsbach.nc ${optional_slf}
  fi

  if [[ ${cmor} = "yes" ]]; then
    echo " -- run c6_cmor_land.sh --"
    $BIN_DIR/c6_cmor_land.sh ${EXP} outdata/jsbach.nc outdata/veg.grb outdata/main.grb \
                   outdata/land.grb outdata/yasso.grb outdata/nitro.grb outdata/CO2_MON \
                   outdata/BOT_MON outdata/accw.grb outdata/surf.grb outdata/forcing.grb \
                   outdata/driving.grb ${dynveg} ${nitrogen} ${ls3mip} ${specialWish} \
                   ${output_tag} ${optional_slf}
  fi

  print -- "--- Plotting"
  #--------------------------
  #   echam time series
  #--------------------------
  if [[ ${echam6} = true && ${echam_ts} = yes ]]; then
    $BIN_DIR/plot_lines_co2.sh $EXP $RES
    $BIN_DIR/plot_enso.sh $EXP
    mv $BIN_DIR/*.ps ${plotdir}
  fi

  #--------------------------
  #   carbon inventory
  #--------------------------
  if [[ ${echam6} = true && ${hamocc} = true ]]; then
    $BIN_DIR/plot_carbon_inventory ${output_tag} $yr1
    mv invent*.pdf ${plotdir}
  fi

  #--------------------------
  #   land carbon pools
  #--------------------------
  $BIN_DIR/plot_land_carbon_pools ${output_tag} $yr1
  mv cland*.pdf ${plotdir}

  #--------------------------
  #   land nitrogen pools
  #--------------------------
  $BIN_DIR/plot_land_nitrogen_pools ${output_tag} $yr1
  mv nland*.pdf ${plotdir}

  #--------------------------
  #   dynveg plot
  #--------------------------
  if [[ ${dynveg} = "yes" ]]; then
    $BIN_DIR/plot_dynveg ${output_tag} $yr1
    mv dynveg*.pdf ${plotdir}
  fi

  rm -f *.asc *.ext CO2* codes* anom* *.gs code103* grads.eps meta

fi # new_files or rebuild_plots

print -- "--- Finished"

exit
