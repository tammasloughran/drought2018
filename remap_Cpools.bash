#!/bin/bash
ncatted -a axis,lon,c,c,"X" cpools_vga0218_18991231_T63.nc cpools_vga0218_18991231_T63_new.nc
ncatted -a axis,lat,c,c,"Y" cpools_vga0218_18991231_T63_new.nc
ncatted -a units,lat,c,c,"degrees_N" cpools_vga0218_18991231_T63_new.nc
ncatted -a units,lon,c,c,"degrees_E" cpools_vga0218_18991231_T63_new.nc
cdo remapbil,T255grid cpools_vga0218_18991231_T63_new.nc cpools_vga0218_18991231_T255.nc
