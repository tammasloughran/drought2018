#!/bin/ksh
#------------------------------------------------------------------------------
# Set date variables in jsbach or echam restart files
#   fdate: initial date of the experiment - 2 years prior to vdate)
#   ftime: initial time - last time step before midnight
#   vdate: restart date
#   vtime: restart time - last time step before midnight
#   nstep: number of model time steps between initial date and restart date
#
# Veronika, 10.12.2013
#------------------------------------------------------------------------------

expid=spinup_T255_ERA5
old_restart_date=41081231
new_restart_date=18591231    # day befor startdate of the run [YearMMDD]

dt=1800                       # model time step [s]
                             #   T31: dt=1800
                             #   T63: dt=450
                             #   jsbach stand-alone: dt=1800

restartdir=/work/bm0891/m300719/drought2018/mpiesm-landveg/experiments/post_spinup_T255_ERA5/restart/jsbach

stream_list="driving forcing jsbach nitro veg yasso" # in jsbach dir.
#stream_list="echam co2 tracer accw"                                       # in echam dir.

#------------------------------------------------------------------------------
set -e

scriptdir=$(dirname $0)        # script expected in contrib directory
fpath=${scriptdir}/../util/running/functions
export PATH=$fpath:${PATH}

whence ncatted || module load nco

vdate=${new_restart_date}                                 # new restart date
startdate=$(calc_date plus -D 1 -- ${new_restart_date})   # start date of the run

# initialization date (fdate) is two years prior to the restart date, 
#    one time step before midnight
fdatetime=$(calc_date minus -Y 2 -s ${dt} -- ${startdate}_000000})
fdate=${fdatetime%%_*}       # remove everything to the right of '_'  
ftime=${fdatetime##*_}       # remove everything to the left of '_'
vtime=${ftime}

# time steps between initialization (fdate) and restart date (vdate)
nseconds=$(time_between ${fdatetime} ${vdate}_${vtime} seconds)
(( nstep = nseconds / dt - 1 ))

for stream in ${stream_list}; do
  new_restart_file=restart_${expid}_${stream}_${old_restart_date}_${new_restart_date}.nc
  cp ${restartdir}/restart_${expid}_${stream}_${old_restart_date}.nc ${new_restart_file}
  ncatted -O -a fdate,global,m,l,${fdate} ${new_restart_file}
  ncatted -O -a ftime,global,m,l,${ftime} ${new_restart_file}
  ncatted -O -a vdate,global,m,l,${vdate} ${new_restart_file}
  ncatted -O -a vtime,global,m,l,${vtime} ${new_restart_file}
  ncatted -O -a nstep,global,m,l,${nstep} ${new_restart_file}
  mv ${new_restart_file} ${restartdir}
done
