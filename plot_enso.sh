#!/bin/ksh
#
# Author
# Monika Esch, MPI, 2007
# Changes:
#   Veronika Gayler, June 2009
#     - usage of 'cdo gradsdes' instead of locally installed gradsdes.
#       This entails usage of variable names instead of code numbers in
#       the grads scripts
#
#
set -e
cdo=cdo
[[ -f ${cdo} ]] || cdo=cdo
#
EXP=$1
bot=outdata/BOT_MON
#
years=`${cdo} showyear $bot`
years=${years##+([ ])}
yrs=${years%% *}
yre=${years##* }
#yrs=0800 ;# start year of file
#yre=0886 ;# end year of file
#
#
#mv codes_${EXP} codes_${EXP}in
#${cdo} setday,15 codes_${EXP}in codes_${EXP}
${cdo} -selcode,103 codes_${EXP}m code103_${EXP}
${cdo} ymonsub code103_${EXP} -ymonavg code103_${EXP} anom_${EXP}
#
${cdo} gradsdes anom_${EXP}

#
#######
exp=${EXP}
nyear1=`expr ${yre} - ${yrs} + 1 `
nmonth1=`expr ${nyear1} \* 12 `

cat >nino.gs<<EOF
'reinit'
'open anom_${exp}.ctl'

'set dfile 1'
'set t 1 ${nmonth1}'

'define nino3=aave(var103,lon=210,lon=270,lat=-5,lat=5)'
'define nino34=aave(var103,lon=190,lon=240,lat=-5,lat=5)'

'set lon 1 1'
'set lat 1 1'

'enable print meta'

******************U*********************

init2a()

'set grid on 0 1'
'set vrange -5.0 5.0'
'set ylint 1.'
'set ylab %.2f'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd nino3'
'set line 14 1 10'
'draw line 0.75 3.3 7.75 3.3'
'draw line 0.75 1.7 7.75 1.7'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Nino3 Sea Surface Temperature Index (departure from average) '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.07 0.1'
'set string 2 l 6'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange -5.0 5.0'
'set ylint 1.'
'set ylab %.2f'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd nino34'
'set line 14 1 10'
'draw line 0.75 3.3 7.75 3.3'
'draw line 0.75 1.7 7.75 1.7'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Nino3.4 Sea Surface Temperature Index (departure from average) '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.07 0.1'
'set string 2 l 6'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_nino_${exp}_1.ps'

******************U*********************
'!rm meta'
'quit'
*--------------------------------------------------
function init2a ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 Global and Annual Means'


*----rahmen setzen----* 
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'

return

*--------------------------------------------------
function init2a2 ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 Sea Ice Area [10\`a12\`n m\`a2\`n]'
*----rahmen setzen----*
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'
return
*--------------------------------------------------
function init2a3 ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 Sea Ice Volume [10\`a12\`n m\`a3\`n]'

*----rahmen setzen----*
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'
return

*--------------------------------------------------
function init2b ()
*--------------------------------------------------

*----rahmen setzen----* 
'set vpage 0. 8.5 0. 5.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'

return 
EOF

grads -cbp nino.gs
#
# NINO3
#
${cdo} sellonlatbox,-150,-90,-5,5 anom_${EXP} anom_${EXP}_sel
${cdo} timstd anom_${EXP}_sel anom_${EXP}_selt
${cdo} info anom_${EXP}_selt
#
# NINO3.4
#
${cdo} sellonlatbox,-170,-120,-5,5 anom_${EXP} anom_${EXP}_sel
${cdo} timstd anom_${EXP}_sel anom_${EXP}_selt
${cdo} info anom_${EXP}_selt
#

