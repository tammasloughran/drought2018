#!/bin/bash
###############################################################################
### Batch Queuing System is SLURM
#SBATCH --job-name=rename.bash
#SBATCH --output=rename.bash.o%j
#SBATCH --error=rename.bash.o%j
#SBATCH --partition=prepost
#SBATCH --mem-per-cpu=5120
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=20
#SBATCH --account bm0891
#SBATCH --mail-type=FAIL       # Notify user by email in case of job failure
#SBATCH --mail-user=t.loughran@lmu.de # Set your e-mail address
module load nco
module load cdo

expname=S2.1
indir=/work/bm0891/m300719/drought2018/output/postprocessed/${expname}/outdata_land
outdir=/work/bm0891/m300719/drought2018/output/postprocessed/${expname}/outdata_land/renamed
if [ ! -d $outdir ]; then
    mkdir $outdir
fi

dailypftList="dlaipft dgpppft"
for var in $dailypftList; do
    {
    var2=${var:1}
    var2=${var2/pft/_pft}
    cdo sellonlatbox,-11,65,32,75 ${indir}/JSBACH_${expname}_${var}.nc tmp${var}
    ncrename -v $var,$var2 tmp${var} ${outdir}/JSBACH_${expname}_${var2}.nc
    } &
done
wait
dailyList="dgpp dra drh dfFire dnbp dlai devapotrans dhfss dhfls dskinT"
for var in $dailyList; do
    {
    cdo sellonlatbox,-11,65,32,75 ${indir}/JSBACH_${expname}_${var}.nc tmp${var}
    ncrename -v $var,${var:1} tmp${var} ${outdir}/JSBACH_${expname}_${var:1}.nc
    } &
done
wait
cmorList="albs burntArea cLitter cSoil cVeg evapotrans fapar fBNF fFire fGrazing fHarvest fLuc fN2O fNdep fNleach fNloss fNnetmin fNup gpp hfls landCoverFrac mrro mrso msl nLitter npp nProduct nSoil nVeg pr ra rh rsds sfls tas tran tsl"
for var in $cmorList; do
    {
    cdo sellonlatbox,-11,65,32,75 ${indir}/JSBACH_${expname}_${var}.nc tmp${var}
    mv tmp${var} ${outdir}/JSBACH_${expname}_${var}_monthly.nc
    } &
done
wait
cmorpftList="cLitterpft cSoilpft cVegpft gpppft laipft npppft"
for var in $cmorpftList; do
    {
    var2=${var/pft/_pft}
    cdo sellonlatbox,-11,65,32,75 ${indir}/JSBACH_${expname}_${var}.nc tmp${var}
    ncrename -v $var,$var2 tmp${var} ${outdir}/JSBACH_${expname}_${var2}_monthly.nc
    } &
done
wait
rm tmp*
