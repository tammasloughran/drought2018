#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 15:57:28 2019

@author: tammas

Compare carbon pools from two periods (cycles) 
"""


import netCDF4 as nc
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
from cdo import Cdo
cdo = Cdo()

mollMassC = 12.0111
mollMassN = 14.006747
g2Pg = 1e15

# Model output
outputDir = '/scratch/m/m300719/drought2018/mpiesm-landveg/experiments/spinup_T255_ERA5/outdata/jsbach/'
# You should create a file containg the cell area of the grid
areaFile = 'gridarea.nc'
cfFile = 'spinup_T255_ERA5_jsbach_jsbach_3699.nc'

# Load the areas
ncin2 = nc.Dataset(outputDir+areaFile,'r')
cell_area = ncin2.variables['cell_area'][...] # (lat, lon)
cell_area = cell_area[None,...]

# List of files for cycle1 and cycle2
cycle_comp = False
if cycle_comp:
    filesCycle1 = glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_320?.nc') \
        + glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_321?.nc')
    filesCycle2 = glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_322?.nc') \
        + glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_323?.nc')

    ncin1 = nc.MFDataset(filesCycle1)
    ncin2 = nc.MFDataset(filesCycle2)

    poolVars = ['boxC_green',
            'boxC_woods',
            'boxC_reserve',
            'box_Cpools_total',
            'boxC_crop_harvest']
    for var in poolVars:
        varCycle1 = ncin1.variables[var][0:-1:12,...].mean(axis=0)*cell_area
        varCycle2 = ncin2.variables[var][0:-1:12,...].mean(axis=0)*cell_area
        change = varCycle2.sum() - varCycle1.sum()
        # 12.0111 = molar mass of carbon. g2Pg = grams->petagrams
        print(var+' change: '+str(change*mollMassC/g2Pg) + ' petagrams')
    
allFiles = glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_????*.nc')
allFiles.sort()
gridpoints = False
if gridpoints:
    series1 = []
    series2 = []
    series3 = []
    series4 = []
    time = []
    var = 'boxC_woods'
    for file in allFiles[:-1]:
        print(file[-10:])
        ncin = nc.Dataset(file)
        boxC_woods = ncin.variables[var][0,...]*cell_area
        series = boxC_woods.sum(axis=0)
        series1 = np.append(series1,series[69,70]*mollMassC/g2Pg)
        series2 = np.append(series2,series[63,103]*mollMassC/g2Pg)
        series3 = np.append(series3,series[86,41]*mollMassC/g2Pg)
        series4 = np.append(series4,series[94,4]*mollMassC/g2Pg)
        date = ncin.variables['time'][0]
        time = np.append(time,nc.num2date(date, ncin.variables['time'].units))
    plt.scatter(time,series1,marker='.')
    plt.scatter(time,series2,marker='o')
    plt.scatter(time,series3,marker='x')
    plt.scatter(time,series4,marker='+')
    plt.title(var)
    plt.ylabel('Petagrams C')
    plt.show()
    
# Plot the raw carbon density
density_plot = True
if density_plot:
    nclast = nc.Dataset(allFiles[-1])
    lons = nclast.variables['lon'][:]
    lats = nclast.variables['lat'][:]
    xx,yy = np.meshgrid(lons,lats)
    density = nclast.variables['boxC_green'][-1,...] + nclast.variables['boxC_reserve'][-1,...] + nclast.variables['boxC_woods'][-1,...] + nclast.variables['boxC_crop_harvest'][-1,...]
    proj = Basemap(llcrnrlon=-10,llcrnrlat=31.5,urcrnrlon=65.5,urcrnrlat=75, projection='mill',resolution='l')
    proj.pcolormesh(xx,yy,density.sum(axis=0)*mollMassC/1000,latlon=True)
    proj.colorbar()
    proj.drawcoastlines()
    plt.title('Biomass carbon density @ end of run (kg(C) m-2(grid box))')
    plt.savefig('Cdensity.png')

series1 = []
series2 = []
time = []
var1 = 'boxC_woods'
var2 = 'box_Cpools_total'
for file in allFiles[:-1]:
    print(file[-10:])
    ncin = nc.Dataset(file)
    data = ncin.variables[var1][0,...]*cell_area
    series1 = np.append(series1,data.sum()*mollMassC/g2Pg)
    data = ncin.variables[var2][0,...]*cell_area
    series2 = np.append(series2,data.sum()*mollMassC/g2Pg)
    date = ncin.variables['time'][0]
    time = np.append(time,nc.num2date(date, ncin.variables['time'].units))
plt.scatter(time,series1)
plt.title(var1)
plt.ylabel('Petagrams C')
plt.savefig(var1+'.png')
plt.figure()
plt.scatter(time,series2)
plt.title(var2)
plt.ylabel('Petagrams C')
plt.savefig(var2+'.png')

saveit = True
if saveit:
    np.save(var1, series1)
    np.save(var2, series2)

allFiles = glob.glob(outputDir+'spinup_T255_ERA5_jsbach_nitro_????*.nc')
allFiles.sort()
plt.figure()
series1 = []
time = []
var1 = 'box_Npools_total'
for file in allFiles[:-1]:
    print(file[-10:])
    ncin = nc.Dataset(file)
    data = ncin.variables[var1][0,...]*cell_area
    series1 = np.append(series1,data.sum()*mollMassN/g2Pg)
    date = ncin.variables['time'][0]
    time = np.append(time,nc.num2date(date, ncin.variables['time'].units))
plt.scatter(time,series1)
plt.title(var1)
plt.ylabel('Petagrams N')
plt.savefig(var1+'.png')

saveit = True
if saveit:
    np.save(var1, series1)

allFiles = glob.glob(outputDir+'spinup_T255_ERA5_jsbach_yasso_????*.nc')
allFiles.sort()
plt.figure()
series1 = []
series2 = []
time = []
var1 = 'boxYC_humus_2'
var2 = 'boxYC_humus_1'
for file in allFiles[:-1]:
    print(file[-10:])
    ncin = nc.Dataset(file)
    data1 = ncin.variables[var1][0,...]*cell_area
    data2 = ncin.variables[var2][0,...]*cell_area
    series1 = np.append(series1,data1.sum()*mollMassC/g2Pg)
    series2 = np.append(series2,data2.sum()*mollMassC/g2Pg)
    date = ncin.variables['time'][0]
    time = np.append(time,nc.num2date(date, ncin.variables['time'].units))
plt.scatter(time,series2)
plt.title(var1)
plt.ylabel('Petagrams C')
plt.savefig(var1+'.png')
plt.figure()
plt.scatter(time,series2)
plt.title(var2)
plt.ylabel('Petagrams C')
plt.savefig(var2+'.png')

saveit = True
if saveit:
    np.save(var1, series1)
    np.save(var2, series2)
