#!/bin/bash 
#
# Script to calculate jsbach forcing data from crujra data remapped to T63 
#
# The script is based on calculate_jsbach_forcing_from_remapped_cruncep_TRENDY_v6.bash (and v4), which were based on 
# preprocessing_site_level_forcing_data_JN_140924.tcsh created by Melinda Galfi and adapted by JN
# and preprocessing_change_time_unit_in_forcing_file.ksh by JN
#
# 1. change var names and calculate required variables 
# 2. merge all variables per year + make all years leap year + change to absolute time axis as required by jsbach
# 3. check variables to adhere to jsbach limits!
# 
#
# NOTE: in the original data shortwave is in J/m2, longwave in W/m2 --> convert also shortwave to J/m2
#
#------------------------------------------------------------------------------
# Julia E.M.S. Nabel, July 2018 (julia.nabel@mpimet.mpg.de)
#
#----------------------------------------------------------------------------
# submit as a serial job to mistral -> adapt wall_clock_limit to your needs!
#----------------------------------------------------------------------------
#
###############################################################################
### Batch Queuing System is SLURM
#SBATCH --job-name=calculate_jsbach_forcing_with_common_slm_from_ERA5_drought2018
#SBATCH --output=calculate_jsbach_forcing_with_common_slm_from_ERA5_drought2018.o%j
#SBATCH --error=calculate_jsbach_forcing_with_common_slm_from_ERA5_drought2018.o%j
#SBATCH --partition=prepost
#SBATCH --mem-per-cpu=5120 
#SBATCH --ntasks=1
#SBATCH --account bm0891
#SBATCH --mail-type=FAIL       # Notify user by email in case of job failure
#SBATCH --mail-user=t.loughran@lmu.de # Set your e-mail address

#============================================================================
#============================================================================
# 'User' definitions

# directory where the cruncep data is located
workingPath='/work/bm0891/m300719/drought2018/indata/ERA5'
inPath='/work/bm0891/m300719/drought2018/indata/ERA5'
#workingPath='/home/tammas/data/ERA5/europe'
#inPath='/home/tammas/data/ERA5/europe'

oldPrefix='era5_europe_S2.1_'
newPrefix='Climate_era5_europe_S2.1_T255_'

first_year=1989
last_year=1989

#---------------------------------------------- constants
newVarName=( longwave qair precip shortwave tmin tmax wspeed ) #wspeed needs to be last item in the list
newUnit=( 'W/m2' 'g/g' 'mm/day' 'W/m2' 'degC' 'degC' 'm/s' )

# old units: 'W/m2' 'kg/kg' 'm/hr' 'W/m2' 'Degrees Kelvin' 'Degrees Kelvin' 'm/s'
#    NOTE: in the original data shortwave is in J/m2, longwave in W/m2
varNameInFileName=( LWdown Qair Precip SWdown Tair Tair Wind ) 
varNameInFile=( LWdown Qair Precip SWdown Tair Tair Wind )

# list of all variables that should adhere to lower limits (given in the same order in limitsList)
limitVarList=( precip wspeed shortwave longwave qair )
limitsList=( 0.0 0.0 0.0 0.0 0.0 )

kelvin_to_c=273.15
secondsIn6h=21600

#fileWithCruncepSLM='/work/bm0891/m300719/drought2018/indata/jsbach_input/ERA5_JSBACH_common_mask_europe.nc'
fileWithCruncepSLM='/work/bm0891/m300719/drought2018/indata/masks/T255mask_global.nc'
#fileWithCruncepSLM='/home/tammas/work-mistral/drought2018/indata/T255mask_global.nc'

#ncap2='/sw/aix53/nco-4.0.3/bin/ncap2'

#============================================================================
#============================================================================
# 'Program'

module load nco
module load cdo

currentDate=$(date +'%m/%d/%Y')
currentYear=${first_year}
while [[ ${currentYear} -le ${last_year} ]]; do

    echo 'Currently processing year: '${currentYear}'...'

    # iterate over the required variables
    fileList=''
    varIndex=0
    while [[ ${varIndex} -lt ${#newVarName[@]} ]]; do
        
        thisNewVarName=${newVarName[varIndex]}
        echo $thisNewVarName
        thisNewUnit=${newUnit[varIndex]}

        thisOldVarFileName=${varNameInFileName[varIndex]}
        thisOldVarNameInFile=${varNameInFile[varIndex]}

        thisOrgFileName=${oldPrefix}${currentYear}.nc
        thisOrgFile=${inPath}/${thisOrgFileName}

        cdo -s -setunit,${thisNewUnit} -chname,${thisOldVarNameInFile},${thisNewVarName} -selvar,${thisOldVarNameInFile} ${thisOrgFile} ${workingPath}/tmp_${thisNewVarName}.nc

        if [[ "${thisNewVarName}" == 'precip' ]]; then
            cdo -s -daysum -mulc,1000 ${workingPath}/tmp_${thisNewVarName}.nc ${workingPath}/tmp2_${thisNewVarName}.nc
        elif [[ "${thisNewVarName}" == 'tmin' ]]; then
            cdo -s -daymin -subc,${kelvin_to_c} ${workingPath}/tmp_${thisNewVarName}.nc ${workingPath}/tmp2_${thisNewVarName}.nc
        elif [[ "${thisNewVarName}" == 'tmax' ]]; then
            cdo -s -daymax -subc,${kelvin_to_c} ${workingPath}/tmp_${thisNewVarName}.nc ${workingPath}/tmp2_${thisNewVarName}.nc
        elif [[ "${thisNewVarName}" == 'wspeed' ]]; then
            cdo -s -dayavg ${workingPath}/tmp_${thisNewVarName}.nc ${workingPath}/tmp2_${thisNewVarName}.nc
        else
            #shortwave longwave and qair 
            cdo -s -dayavg ${workingPath}/tmp_${thisNewVarName}.nc ${workingPath}/tmp2_${thisNewVarName}.nc
        fi

        echo "Remapping"
        # Remap to desired resolution
        cdo -s remapbil,T255grid ${workingPath}/tmp2_${thisNewVarName}.nc ${workingPath}/tmp_${thisNewVarName}.nc
        echo "Applying mask"
        #cut to common JSBACH slm
        cdo -s -ifthen -selvar,slm ${fileWithCruncepSLM} -selvar,${thisNewVarName} ${workingPath}/tmp_${thisNewVarName}.nc ${workingPath}/tmp2_${thisNewVarName}_${currentYear}.nc

        fileList="${fileList} ${workingPath}/tmp2_${thisNewVarName}_${currentYear}.nc"

        (( varIndex = varIndex + 1 ))
    done #while [[ ${varIndex} -lt ${#varInFileName[@]} ]]; do
    echo "Merging"
    # merge and change relative to absolute time axis and convert to cdo netcdf
    cdo -s -a -f nc -merge ${fileList} ${workingPath}/tmp_${newPrefix}${currentYear}.nc
    #rm -f ${fileList}

    #--- make each year a leap year 
    #get the 28 of February
    #cdo -s -seldate,${currentYear}-02-28 ${workingPath}/tmp_${newPrefix}${currentYear}.nc ${workingPath}/tmp2_${newPrefix}2802${currentYear}.nc
    #set it to 29
    #cdo -s -setdate,${currentYear}-02-29 ${workingPath}/tmp2_${newPrefix}2802${currentYear}.nc ${workingPath}/tmp2_${newPrefix}2902${currentYear}.nc

    #merge it 
    #cdo -s -mergetime ${workingPath}/tmp_${newPrefix}${currentYear}.nc ${workingPath}/tmp2_${newPrefix}2902${currentYear}.nc ${workingPath}/tmp2_${newPrefix}${currentYear}.nc
    #rm -f ${workingPath}/tmp_${newPrefix}${currentYear}.nc
    #rm -f ${workingPath}/tmp2_${newPrefix}2902${currentYear}.nc ${workingPath}/tmp2_${newPrefix}2802${currentYear}.nc
    # Move the files
    #mv ${workingPath}/tmp2_${newPrefix}${currentYear}.nc ${workingPath}/tmp_${newPrefix}${currentYear}.nc

    # set the required limits for the different variables
    varIndex=0
    while [[ ${varIndex} -lt ${#limitVarList[@]} ]]; do
        thisVar=${limitVarList[varIndex]}
        echo 'Currently setting the limit for : '${thisVar}'...'

        thisLimit=${limitsList[varIndex]}
        ncap2 -s "where(${thisVar}<${thisLimit}) ${thisVar}=${thisLimit};" ${workingPath}/tmp_${newPrefix}${currentYear}.nc ${workingPath}/tmp2_${newPrefix}${currentYear}.nc
        mv ${workingPath}/tmp2_${newPrefix}${currentYear}.nc ${workingPath}/${newPrefix}${currentYear}.nc
        (( varIndex = varIndex + 1 ))
    done #while [[ ${varIndex} -lt ${#varList[@]} ]]; do

    # remove file_name from global attributes
    #ncatted -O -a file_name,global,d,, ${workingPath}/${newPrefix}${currentYear}.nc

    # remove several of the global attributes
    ncatted -O -a conventions,global,d,, ${workingPath}/${newPrefix}${currentYear}.nc ${workingPath}/${newPrefix}${currentYear}.nc
    #ncatted -O -a id,global,d,, ${workingPath}/${newPrefix}${currentYear}.nc

    # add/replace some
    ncatted -O -a title,global,o,c,"ERA5 based offline forcing for JSBACH" ${workingPath}/${newPrefix}${currentYear}.nc
    ncatted -O -a institution,global,o,c,"Ludwig-Maximilians-Universitaet Muenchen" ${workingPath}/${newPrefix}${currentYear}.nc
    ncatted -O -a comment,global,o,c,"${currentDate}: Derived from ERA5 data (https://www.ecmwf.int/en/forecasts/datasets/reanalysis-datasets/era5) downloaded 13.02.19. Scripts: calculate_jsbach_forcing_with_common_slm_from_ERA5_drought2018.bash - t.loughran@lmu.de" ${workingPath}/${newPrefix}${currentYear}.nc -h
    ncatted -O -a contact,global,o,c,"t.loughran@lmu.de" ${workingPath}/${newPrefix}${currentYear}.nc
    ncatted -O -a history,global,d,, ${workingPath}/${newPrefix}${currentYear}.nc

    (( currentYear = currentYear + 1 ))
done #while [[ ${currentYear} -le ${last_year} ]]; do
rm -f ${workingPath}/tmp*.nc

