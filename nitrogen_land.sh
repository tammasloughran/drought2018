#!/bin/ksh
#
# Compute total nitrogen inventories of the different land nitrogen pools
#
# The input files are expected to be monthly averages
set -e

infile=$1       # jsbach nitro output stream:
                #   code 222  boxN_crop_harvest    [mol(N) m-2(grid box)]
                #   code 240  boxN_green           [mol(N) m-2(grid box)]
                #   code 241  boxN_woods           [mol(N) m-2(grid box)]
                #   code 242  boxN_mobile          [mol(N) m-2(grid box)]
                #   code 243  boxN_litter_green_ag [mol(N) m-2(grid box)]
                #   code 244  boxN_litter_green_bg [mol(N) m-2(grid box)]
                #   code 245  boxN_litter_wood_ag  [mol(N) m-2(grid box)]
                #   code 246  boxN_slow            [mol(N) m-2(grid box)]
                #   code 247  box_Npools_total     [mol(N) m-2(grid box)]
                #   code 248  boxN_litter_wood_bg  [mol(N) m-2(grid box)]
                #   code 250  boxN_sminN           [mol(N) m-2(grid box)]

if [[ $2 != "" ]]; then
  echam_fractional=yes
  olf=$2       # if echam uses fractional land sea mask: fractional ocean land mask
fi

# nitrogen in vegetation
[[ -f vegN.tmp ]] && rm vegN.tmp
cdo enssum -vertsum -selcode,240 ${infile} \
           -vertsum -selcode,241 ${infile} \
           -vertsum -selcode,242 ${infile}  vegN.tmp

# nitrogen in above ground litter (-> litter)
cdo    add -vertsum -selcode,243 ${infile} \
           -vertsum -selcode,245 ${infile}  litterN.tmp

# nitrogen in below ground litter (-> fast soil)
cdo    add -vertsum -selcode,244 ${infile} \
           -vertsum -selcode,248 ${infile}  soilNfast.tmp

# nitrogen in the slow soil
cdo         vertsum -selcode,246 ${infile}  soilNslow.tmp

# nitrogen in the soil
cdo    add  soilNfast.tmp soilNslow.tmp     soilN.tmp

# mineral nitrogen
cdo         vertsum -selcode,250 ${infile}  sminN.tmp

# total land nitrogen
cdo                  selcode,247 ${infile}  totalN.tmp

# Convert N pools from [mol(N)/m^2] to [Gt N]
# 1 mol N = 14.0068 g N   => 1 mol N = 1.40068e-14 Gt N
# Global surface area: 5.100656e14 m^2
# => 1 mol N / m^2 (area weighted global average) = 1.40068e-14 * 5.100656e14 Gt N = 7.144387 Gt N

varlist="vegN litterN soilN sminN soilNfast soilNslow totalN"
for f in ${varlist}
do
  if [[ ${echam_fractional} = yes ]]; then
    cdo fldavg -setmisstoc,0 -mul ${olf} $f.tmp z$$
  else
    cdo fldavg -setmisstoc,0 $f.tmp z$$
  fi
  cdo -f ext -b F64 mulc,7.144387 z$$ z$$.ext
  cdo setday,15 z$$.ext $f.ext
  rm $f.tmp z$$ z$$.ext
done

# write ascii output for R

for f in ${varlist}
do
  cdo output $f.ext > $f.asc
  rm $f.ext
done

