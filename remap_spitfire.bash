#!/bin/bash

module load cdo

cdo remapnn,T255grid a_nd_05x05.nc a_nd_T255.nc
cdo remapbil,T255grid population_density_HYDE_1x1.nc population_density_HYDE_T255.nc
cdo remapbil,T255grid LISOTD_HRMC_V2.2_05x05.nc LISOTD_HRMC_V2.2_T255.nc
