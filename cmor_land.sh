#!/bin/ksh
#------------------------------------------------------------------------------
# CMOR-like postprocessing of monthly jsbach output
#
#    the output files are not exactly cmor complient but have cmor variable 
#    names and units.
#------------------------------------------------------------------------------
# This Script only works for the jsbach CMIP5 setup with 11 tiles and 21 lcts:
#
#     tile 1:      PFT 1 (glacier) 
#              and PFT 2 (tropical broadleaf evergreen trees)
#     tile 2:      PFT 3 (tropical broadleaf deciduous trees)
#     tile 3:      PFT 4 (extra-tropical evergreen trees)
#     tile 4:      PFT 5 (extra-tropical deciduous trees)
#     tile 5:      PFT 10 (raingreen shrubs)
#     tile 6:      PFT 11 (deciduous shrubs)
#     tile 7:      PFT 12 (C3 grass)
#     tile 8:      PFT 13 (C4 grass)
#     tile 9:      PFT 15 (C3 pasture)
#     tile 10:     PFT 16 (C4 pasture)
#     tile 11:     PFT 20 (C3 crops)
#              and PFT 21(C4 crops)
#
# Veronika Gayler, April 2012
#------------------------------------------------------------------------------
set -e

expid=$1       # experiment id

inf_ini=$2     # jsbach initial file
               #   cover_type
               #   lsm

inf_veg=$3     # selected codes from jsbach veg stream (monthly means)
               #   code  53  box_burned_total     [m2/m2(grid box)]
               #      or:  code 31 (act_fpc) and code 32 (burned_fpc)     
               #   code 159  boxC_litter_wood     [mol(C)/m2(grid box)]
               #   code 160  boxC_green           [mol(C)/m2(grid box)]
               #   code 161  boxC_woods           [mol(C)/m2(grid box)]
               #   code 162  boxC_reserve         [mol(C)/m2(grid box)]
               #   code 163  boxC_litter_green_bg [mol(C)/m2(grid box)]
               #   code 164  boxC_slow            [mol(C)/m2(grid box)]
               #   code 170  box_soil_respiration [mol(C)/m2(grid box) s]
               #   code 171  box_NPP_yDayMean     [mol(C)/m2(grid box) s]
               #   code 173  box_GPP_yDayMean     [mol(C)/m2(grid box) s]
               #   code 175  box_litter_flux      [mol(C)/m2(grid box) s]
               #   code 176  box_Cpools_total     [mol(C)/m2(grid box)]
               #   code 178  box_NPP_act_yDayMean [mol(C)/m2(grid box) s]
               #   code 179  boxC_litter_green_ag [mol(C)/m2(grid box)]

inf_main=$4    # selected codes jsbach main stream (monthly means)
               #   code  10  cover_fract_pot      []
               #   code  12  cover_fract          []
               #   code  20  veg_ratio_max        []

inf_land=$5    # selected codes jsbach land stream (monthly means)
               #   code  68  soil_temperature     [K]
               #   code  76  transpiration        [kg/m2s]
               #   code 107  lai                  []

inf_yasso=$6   # selected codes from jsbach yasso stream
               #   code  31  boxYC_acid_ag1       [mol(C) m-2(grid box)]
               #   code  32  boxYC_acid_bg1       [mol(C) m-2(grid box)]
               #   code  33  boxYC_water_ag1      [mol(C) m-2(grid box)]
               #   code  34  boxYC_water_bg1      [mol(C) m-2(grid box)]
               #   code  35  boxYC_ethanol_ag1    [mol(C) m-2(grid box)]
               #   code  36  boxYC_ethanol_bg1    [mol(C) m-2(grid box)]
               #   code  37  boxYC_nonsoluble_ag1 [mol(C) m-2(grid box)]
               #   code  38  boxYC_nonsoluble_bg1 [mol(C) m-2(grid box)]
               #   code  39  boxYC_humus_1        [mol(C) m-2(grid box)]
               #   code  41  boxYC_acid_ag2       [mol(C) m-2(grid box)]
               #   code  42  boxYC_acid_bg2       [mol(C) m-2(grid box)]
               #   code  43  boxYC_water_ag2      [mol(C) m-2(grid box)]
               #   code  44  boxYC_water_bg2      [mol(C) m-2(grid box)]
               #   code  45  boxYC_ethanol_ag2    [mol(C) m-2(grid box)]
               #   code  46  boxYC_ethanol_bg2    [mol(C) m-2(grid box)]
               #   code  47  boxYC_nonsoluble_ag2 [mol(C) m-2(grid box)]
               #   code  48  boxYC_nonsoluble_bg2 [mol(C) m-2(grid box)]
               #   code  49  boxYC_humus_2        [mol(C) m-2(grid box)]
 
inf_co2=$7     # selected codes from echam6 co2 stream (monthly means)
               #   code   6  co2_flx_land         [kg m-2 s-1]
               #   code  24  co2_flx_lcc          [kg m-2 s-1]
               #   code  25  co2_flx_harvest      [kg m-2 s-1]
               #   code  26  co2_flx_fire         [kg m-2 s-1]

inf_BOT=$8     # selected codes from echam6 BOT stream (monthly means)
               #   code  140  soil wetness        [m]
               #   code  142  large scale precip  [kg/m**2s]
               #   code  143  convective precip   [kg/m**2s]
               #   code  146  sensible heat flux  [W/m**2]
               #   code  147  latent heat flux    [W/m**2]
               #   code  160  runoff and drainage [kg/m**2s]
               #   code  161  drainage            [kg/m**2s]
               #   code  167  2m temperature      [K]
               #   code  169  surface temperature [K]
               #   code  171  10m windspeed       [m/s]
               #   code  176  net surf. sw rad.   [W/m**2]
               #   code  177  net surf. lw rad.   [W/m**2]
               #   code  182  evaporation         [kg/m**2s]
               #   code  204  upward surf. sw rad [W/m**2]
               #   code  205  upward surf. lw rad [W/m**2]

inf_accw=$9    # selected codes from jsbach accw stream (monthly means)
               #   code  160  runoff and drainage [kg/m**2s]
               #   code  161  drainage            [kg/m**2s]

dynveg=${10}   # experiment with dynamic vegetation
yasso=${11}    # experiment with yasso soil carbon scheme

if [[ ! ${12} = "" ]]; then
  echam_fractional=yes
  slf=${12}       # if echam uses fractional land sea mask: fractional land mask
fi

#------------------------------------------------------------------------------
# land (and selected atmosphere) variables from echam BOT file
#------------------------------------------------------------------------------

if [[ -f ${inf_BOT} ]]; then

  # total runoff
  if [[ -f ${inf_accw} ]]; then
    cdo selcode,160 ${inf_accw} mrro.tmp
  else
    cdo selcode,160 ${inf_BOT}  mrro.tmp
  fi
  echo '&parameter name=mrro long_name="Total Runoff" units="kg m-2 s-1" /' > mrro.partab
  cdo -f nc -r setpartabn,mrro.partab -invertlat -setvar,mrro mrro.tmp mrro.nc

  # surface runoff
  if [[ -f ${inf_accw} ]]; then
    cdo sub -selcode,160 ${inf_accw} -selcode,161 ${inf_accw} mrros.tmp
  else
    cdo sub -selcode,160 ${inf_BOT}  -selcode,161 ${inf_BOT}  mrros.tmp
  fi
  echo '&parameter name=mrros long_name="Surface Runoff" units="kg m-2 s-1" /' > mrros.partab
  cdo -f nc -r setpartabn,mrros.partab -invertlat -setvar,mrros mrros.tmp mrros.nc

  # soil moisture content
  cdo mulc,1000. -selcode,140 ${inf_BOT}  mrso.tmp
  echo '&parameter name=mrso long_name="Total Soil Moisture Content" units="kg m-2" /' > mrso.partab
  cdo -f nc -r setpartabn,mrso.partab -invertlat -setvar,mrso mrso.tmp mrso.nc

  # evaporation
  cdo mulc,-1 -selcode,182 ${inf_BOT}  evspsbl.tmp
  echo '&parameter name=evspsbl long_name="Evaporation" units="kg m-2 s-1" /' > evspsbl.partab
  cdo -f nc -r setpartabn,evspsbl.partab -invertlat -setvar,evspsbl evspsbl.tmp evspsbl.nc

  # latent heat flux
  cdo mulc,-1 -selcode,147 ${inf_BOT}  hfls.tmp
  echo '&parameter name=hfls long_name="Surface Upward Latent Heat Flux" units="W m-2" /' > hfls.partab
  cdo -f nc -r setpartabn,hfls.partab -invertlat -setvar,hfls hfls.tmp hfls.nc

  # sensible heat flux
  cdo mulc,-1 -selcode,146 ${inf_BOT}  hfss.tmp
  echo '&parameter name=hfss long_name="Surface Upward Sensible Heat Flux" units="W m-2" /' > hfss.partab
  cdo -f nc -r setpartabn,hfss.partab -invertlat -setvar,hfss hfss.tmp hfss.nc

  # precipitation
  cdo add -selcode,142 ${inf_BOT} -selcode,143 ${inf_BOT} pr.tmp
  echo '&parameter name=pr long_name="Precipitation" units="kg m-2 s-1" /' > pr.partab
  cdo -f nc -r setpartabn,pr.partab -invertlat -setvar,pr pr.tmp pr.nc

  # downward shortwave radiation
  cdo sub -selcode,176 ${inf_BOT} -selcode,204 ${inf_BOT} rsds.tmp
  echo '&parameter name=rsds long_name="Surface Downwelling Shortwave Radiation" units="W m-2" /' > rsds.partab
  cdo -f nc -r setpartabn,rsds.partab -invertlat -setvar,rsds rsds.tmp rsds.nc

  # upward shortwave radiation
  cdo mulc,-1 -selcode,204 ${inf_BOT} rsus.tmp
  echo '&parameter name=rsus long_name="Surface Upwelling Shortwave Radiation" units="W m-2" /' > rsus.partab
  cdo -f nc -r setpartabn,rsus.partab -invertlat -setvar,rsus rsus.tmp rsus.nc

  # downward longwave radiation
  cdo sub -selcode,177 ${inf_BOT} -selcode,205 ${inf_BOT} rlds.tmp
  echo '&parameter name=rlds long_name="Surface Downwelling Longwave Radiation" units="W m-2" /' > rlds.partab
  cdo -f nc -r setpartabn,rlds.partab -invertlat -setvar,rlds rlds.tmp rlds.nc

  # upward longwave radiation
  cdo mulc,-1 -selcode,205 ${inf_BOT} rlus.tmp
  echo '&parameter name=rlus long_name="Surface Upwelling Longwave Radiation" units="W m-2" /' > rlus.partab
  cdo -f nc -r setpartabn,rlus.partab -invertlat -setvar,rlus rlus.tmp rlus.nc

  # 10m wind speed
  cdo selcode,171 ${inf_BOT}  sfcWind.tmp
  echo '&parameter name=sfcWind long_name="Near-Surface Wind Speed" units="m s-1" /' > sfcWind.partab
  cdo -f nc -r setpartabn,sfcWind.partab -invertlat -setvar,sfcWind sfcWind.tmp sfcWind.nc

  # 2m temperature
  cdo selcode,167 ${inf_BOT}  tas.tmp
  echo '&parameter name=tas long_name="Near-Surface Air Temperature" units="K" /' > tas.partab
  cdo -f nc -r setpartabn,tas.partab -invertlat -setvar,tas tas.tmp tas.nc

  # surface temperature
  cdo selcode,169 ${inf_BOT}  ts.tmp
  echo '&parameter name=ts long_name="Surface Temperature" units="K" /' > ts.partab
  cdo -f nc -r setpartabn,ts.partab -invertlat -setvar,ts ts.tmp ts.nc
fi
#------------------------------------------------------------------------------
# land carbon pools and fluxes from veg stream
#------------------------------------------------------------------------------

# convert from mol(C)/m2 to kg(C)/m2
#  factor: 0.012011

# carbon in vegetation
[[ ! -f cVeg.tmp ]] || rm cVeg.tmp
cdo enssum -vertsum -selcode,160 ${inf_veg} \
           -vertsum -selcode,161 ${inf_veg} \
           -vertsum -selcode,162 ${inf_veg}  cVeg.tmp
echo '&parameter name=cVeg long_name="Carbon Mass in Vegetation" units="kg m-2" /' > cVeg.partab
cdo -f nc -r setpartabn,cVeg.partab -invertlat -setvar,cVeg -mulc,0.012011 cVeg.tmp cVeg.nc

if [[ ${yasso} = yes ]]; then
  # carbon in humus pools
  [[ ! -f cHumus.tmp ]] || rm cHumus.tmp
  cdo enssum -vertsum -selcode,39 ${inf_yasso} \
             -vertsum -selcode,49 ${inf_yasso}  cHumus.tmp
  echo '&parameter name=cHumus long_name="Carbon Mass in Humus Pools" units="kg m-2" /' > cHumus.partab
  cdo -f nc -r setpartabn,cHumus.partab -invertlat -setvar,cHumus -mulc,0.012011 cHumus.tmp cHumus.nc

  # carbon in non-humus soil pools
  [[ ! -f cSoilFast.tmp ]] || rm cSoilFast.tmp
  cdo enssum -vertsum -selcode,31 ${inf_yasso} \
             -vertsum -selcode,32 ${inf_yasso} \
             -vertsum -selcode,33 ${inf_yasso} \
             -vertsum -selcode,34 ${inf_yasso} \
             -vertsum -selcode,35 ${inf_yasso} \
             -vertsum -selcode,36 ${inf_yasso} \
             -vertsum -selcode,37 ${inf_yasso} \
             -vertsum -selcode,38 ${inf_yasso} \
             -vertsum -selcode,41 ${inf_yasso} \
             -vertsum -selcode,42 ${inf_yasso} \
             -vertsum -selcode,43 ${inf_yasso} \
             -vertsum -selcode,44 ${inf_yasso} \
             -vertsum -selcode,45 ${inf_yasso} \
             -vertsum -selcode,46 ${inf_yasso} \
             -vertsum -selcode,47 ${inf_yasso} \
             -vertsum -selcode,48 ${inf_yasso}  cSoilFast.tmp
  echo '&parameter name=cSoilFast long_name="Carbon Mass in Fast Soil Pools" units="kg m-2" /' > cSoilFast.partab
  cdo -f nc -r setpartabn,cSoilFast.partab -invertlat -setvar,cSoilFast -mulc,0.012011 cSoilFast.tmp cSoilFast.nc
else
  # carbon in litter
  [[ ! -f cLitter.tmp ]] || rm cLitter.tmp
  cdo enssum -vertsum -selcode,159 ${inf_veg} \
             -vertsum -selcode,163 ${inf_veg} \
             -vertsum -selcode,179 ${inf_veg}  cLitter.tmp
  echo '&parameter name=cLitter long_name="Carbon Mass in Litter Pool" units="kg m-2" /' > cLitter.partab
  cdo -f nc -r setpartabn,cLitter.partab -invertlat -setvar,cLitter -mulc,0.012011 cLitter.tmp cLitter.nc

  # carbon in the soil
  cdo         vertsum -selcode,164 ${inf_veg}  cSoil.tmp
  echo '&parameter name=cSoil long_name="Carbon Mass in Soil Pool" units="kg m-2" /' > cSoil.partab
  cdo -f nc -r setpartabn,cSoil.partab -invertlat -setvar,cSoil -mulc,0.012011 cSoil.tmp cSoil.nc
fi

# GPP
cdo vertsum -selcode,173 ${inf_veg}  gpp.tmp
echo '&parameter name=gpp long_name="Carbon Mass Flux out of Atmosphere due to Gross Primary Production on Land" units="kg m-2 s-1" /' > gpp.partab
cdo -f nc -r setpartabn,gpp.partab -invertlat -setvar,gpp -mulc,0.012011 gpp.tmp gpp.nc

# NPP
cdo vertsum -selcode,178 ${inf_veg}  npp.tmp
echo '&parameter name=npp long_name="Carbon Mass Flux out of Atmosphere due to Net Primary Production on Land" units="kg m-2 s-1" /' > npp.partab
cdo -f nc -r setpartabn,npp.partab -invertlat -setvar,npp -mulc,0.012011 npp.tmp npp.nc

# potential NPP (used as NPP in cmip5 cmor output)
cdo vertsum -selcode,171 ${inf_veg}  npp_pot.tmp
echo '&parameter name=npp_pot long_name="potential Net Primary Production on Land" units="kg m-2 s-1" /' > npp_pot.partab
cdo -f nc -r setpartabn,npp_pot.partab -invertlat -setvar,npp_pot -mulc,0.012011 npp_pot.tmp npp_pot.nc

# autotrophic respiration
cdo sub -vertsum -selcode,173 ${inf_veg} \
        -vertsum -selcode,178 ${inf_veg}  ra.tmp
echo '&parameter name=ra long_name="Carbon Mass Flux into Atmosphere due to Autotrophic (Plant) Respiration on Land" units="kg m-2 s-1" /' > ra.partab
cdo -f nc -r setpartabn,ra.partab -invertlat -setvar,ra -mulc,0.012011 ra.tmp ra.nc

# soil respiration
cdo vertsum -selcode,170 ${inf_veg}  rh.tmp
echo '&parameter name=rh long_name="Carbon Mass Flux into Atmosphere due to Heterotrophic Respiration on Land" units="kg m-2 s-1" /' > rh.partab
cdo -f nc -r setpartabn,rh.partab -invertlat -setvar,rh -mulc,-0.012011 rh.tmp rh.nc

# litter flux
cdo vertsum -selcode,175 ${inf_veg}  fVegLitter.tmp
echo '&parameter name=fVegLitter long_name="Total Carbon Mass Flux from Vegetation to Litter" units="kg m-2 s-1" /' > fVegLitter.partab
cdo -f nc -r setpartabn,fVegLitter.partab -invertlat -setvar,fVegLitter -mulc,0.012011 fVegLitter.tmp fVegLitter.nc

#------------------------------------------------------------------------------
# variables from the land stream
#------------------------------------------------------------------------------

# LAI
cdo selcode,107 ${inf_land}  lai.tmp
echo '&parameter name=lai long_name="Leaf Area Index" units="1" /' > lai.partab
cdo -f nc -r setpartabn,lai.partab -invertlat -setvar,lai lai.tmp lai.nc

# transpiration
cdo mulc,-1 -selcode,76 ${inf_land}  tran.tmp
echo '&parameter name=tran long_name="Transpiration" units="kg m-2 s-1" /' > tran.partab
cdo -f nc -r setpartabn,tran.partab -invertlat -setvar,tran tran.tmp tran.nc

# soil temperature
cdo sellevidx,1 -selcode,68 ${inf_land}  tsl.tmp
echo '&parameter name=tsl long_name="Temperature of Soil" units="K" /' > tsl.partab
cdo -f nc -r setpartabn,tsl.partab -invertlat -setvar,tsl tsl.tmp tsl.nc

#------------------------------------------------------------------------------
# carbon fluxes from co2 stream
#------------------------------------------------------------------------------

# convert from [kg(CO2)/m2 s] to [kg(C)/m2 s]
# molar Mass CO2: 44.0095 
# molar Mass C:   12,011
#  => factor: 0.272912

if [[ $(cdo showcode ${inf_co2} | grep 26 ) != "" ]]; then
  fire_flux=yes
  # carbon flux due to fire
  cdo mulc,0.272912 -selcode,26 ${inf_co2}  fFire.tmp
  echo '&parameter name=fFire long_name="Carbon Mass Flux into Atmosphere due to CO2 Emission from Fire" units="kg m-2 s-1" /' > fFire.partab
  cdo -f nc -r setpartabn,fFire.partab -invertlat -setvar,fFire fFire.tmp fFire.nc

  # carbon flux due to land use change
  cdo mulc,0.272912 -selcode,24 ${inf_co2}  fLuc.tmp
  echo '&parameter name=fLuc long_name="Net Carbon Mass Flux into Atmosphere due to Land Use Change" units="kg m-2 s-1" /' > fLuc.partab
  cdo -f nc -r setpartabn,fLuc.partab -invertlat -setvar,fLuc fLuc.tmp fLuc.nc

  # net biospheric production
  [[ ! -f  nbp.tmp ]] || rm nbp.tmp
  cdo enssum -selcode,6 ${inf_co2} -selcode,24 ${inf_co2} -selcode,25 ${inf_co2}  nbp.tmp
  echo '&parameter name=nbp long_name="Carbon Mass Flux out of Atmosphere due to Net Biospheric Production on Land" units="kg m-2 s-1" /' > nbp.partab
  cdo -f nc -r setpartabn,nbp.partab -invertlat -mulc,0.272912 -setvar,nbp nbp.tmp nbp.nc
fi

#------------------------------------------------------------------------------
# land cover frations
#------------------------------------------------------------------------------

# find out number of tiles. This section only works with 8 or 11 tiles.

ntiles=$(cdo -s nlevel -selzaxis,3 ${inf_ini} | head -1)
if [[ ${ntiles} != 11 && ${ntiles} != 8 ]]; then
  echo "cmor_land.sh: Calculation of vegetation fractions only possible with 8 or 11 tiles"
else

# preparations
  cdo selvar,slm        ${inf_ini}  slm
  [[ ${echam_fractional} != yes ]] && slf=slm
  cdo invertlat -mul ${slf} -gridarea ${slf} gridareafrac.nc
  cdo selvar,cover_type ${inf_ini}  cover_type
  cdo selcode,12        ${inf_main} cover_fract
  if [[ ${dynveg} = yes ]]; then
    cdo selcode,20      ${inf_main} veg_ratio_max
  else
    cdo selvar,veg_ratio_max  ${inf_ini} veg_ratio_max
  fi
  if [[ ${ntiles} = 11 && $(cdo showcode ${inf_main} | grep 10 ) != "" ]]; then
    cdo selcode,10      ${inf_main} cover_fract_pot
    no_cover_fract_pot=false
  else
    no_cover_fract_pot=true
  fi

  #----------------------------------------------------------------------------

  # forest fraction
  cdo ifthen slm -mul veg_ratio_max -vertsum -sellevel,1/4  cover_fract treeFrac.tmp
  echo '&parameter name=treeFrac long_name="Tree Cover Fraction" units="%" /' > treeFrac.partab
  cdo -f nc -r setpartabn,treeFrac.partab -invertlat -setvar,treeFrac -mulc,100. treeFrac.tmp treeFrac.nc

  # shrub fraction
  cdo ifthen slm -mul veg_ratio_max -vertsum -sellevel,5/6  cover_fract shrubFrac.tmp
  echo '&parameter name=shrubFrac long_name="Shrub Fraction" units="%" /' > shrubFrac.partab
  cdo -f nc -r setpartabn,shrubFrac.partab -invertlat -setvar,shrubFrac -mulc,100. shrubFrac.tmp shrubFrac.nc

  # grass fraction
  cdo ifthen slm -mul veg_ratio_max -vertsum -sellevel,7/8  cover_fract grassFrac.tmp
  echo '&parameter name=grassFrac long_name="Natural Grass Fraction" units="%" /' > grassFrac.partab
  cdo -f nc -r setpartabn,grassFrac.partab -invertlat -setvar,grassFrac -mulc,100. grassFrac.tmp grassFrac.nc

  if [[ ${ntiles} = 11 ]]; then

    # crop fraction
    cdo ifthen slm -mul veg_ratio_max -vertsum -sellevel,11   cover_fract cropFrac.tmp
    echo '&parameter name=cropFrac long_name="Crop Fraction" units="%" /' > cropFrac.partab
    cdo -f nc -r setpartabn,cropFrac.partab -invertlat -setvar,cropFrac -mulc,100. cropFrac.tmp cropFrac.nc

    # pasture fraction
    cdo ifthen slm -mul veg_ratio_max -vertsum -sellevel,9/10 cover_fract pastureFrac.tmp
    echo '&parameter name=pastureFrac long_name="Anthropogenic Pasture Fraction" units="%" /' > pastureFrac.partab
    cdo -f nc -r setpartabn,pastureFrac.partab -invertlat -setvar,pastureFrac -mulc,100. pastureFrac.tmp pastureFrac.nc

    if [[ ${no_cover_fract_pot} = false ]]; then
      # potential forest fraction
      cdo ifthen slm -mul veg_ratio_max -vertsum -sellevel,1/4  cover_fract_pot treeFracPot.tmp
      echo '&parameter name=treeFracPot long_name="Potential Tree Cover Fraction" units="%" /' > treeFracPot.partab
      cdo -f nc -r setpartabn,treeFracPot.partab -invertlat -setvar,treeFracPot -mulc,100. treeFracPot.tmp treeFracPot.nc

      # potential shrub fraction
      cdo ifthen slm -mul veg_ratio_max -vertsum -sellevel,5/6  cover_fract_pot shrubFracPot.tmp
      echo '&parameter name=shrubFracPot long_name="Potential Shrub Fraction" units="%" /' > shrubFracPot.partab
      cdo -f nc -r setpartabn,shrubFracPot.partab -invertlat -setvar,shrubFracPot -mulc,100. shrubFracPot.tmp shrubFracPot.nc

      # potential grass fraction
      cdo ifthen slm -mul veg_ratio_max -vertsum -sellevel,7/8  cover_fract_pot grassFracPot.tmp
      echo '&parameter name=grassFracPot long_name="Potential natural Grass Fraction" units="%" /' > grassFracPot.partab
      cdo -f nc -r setpartabn,grassFracPot.partab -invertlat -setvar,grassFracPot -mulc,100. grassFracPot.tmp grassFracPot.nc
    fi
  fi

  # C3 plant fraction
  [[ ! -f c3PftFrac.tmp ]] || rm c3PftFrac.tmp
  cdo   ifthen slm -mul veg_ratio_max -sellevel,7 cover_fract c3grass.tmp
  if [[ ${ntiles} = 11 ]]; then
    cdo ifthen slm -mul veg_ratio_max -sellevel,9 cover_fract c3pasture.tmp
    cdo subc,20 -sellevel,11 cover_type msk
    cdo ifthen slm -mul veg_ratio_max -setmisstoc,0 -ifnotthen msk -sellevel,11 cover_fract c3crop.tmp
    cdo enssum treeFrac.tmp shrubFrac.tmp c3grass.tmp c3pasture.tmp c3crop.tmp  c3PftFrac.tmp
  else
    cdo enssum treeFrac.tmp shrubFrac.tmp c3grass.tmp  c3PftFrac.tmp
  fi
  echo '&parameter name=c3PftFrac long_name="Total C3 PFT Cover Fraction" units="%" /' > c3PftFrac.partab
  cdo -f nc -r setpartabn,c3PftFrac.partab -invertlat -setvar,c3PftFrac -mulc,100. c3PftFrac.tmp c3PftFrac.nc

  # C4 plant fraction
  [[ ! -f c4PftFrac.tmp ]] || rm c4PftFrac.tmp
  cdo   ifthen slm -mul veg_ratio_max -sellevel,8  cover_fract c4grass.tmp
  if [[ ${ntiles} = 11 ]]; then
    cdo ifthen slm -mul veg_ratio_max -sellevel,10 cover_fract c4pasture.tmp
    cdo subc,20 -sellevel,11 cover_type msk
    cdo ifthen slm -mul veg_ratio_max -setmisstoc,0 -ifthen msk -sellevel,11 cover_fract c4crop.tmp
    cdo enssum  c4grass.tmp c4pasture.tmp c4crop.tmp  c4PftFrac.tmp
  else
    cdo copy c4grass.tmp c4PftFrac.tmp
  fi
  echo '&parameter name=c4PftFrac long_name="Total C4 PFT Cover Fraction" units="%" /' > c4PftFrac.partab
  cdo -f nc -r setpartabn,c4PftFrac.partab -invertlat -setvar,c4PftFrac -mulc,100. c4PftFrac.tmp c4PftFrac.nc

  # residual fraction (glaciers)
  cdo subc,1 -sellevel,1 cover_type msk
  cdo ifthen slm -setmisstoc,0 -ifnotthen msk -sellevel,1 cover_fract residualFrac.tmp
  echo '&parameter name=residualFrac long_name="Fraction of Grid Cell that is Land but Neither Vegetation-Covered nor Bare Soil" units="%" /' > residualFrac.partab
  cdo -f nc -r setpartabn,residualFrac.partab -invertlat -setvar,residualFrac -mulc,100. residualFrac.tmp residualFrac.nc

  # bare soil (desert)
  cdo sub -addc,1 -mulc,-1 veg_ratio_max residualFrac.tmp baresoilFrac.tmp
  echo '&parameter name=baresoilFrac long_name="Bare Soil Fraction" units="%" /' > baresoilFrac.partab
  cdo -f nc -r setpartabn,baresoilFrac.partab -invertlat -setvar,baresoilFrac -mulc,100. baresoilFrac.tmp baresoilFrac.nc

  # burnt area (for exact results with code 32, you have to use daily data)
  #
  #   burnt = veg_ratio_max * SUM(act_fpc(i)) * SUM(cover_fract(i) * burned_fpc(i)/act_fpc(i))
  if [[ ${dynveg} = yes ]]; then
    if [[ $(cdo showcode ${inf_veg} | grep 32 ) != "" ]]; then
      cdo -f nc -r ifthen slm -mul veg_ratio_max -muldpm -mul -vertsum -selcode,31 ${inf_veg} \
        -vertsum -mul cover_fract -div -selcode,32 ${inf_veg} -selcode,31 ${inf_veg} burntArea.tmp
    else
      cdo -f nc -r muldpm -vertsum -selcode,53 ${inf_veg} burntArea.tmp
    fi
    echo '&parameter name=burntArea long_name="Burnt Area Fraction" units="%" /' > burntArea.partab
    cdo -f nc -r setpartabn,burntArea.partab -invertlat -setvar,burntArea -mulc,100. burntArea.tmp burntArea.nc
  fi
fi

#------------------------------------------------------------------------------
# generate global time series and move the 2d netcdf output to outdata_land
#------------------------------------------------------------------------------
 
[[ -d outdata_land ]] || mkdir outdata_land
file_list="cVeg gpp npp npp_pot rh ra fVegLitter lai tran tsl"
if [[ ${yasso} = yes ]]; then
  file_list="${file_list} cHumus cSoilFast"
else
  file_list="${file_list} cLitter cSoil"
fi
if [[ ${ntiles} = 8 || ${ntiles} = 11 ]]; then
  file_list="${file_list} treeFrac shrubFrac grassFrac c3PftFrac c4PftFrac residualFrac baresoilFrac"
  [[ ${dynveg} = yes ]]    && file_list="${file_list} burntArea"
fi
if [[ ${ntiles} = 11 ]]; then
  file_list="${file_list} cropFrac pastureFrac"
  if [[ ${no_cover_fract_pot} = false ]]; then
    file_list="${file_list} treeFracPot shrubFracPot grassFracPot"
  fi
fi
[[ ${fire_flux} = yes ]] && file_list="${file_list} fFire fLuc nbp"
if [[ -f ${inf_BOT} ]]; then
  file_list="${file_list} mrro mrros mrso evspsbl hfls hfss pr rsds rsus rlds rlus sfcWind tas ts"
fi

for f in ${file_list}; do

  # generate global time series
  #   global surface area: 5.100656e14 m^2
  #   seconds per year: 31557600
  case $f in
    *Frac | *FracPot | burntArea )
      # % -> Mio km^2
      cdo mul ${f}.nc -invertlat ${slf} ${f}.slf
      echo '&parameter name=$f units="Mio km^2" /' > ${f}.partab
      cdo setpartabn,${f}.partab -mulc,5.100656 -fldmean -setmisstoc,0 -yearavg ${f}.slf \
                                                 outdata_land/${f}_${expid}.global_area.nc
      ;;
    c[A-Z]* )
      # kg m-2 -> Gt
      cdo mul ${f}.nc -invertlat ${slf} ${f}.slf
      echo '&parameter name=$f units="Gt" /' > ${f}.partab
      cdo setpartabn,${f}.partab -mulc,5.100656e02 -fldmean -setmisstoc,0 -yearavg ${f}.slf \
                                                 outdata_land/${f}_${expid}.global_mass.nc
      ;;
    f[A-Z]* | gpp | nbp | npp | npp_pot | ra | rh )
      # kg m-2 s-1 -> Gt yr-1
      cdo mul ${f}.nc -invertlat ${slf} ${f}.slf
      echo '&parameter name=$f units="Gt yr-1" /' > ${f}.partab
      cdo setpartabn,${f}.partab -mulc,5.100656e02 -mulc,31557600 -fldmean -setmisstoc,0 -yearavg ${f}.slf \
                                                 outdata_land/${f}_${expid}.global_flux.nc
      ;;
    lai | tsl )
      # global land mean
      cdo setgridarea,gridareafrac.nc ${f}.nc ${f}.slf
      cdo fldmean -yearavg ${f}.slf outdata_land/${f}_${expid}.land_mean.nc
      ;;
    * )
      # global mean
      cdo fldmean -setmisstoc,0 -yearavg ${f}.nc outdata_land/${f}_${expid}.global_mean.nc
      ;;
  esac
 
  mv ${f}.nc outdata_land/${f}_${expid}.nc
done

# clean up
rm -f cover_type cover_fract veg_ratio_max msk slm gridareafrac.nc
rm *.tmp
rm -f cover_fract_pot *.slf

exit




