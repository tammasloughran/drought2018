#!/bin/sh
#
# Compute total carbon inventory, i.e. sum of global C content in ocean, atmosphere and land
#
# The input files are expected to be monthly averages
set -e

inf_ocean=$1       # hamocc output file, code 301 [Mol(C)]
inf_atmos=$2       # echam5 co2 output stream, code 8 co2_burden [kg(CO2)/m^2]
inf_land=$3        # jsbach veg output stream, code 176 box_Cpools_total [mol(CO2)/m^2]

# Convert ocean C content from [kMol C] to [Gt C]
# 1 mol C = 12.0107 g C  = 1.20107e-14 Gt C
cdo -f ext -b F64 mulc,1.20107e-14 -selcode,305 ${inf_ocean} z$$.ext
cdo setcode,301 -setday,15 z$$.ext ocean.ext
rm z$$.ext

# look for code 203: cinput  [kmol C]
[[ "$(cdo showcode ${inf_ocean} | grep 203)" != "" ]] && cinput=yes
if [[ ${cinput} = yes ]]; then
  # accumulated calcinput (see hamocc namelist deltacalc)
  cdo -f ext -b F64 mulc,1.20107e-14 -mulc,1000 -selcode,203 ${inf_ocean} z$$.ext
  cdo setcode,301 -setday,15 z$$.ext cinput.ext                
  rm z$$.ext
  # we add the accumulated calcinput which was previously substracted in hamocc
  cdo -f ext -b F64 mulc,1.20107e-14 -add -selcode,306 ${inf_ocean} -mulc,1000 -selcode,203 ${inf_ocean} z$$.ext
  cdo setcode,301 -setday,15 z$$.ext sediment.ext
  rm z$$.ext
else
  cdo -f ext -b F64 mulc,1.20107e-14 -selcode,306 ${inf_ocean} z$$.ext
  cdo setcode,301 -setday,15 z$$.ext sediment.ext
  cdo sub z$$.ext z$$.ext cinput.ext # dummy: cint=zero 
  rm z$$.ext
fi

# Convert atmospheric CO2 burden to C content
# 44.0095 g CO2 = 12.0107 g C  => 1 g CO2 = 12.0107/44.0095 g C = 0.272912 g C
# Global surface area: 5.100656e14 m^2
# => 1 kg CO2 / m^2 (area weighted global average) = 5.100656e14 * 0.272912 * 1e-12 Gt C = 139.20 Gt C
cdo fldavg -selcode,8 ${inf_atmos} z$$
cdo -f ext -b F64 mulc,139.20 z$$ z$$.ext
cdo setcode,301 -setday,15 z$$.ext atmos.ext
rm z$$ z$$.ext

# Convert land C pool from [mol(CO2)/m^2] to [Gt C]
# 1 mol CO2 = 12.0107 g C   => 1 mol CO2 = 1.20107e-14 Gt C
# Global surface area: 5.100656e14 m^2
# => 1 mol CO2 / m^2 (area weighted global average) = 1.20107e-14 * 5.100656e14 Gt C = 6.126245 Gt C
cdo fldavg -setmisstoc,0 -selcode,176 ${inf_land} z$$
cdo -f ext -b F64 mulc,6.126245 z$$ z$$.ext
cdo setcode,301 -setday,15 z$$.ext land.ext
rm z$$ z$$.ext

# Compute global inventory
rm -f invent.ext
cdo enssum ocean.ext sediment.ext cinput.ext atmos.ext land.ext invent.ext

for f in ocean sediment cinput atmos land invent 
do
  cdo output $f.ext > $f.asc
done

