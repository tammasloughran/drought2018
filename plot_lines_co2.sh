#!/bin/ksh
#
# Author
# Renate Brokopf, MPI, 2004
# Monika Esch, MPI, 2004, 2008
# Changes:
#   Veronika Gayler, June 2009
#     - removed ZMAW path to grads
#     - usage of 'cdo gradsdes' instead of locally installed gradsdes.
#       This entails usage of variable names instead of code numbers in
#       the grads scripts
#
set -e
cdo=cdo
[[ -f ${cdo} ]] || cdo=cdo
#
EXP=$1
RES=$2
bot=outdata/BOT_MON
co2=outdata/CO2_MON
#
years=`${cdo} showyear $bot`
years=${years##+([ ])}
yrs=${years%% *}
yre=${years##* }
#yrs=0800 ;# start year of file
#yre=0886 ;# end year of file
#
#
nra=5 ;# running average window length [years]
#
# ${cdo} selcode,97,103,167,210,211 ... codes_${EXP}
${cdo} setday,15 -selyear,${yrs}/${yre} ${bot} codes_${EXP}m
${cdo} yearavg codes_${EXP}m codes_${EXP}y
${cdo} setday,30 codes_${EXP}y codes_${EXP}yin
${cdo} setmon,06 codes_${EXP}yin codes_${EXP}y
${cdo} runmean,${nra} codes_${EXP}y codes_${EXP}yr
rm codes_${EXP}yin
${cdo} setday,15 -selyear,${yrs}/${yre} ${co2} CO2_${EXP}m
${cdo} yearavg CO2_${EXP}m CO2_${EXP}y
${cdo} setday,30 CO2_${EXP}y CO2_${EXP}yin
${cdo} setmon,06 CO2_${EXP}yin CO2_${EXP}y
rm CO2_${EXP}yin
#
# Strip down monthly files and retain only needed variables
# Otherwise, the file might become too large for grads (2GB limit)
${cdo} -t echam6 selvar,friac,siced,tsw codes_${EXP}m z$$ && mv z$$ codes_${EXP}m
#
${cdo} -t echam6 -f nc -r copy codes_${EXP}m  codes_${EXP}m.nc
${cdo} -t echam6 -f nc -r copy codes_${EXP}y  codes_${EXP}y.nc
${cdo} -t echam6 -f nc -r copy codes_${EXP}yr codes_${EXP}yr.nc
#
${cdo} -t echam6 -f nc -r copy CO2_${EXP}m    CO2_${EXP}m.nc
${cdo} -t echam6 -f nc -r copy CO2_${EXP}y    CO2_${EXP}m.nccase 
#
#######
exp=${EXP}
nyear=`expr ${yre} - ${yrs} + 1 `
nyearr=`expr ${nyear} - ${nra} + 1 `
nmonth=`expr ${nyear} \* 12 `

cat >scen.gs<<EOF
'reinit'
'open $(dirname $0)/fixcode_${RES}.ctl'
'sdfopen codes_${exp}y.nc'
'sdfopen codes_${exp}m.nc'
'sdfopen codes_${exp}yr.nc'

'set dfile 1'
'set t 1'
'define c172=slm'

'set dfile 2'
'set t 1 ${nyear}'

'define su=1.-friac-c172'
'define cc = tsw * su'
'define sum=aave(su,lon=0,lon=360,lat=-90,lat=90)'
'define ccm=aave(cc,lon=0,lon=360,lat=-90,lat=90)'
'define temp=aave(temp2,lon=0,lon=360,lat=-90,lat=90)-273.15'
'define stemp=(ccm/sum)-273.15' 
'define tcc=aave(aclcov,lon=0,lon=360,lat=-90,lat=90)*100.'
'define zwischi=aave(srad0u-srad0,lon=0,lon=360,lat=-90,lat=90)'
'define zwaschi=aave(srad0u,lon=0,lon=360,lat=-90,lat=90)'
'define plana=zwaschi/zwischi*100.'
'define nssr=aave(srads,lon=0,lon=360,lat=-90,lat=90)'
'define nstr=aave(trads,lon=0,lon=360,lat=-90,lat=90)'
'define ntsr=aave(srad0,lon=0,lon=360,lat=-90,lat=90)'
'define olr=aave(trad0,lon=0,lon=360,lat=-90,lat=90)*-1.'
'define tprec=aave(precip,lon=0,lon=360,lat=-90,lat=90)*86400.'
'define viwv=aave(qvi,lon=0,lon=360,lat=-90,lat=90)'
'define xivi1=aave(xivi,lon=0,lon=360,lat=-90,lat=90)*1000.'
'define xlvi1=aave(xlvi,lon=0,lon=360,lat=-90,lat=90)*1000.'
'define swtc=aave(srad0-sraf0,lon=0,lon=360,lat=-90,lat=90)'
'define lwtc=aave(trad0-traf0,lon=0,lon=360,lat=-90,lat=90)'
'define zwasch=srad0+trad0'
'define tnr=aave(zwasch,lon=0,lon=360,lat=-90,lat=90)'
'define zwusch=ahfs+ahfl+srads+trads'
'define snhf=aave(zwusch,lon=0,lon=360,lat=-90,lat=90)'

'define c97211=(friac*siced)*255'
'define nord211=aave(c97211,lon=0,lon=359,lat=0.1,lat=90)'
'define sued211=aave(c97211,lon=0,lon=359,lat=-90,lat=-0.1)'

'define c97=friac*255'
'define nord =aave(c97,lon=0,lon=359,lat=0.1,lat=90)'
'define sued =aave(c97,lon=0,lon=359,lat=-90,lat=-0.1)'

'set dfile 3'
'set t 1 ${nmonth}'
'define c97211m=(friac*siced)*255'
'define nord211m=aave(c97211m,lon=0,lon=359,lat=0.1,lat=90)'
'define sued211m=aave(c97211m,lon=0,lon=359,lat=-90,lat=-0.1)'

'define c97m=friac*255'
'define nordm =aave(c97m,lon=0,lon=359,lat=0.1,lat=90)'
'define suedm =aave(c97m,lon=0,lon=359,lat=-90,lat=-0.1)'

'set dfile 4'
'set t 1 ${nyearr}'

'define su3=1.-friac-c172'
'define cc3 = tsw * su3'
'define sum3=aave(su3,lon=0,lon=360,lat=-90,lat=90)'
'define ccm3=aave(cc3,lon=0,lon=360,lat=-90,lat=90)'
'define temp3=aave(temp2,lon=0,lon=360,lat=-90,lat=90)-273.15'
'define stemp3=(ccm3/sum3)-273.15'
'define tcc3=aave(aclcov,lon=0,lon=360,lat=-90,lat=90)*100.'
'define zwischi3=aave(srad0u-srad0,lon=0,lon=360,lat=-90,lat=90)'
'define zwaschi3=aave(srad0u,lon=0,lon=360,lat=-90,lat=90)'
'define plana3=zwaschi3/zwischi3*100.'
'define nssr3=aave(srads,lon=0,lon=360,lat=-90,lat=90)'
'define nstr3=aave(trads,lon=0,lon=360,lat=-90,lat=90)'
'define ntsr3=aave(srad0,lon=0,lon=360,lat=-90,lat=90)'
'define olr3=aave(trad0,lon=0,lon=360,lat=-90,lat=90)*-1.'
'define tprec3=aave(precip,lon=0,lon=360,lat=-90,lat=90)*86400.'
'define viwv3=aave(qvi,lon=0,lon=360,lat=-90,lat=90)'
'define xivi3=aave(xivi,lon=0,lon=360,lat=-90,lat=90)*1000.'
'define xlvi3=aave(xlvi,lon=0,lon=360,lat=-90,lat=90)*1000.'
'define swtc3=aave(srad0-sraf0,lon=0,lon=360,lat=-90,lat=90)'
'define lwtc3=aave(trad0-traf0,lon=0,lon=360,lat=-90,lat=90)'
'define zwasch3=srad0+trad0'
'define tnr3=aave(zwasch3,lon=0,lon=360,lat=-90,lat=90)'
'define zwusch3=ahfs+ahfl+srads+trads'
'define snhf3=aave(zwusch3,lon=0,lon=360,lat=-90,lat=90)'

'set dfile 2'
'set t 1 ${nyear}'

'set lon 1 1'
'set lat 1 1'

'enable print meta'

******************U*********************

init2a()

'set vrange 18.0 20.0'
'set ylint .5'
'set ylab %.2f'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd stemp'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd stemp3'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Sea surface temperature [C] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange 13.0 16.0'
'set ylint 1.'
'set ylab %.2f'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd temp'
'set cthick 10'
'set cmark 0'
'set ccolor 4'
'd temp3'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Surface air temperature [C] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_1.ps'

'enable print meta'
******************U*********************
* ice area

init2a2()

'set vrange 0 20'
'set ylint 5'
*'set ylab %.1f'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 4'
'd nordm'
'set ccolor 4'
'set cmark 0'
'set cthick 10'
'd nord'
'set line 2 1 10'
'draw line 0.75 3.3 7.75 3.3'
'draw line 0.75 1.7 7.75 1.7'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7  Northern hemisphere'
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'

*************V************************

init2b()


'set vrange 0 20'
'set ylint 5'
*'set ylab %.1f'

'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 4'
'd suedm'
'set ccolor 4'
'set cmark 0'
'set cthick 10'
'd sued'
'set line 2 1 10'
'draw line 0.75 3.5 7.75 3.5'
'draw line 0.75 0.9 7.75 0.9'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Southern hemisphere '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'

'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_2.ps'

'enable print meta'
******************U*********************

init2a3()

'set vrange 0 50'
'set ylint 5.'
*'set ylab %.1f'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 4'
'd nord211m'
'set ccolor 4'
'set cmark 0'
'set cthick 10'
'd nord211'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7  Northern hemisphere'
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'

*************V************************

init2b()


'set vrange 0 20'
'set ylint 2.'
*'set ylab %.1f'

'set ccolor 3'
'set cmark 0'
'set cthick 4'
'd sued211m'
'set grid on 0 1'
'set ccolor 4'
'set cmark 0'
'set cthick 10'
'd sued211'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Southern hemisphere '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'

'print '

'disable print'
'!gxps -c -i meta -o years_${exp}_3.ps'
'enable print meta'

******************U*********************

init2a()

'set vrange 60.0 70.0'
'set ylint 1.'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd tcc'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd tcc3'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Total Cloud Cover [%] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange 30. 34.'
'set ylint 0.5'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd plana'
'set cthick 10'
'set cmark 0'
'set ccolor 4'
'd plana3'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Planetary Albedo [%] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_4.ps'

'enable print meta'

******************U*********************

init2a()

'set vrange 2.4 3.2'
*'set ylint .1'
'set ylab %.2f'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd tprec'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd tprec3'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Total Precipitation [mm/d] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange 22.0 28.0'
'set ylint 2.'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd viwv'
'set cthick 10'
'set cmark 0'
'set ccolor 4'
'd viwv3'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Vertically integrated Water Vapour [kg/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_5.ps'

'enable print meta'

******************U*********************

init2a()

'set vrange 155.0 165.'
'set ylint 1'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd nssr'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd nssr3'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Net Surface Solar Radiation [W/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange -60.0 -50.0'
'set ylint 2.5'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd nstr'
'set cthick 10'
'set cmark 0'
'set ccolor 4'
'd nstr3'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Net Surface Thermal Radiation [W/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_6.ps'

'enable print meta'

******************U*********************

init2a()

'set vrange 233.0 238.'
'set ylint 1'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd ntsr'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd ntsr3'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Net Top Solar Radiation [W/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange 233.0 238.0'
'set ylint 1'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd olr'
'set cthick 10'
'set cmark 0'
'set ccolor 4'
'd olr3'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 OLR [W/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_7.ps'

'enable print meta'

******************U*********************

init2a()

'set vrange 35. 37.'
*'set ylint 1'
*'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd xivi1'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd xivi3'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Vertically Integrated Cloud Ice [g/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange 55.0 65.0'
'set ylint 1'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd xlvi1'
'set cthick 10'
'set cmark 0'
'set ccolor 4'
'd xlvi3'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Vertically Integrated Cloud Water [g/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_8.ps'

'enable print meta'

******************U*********************

init2a()

'set vrange -55.0 -50.'
'set ylint 1'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd swtc'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd swtc3'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 SW Top Cloud Forcing [W/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange 27.0 29.0'
*'set ylint 1'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd lwtc'
'set cthick 10'
'set cmark 0'
'set ccolor 4'
'd lwtc3'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 LW Top Cloud Forcing [W/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_9.ps'
'enable print meta'

******************U*********************

init2a()

'set vrange 0.0 5.5'
'set ylint .5'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd tnr'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd tnr3'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 TOA Net Radiation [W/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year' 
*************V************************
 
init2b()
 
'set grid on 0 1'
'set vrange 0.0 5.0'
'set ylint 0.5'
'set cthick 6'
'set ccolor 3'
'set cmark 0'
'd snhf'
'set cthick 10'
'set cmark 0'
'set ccolor 4'
'd snhf3'
 
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Surface Net Heat Flux [W/m2] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
 
'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_10.ps'

'!rm meta'
'quit'
*--------------------------------------------------
function init2a ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 Global and Annual Means'


*----rahmen setzen----* 
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'

return

*--------------------------------------------------
function init2a2 ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 Sea Ice Area [10\`a12\`n m\`a2\`n]'
*----rahmen setzen----*
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'
return
*--------------------------------------------------
function init2a3 ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 Sea Ice Volume [10\`a12\`n m\`a3\`n]'

*----rahmen setzen----*
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'
return

*--------------------------------------------------
function init2b ()
*--------------------------------------------------

*----rahmen setzen----* 
'set vpage 0. 8.5 0. 5.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'

return 
EOF

grads -cbp scen.gs

cat >scen_co2.gs<<EOF
'reinit'
'sdfopen CO2_${exp}m.nc'
'sdfopen CO2_${exp}y.nc'

'set dfile 2'
'set t 1 ${nyear}'

'define co2flx=aave(var5,lon=0,lon=360,lat=-90,lat=90)*4.39355e+09/12'
'define co2flxl=aave(var6,lon=0,lon=360,lat=-90,lat=90)*4.39355e+09/12'
'define co2flxo=aave(var7,lon=0,lon=360,lat=-90,lat=90)*4.39355e+09/12'
'define co2flxa=aave(var20,lon=0,lon=360,lat=-90,lat=90)*4.39355e+09/12'
'define co2burden=aave(var8,lon=0,lon=360,lat=-90,lat=90)*139.223*0.470396'
* 139.223 -> GtC
* 0.470396 -> ppm

'set dfile 1'
'set t 1 ${nmonth}'
'define co2flxm=aave(var5,lon=0,lon=360,lat=-90,lat=90)*3.66129e+08'
'define co2flxlm=aave(var6,lon=0,lon=360,lat=-90,lat=90)*3.66129e+08'
'define co2flxom=aave(var7,lon=0,lon=360,lat=-90,lat=90)*3.66129e+08'
'define co2flxam=aave(var20,lon=0,lon=360,lat=-90,lat=90)*3.66129e+08'
'define co2burdenm=aave(var8,lon=0,lon=360,lat=-90,lat=90)*139.223*0.470396'

'set dfile 2'
'set t 1 ${nyear}'

'set lon 1 1'
'set lat 1 1'

'enable print meta'

******************U*********************
******************U*********************

init2a()

'set vrange -2 2'
'set ylint .5'
*'set ylab %.2f'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 6'
'd co2flxm'
'set cmark 0'
'set ccolor 4'
'set cthick 10'
'd co2flx'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 CO2 Flux [GtC/mo] '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.07 0.1'
'set string 2 l 6'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'
*************V************************
 init2b()

 'set grid on 0 1'
* 'set vrange 272. 288.'
 'set vrange 275. 330.'
 'set ylint 25.'
 'set cthick 6'
 'set ccolor 3'
 'set cmark 0'
 'd co2burdenm'
 'set cthick 10'
 'set cmark 0'
 'set ccolor 4'
 'd co2burden'

 'set strsiz 0.1 0.15'
 'set string 1 l 6'
 'draw string 1. 4.7 CO2 Burden [ppm] '
 'set strsiz 0.07 0.1'
 'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
 'set strsiz 0.07 0.1'
 'set string 2 l 6'
 'set strsiz 0.1 0.15'
 'set string 1 l 6'
 'draw string 7.5 .2 year'

'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_co2_1.ps'
 
'enable print meta'
******************U*********************
init2a2()

'set vrange -2 2'
'set ylint 1.'
**'set ylab %.1f'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 4'
'd co2flxlm'
'set ccolor 4'
'set cmark 0'
'set cthick 10'
'd co2flxl'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7  Land'
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'

*************V************************
init2b()


'set vrange -2 2'
'set ylint .5'
**'set ylab %.1f'

'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 4'
'd co2flxom'
'set ccolor 4'
'set cmark 0'
'set cthick 10'
'd co2flxo'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7 Ocean '
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'

'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_co2_2.ps'
'!rm meta'

'enable print meta'
******************U*********************
init2a3()

'set vrange -2 5'
'set ylint .5'
**'set ylab %.1f'
'set grid on 0 1'
'set ccolor 3'
'set cmark 0'
'set cthick 4'
'd co2flxam'
'set ccolor 4'
'set cmark 0'
'set cthick 10'
'd co2flxa'

'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 1. 4.7  anthro'
'set strsiz 0.07 0.1'
'set string 4 l 6'
'draw string 1. 4.4 ${exp}'
'set strsiz 0.1 0.15'
'set string 1 l 6'
'draw string 7.5 .2 year'

**************V************************
*init2b()
*
*
*'set vrange -2 2'
*'set ylint .5'
***'set ylab %.1f'
*
*'set grid on 0 1'
*'set ccolor 3'
*'set cmark 0'
*'set cthick 4'
*'d co2flxom'
*'set ccolor 4'
*'set cmark 0'
*'set cthick 10'
*'d co2flxo'
*
*'set strsiz 0.1 0.15'
*'set string 1 l 6'
*'draw string 1. 4.7 Ocean '
*'set strsiz 0.07 0.1'
*'set string 4 l 6'
*'draw string 1. 4.4 ${exp}'
*'set strsiz 0.1 0.15'
*'set string 1 l 6'
*'draw string 7.5 .2 year'

'print '
'disable print'
'!gxps -c -i meta -o years_${exp}_co2_3.ps'

'quit'
*--------------------------------------------------
function init2a ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 Global and Annual Means'


*----rahmen setzen----* 
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'

return

*--------------------------------------------------
function init2a2 ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 CO2 Flux [GtC/mo]'
*----rahmen setzen----*
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'
return
*--------------------------------------------------
function init2a3 ()
*--------------------------------------------------
'clear'
*----ueberschrift----*
'set vpage 0. 8.5 0. 11.'
'set strsiz 0.14 0.19'
'set string 1 c 6'
'draw string 4.3 10.6 CO2 Flux [GtC/mo]'

*----rahmen setzen----*
'set vpage 0. 8.5 5. 11.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'
return

*--------------------------------------------------
function init2b ()
*--------------------------------------------------

*----rahmen setzen----* 
'set vpage 0. 8.5 0. 5.'
'set parea 0.75 7.75 0.5 4.5'
'set grads off'

return 
EOF

grads -cbp scen_co2.gs

