#!/bin/ksh
#
# Compute total carbon inventories of the different land carbon pools
#
# The input files are expected to be monthly averages
set -e

infile=$1       # jsbach veg output stream:
                #   code 159  boxC_litter_wood     [mol(C)/m2(grid box)]
                #   code 160  boxC_green           [mol(C)/m2(grid box)]
                #   code 161  boxC_woods           [mol(C)/m2(grid box)]
                #   code 162  boxC_reserve         [mol(C)/m2(grid box)]
                #   code 163  boxC_litter_green_bg [mol(C)/m2(grid box)]
                #   code 164  boxC_slow            [mol(C)/m2(grid box)]
                #   code 176  box_Cpools_total     [mol(C)/m2(grid box)]
                #   code 179  boxC_litter_green_ag [mol(C)/m2(grid box)]
                #   code 218  box_Cpool_onSite_avg_LCC       [mol(C) m-2(grid box)]
                #   code 220  box_Cpool_paper_avg_LCC        [mol(C) m-2(grid_box)]
                #   code 221  box_Cpool_construction_avg_LCC [mol(C) m-2(grid box)]
                #   code 222  box_Cpool_paper_harvest_avg    [mol(C) m-2(grid box)]
                #   code 223  box_Cpool_construction_harvest_avg  [mol(C) m-2(grid box)]
                #   code 224  box_Cpool_onSite_harvest_avg   [mol(C)m-2(grid box)]
                #   code 225  boxC_crop_harvest    [mol(C)m-2(grid box)]
yfile=$2        # jsbach yasso output stream:
                #   code  31  boxYC_acid_ag1       [mol(C) m-2(grid box)]
                #   code  32  boxYC_acid_bg1       [mol(C) m-2(grid box)]
                #   code  33  boxYC_water_ag1      [mol(C) m-2(grid box)]
                #   code  34  boxYC_water_bg1      [mol(C) m-2(grid box)]
                #   code  35  boxYC_ethanol_ag1    [mol(C) m-2(grid box)]
                #   code  36  boxYC_ethanol_bg1    [mol(C) m-2(grid box)]
                #   code  37  boxYC_nonsoluble_ag1 [mol(C) m-2(grid box)]
                #   code  38  boxYC_nonsoluble_bg1 [mol(C) m-2(grid box)]
                #   code  39  boxYC_humus_1        [mol(C) m-2(grid box)]
                #   code  41  boxYC_acid_ag2       [mol(C) m-2(grid box)]
                #   code  42  boxYC_acid_bg2       [mol(C) m-2(grid box)]
                #   code  43  boxYC_water_ag2      [mol(C) m-2(grid box)]
                #   code  44  boxYC_water_bg2      [mol(C) m-2(grid box)]
                #   code  45  boxYC_ethanol_ag2    [mol(C) m-2(grid box)]
                #   code  46  boxYC_ethanol_bg2    [mol(C) m-2(grid box)]
                #   code  47  boxYC_nonsoluble_ag2 [mol(C) m-2(grid box)]
                #   code  48  boxYC_nonsoluble_bg2 [mol(C) m-2(grid box)]
                #   code  49  boxYC_humus_2        [mol(C) m-2(grid box)]
yasso=$3
if [[ $4 != "" ]]; then
  echam_fractional=yes
  olf=$4       # if echam uses fractional land sea mask: fractional ocean land mask
fi

# carbon in vegetation
[[ -f veg.tmp ]] && rm veg.tmp
cdo enssum -vertsum -selcode,160 ${infile} \
           -vertsum -selcode,161 ${infile} \
           -vertsum -selcode,162 ${infile}  veg.tmp

# carbon in litter
[[ -f litter.tmp ]] && rm litter.tmp
if [[ ${yasso} = yes ]]; then
  cdo enssum -vertsum -selcode,31 ${yfile} -vertsum -selcode,41 ${yfile} \
             -vertsum -selcode,33 ${yfile} -vertsum -selcode,43 ${yfile} \
             -vertsum -selcode,35 ${yfile} -vertsum -selcode,45 ${yfile} \
             -vertsum -selcode,37 ${yfile} -vertsum -selcode,47 ${yfile}  litter.tmp

else
  cdo enssum -vertsum -selcode,159 ${infile} \
             -vertsum -selcode,163 ${infile} \
             -vertsum -selcode,179 ${infile}  litter.tmp
fi

# carbon in soil
if [[ ${yasso} = yes ]]; then
  [[ -f soilFast.tmp ]] && rm soilFast.tmp
  cdo enssum -vertsum -selcode,32 ${yfile} -vertsum -selcode,42 ${yfile} \
             -vertsum -selcode,34 ${yfile} -vertsum -selcode,44 ${yfile} \
             -vertsum -selcode,36 ${yfile} -vertsum -selcode,46 ${yfile} \
             -vertsum -selcode,38 ${yfile} -vertsum -selcode,48 ${yfile}  soilFast.tmp
  cdo add    -vertsum -selcode,39 ${yfile} -vertsum -selcode,49 ${yfile}  soilSlow.tmp
  cdo add soilFast.tmp soilSlow.tmp soil.tmp
else
  cdo         vertsum -selcode,164 ${infile}  soil.tmp
fi

# carbon in product pools
#[[ -f product.tmp ]] && rm product.tmp
#cdo enssum -selcode,218 ${infile} \
#           -selcode,220 ${infile} \
#           -selcode,221 ${infile} \
#           -selcode,222 ${infile} \
#           -selcode,223 ${infile} \
#           -selcode,224 ${infile} \
#           -vertsum -selcode,225 ${infile}  product.tmp

# total land carbon
cdo   selcode,176 ${infile}  total.tmp

# Convert C pools from [mol(CO2)/m^2] to [Gt C]
# 1 mol CO2 = 12.0107 g C   => 1 mol CO2 = 1.20107e-14 Gt C
# Global surface area: 5.100656e14 m^2
# => 1 mol CO2 / m^2 (area weighted global average) = 1.20107e-14 * 5.100656e14 Gt C = 6.126245 Gt C

#varlist="veg litter soil product soilFast soilSlow total"
varlist="veg litter soil soilFast soilSlow total"

for f in ${varlist}
do
  if [[ ${echam_fractional} = yes ]]; then
    cdo fldavg -setmisstoc,0 -mul ${olf} $f.tmp z$$
  else
    cdo fldavg -setmisstoc,0 $f.tmp z$$
  fi
  cdo -f ext -b F64 mulc,6.126245 z$$ z$$.ext
  cdo setday,15 z$$.ext $f.ext
  rm $f.tmp z$$ z$$.ext
done

# write ascii output for R

for f in ${varlist}
do
  cdo output $f.ext > $f.asc
  rm $f.ext
done

