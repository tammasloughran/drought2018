#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 12:11:38 2019

@author: tammas
"""
import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt

directory = '/home/tammas/data/ERA5/europe/'
nceramask = nc.Dataset(directory+'lsm_gt0.5_ERA5_europe.nc')
ncjsmask = nc.Dataset(directory+'mask_europe.nc')
eraMask = nceramask.variables['lsm'][:]
jsMask = ncjsmask.variables['slm'][:]
intersection = eraMask - jsMask
plt.pcolormesh(np.flipud(intersection[0,...]), cmap='bwr')
plt.title('Red: ERA5, Blue: JSBACH')
plt.colorbar()
plt.show()
