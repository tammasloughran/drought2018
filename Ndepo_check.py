#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 09:47:33 2019

@author: tammas
"""
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import netCDF4 as nc
import numpy as np


ncinclim = nc.Dataset('/mnt/lustre01/pool/data/JSBACH/input/r0010/T255/T255_ndepo_CMIP_NCAR_CCMI-1-0_gr_185001-185012-clim.nc','r')
ncinfull = nc.Dataset('/mnt/lustre01/pool/data/JSBACH/input/r0010/T255/T255_ndepo_CMIP_NCAR_CCMI-1-0_gr_185001-185012-clim.nc','r')
ncintrendy = nc.Dataset('/mnt/lustre01/pool/data/JSBACH/input/r0009/T63/T63_ndepo_CMIP_NCAR_CCMI-1-0_gr_185001-185012-clim.nc','r')

clim = ncinclim.variables['Ndepo'][:].mean(axis=0)
full = ncinfull.variables['Ndepo'][:].mean(axis=0)
lats = ncinclim.variables['lat'][:]
lons = ncinclim.variables['lon'][:]
xx,yy = np.meshgrid(lons,lats)


proj = Basemap(projection='robin',lon_0=0,resolution='c')
proj.pcolormesh(xx,yy,clim,vmin=0,vmax=5.4e-11,latlon=True)
proj.colorbar()
proj.drawcoastlines()
plt.title('1950 Clim Ndepo @ T255 (kg m-2 s-1)')
plt.show()

#proj = Basemap(projection='robin',lon_0=0,resolution='c')
#proj.pcolormesh(xx,yy,clim-full,latlon=True)
#proj.drawcoastlines()
#proj.colorbar()
#plt.show()

trendy = ncintrendy.variables['Ndepo'][:].mean(axis=0)
lats = ncintrendy.variables['lat'][:]
lons = ncintrendy.variables['lon'][:]
xx,yy = np.meshgrid(lons,lats)
proj.pcolormesh(xx,yy,trendy,vmin=0,vmax=5.4e-11,latlon=True)
proj.colorbar()
proj.drawcoastlines()
plt.title('1950 Clim Ndepo @ T63 (kg m-2 s-1)')
plt.show()