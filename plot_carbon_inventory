#! /bin/sh
#
# Time series of carbon inventory per component
#

# Settings
R=${R:-R} # Override for executable name

# Parameters
if [ "empty$2" = empty ]
then
    exec >&1
    echo "Oops: invalid number of parameters"
    echo "Usage: $(basename $0) experiment_id first_year"
    exit 1
fi

EXP=$1
yr1=$2

# Temporary file setup
SCRIPT=visualize.R
trap "rm -f $SCRIPT ${SCRIPT}out" 0

# Print header comment as user feedback
awk '/^[[:space:]]*#[[:space:]]*[^[:space:]\/!]/ { print; exit }' $0

# Script
cat > $SCRIPT << EOF

pdf(file="invent_${EXP}.pdf", width=7.5, height=9, paper="a4")
run <- 11

ocean <- scan(file='ocean.asc')
sediment <- scan(file='sediment.asc')
cinput <- scan(file='cinput.asc')
atmos <- scan(file='atmos.asc')
land <- scan(file='land.asc')
invent <- scan(file='invent.asc')

if(length(ocean) < (run + 4)*12) run <- 1

xstart=c(${yr1}-1,7)
ocean.ts <- ts(ocean,start=xstart,freq=12)
sediment.ts <- ts(sediment,start=xstart,freq=12)
cinput.ts <- ts(cinput,start=xstart,freq=12)
atmos.ts <- ts(atmos,start=xstart,freq=12)
land.ts <- ts(land,start=xstart,freq=12)
invent.ts <- land.ts + ocean.ts - cinput.ts + sediment.ts + atmos.ts

x <- window(cbind(invent.ts,ocean.ts+sediment.ts-cinput.ts,atmos.ts,land.ts,cinput.ts,ocean.ts,sediment.ts),start=xstart)
colnames(x) <- c("Total","Ocean+Sed-Cinp","Atmosphere","Land","Cinput","Ocean","Sediment")

panel.fun <- function(x,col,bg,pch,cex,lwd,lty,type){
   lines(x,lwd=1); y<-aggregate(x,ndeltat=1,FUN=mean); lines(filter(y,rep(1/run,run),sides=2),col="red",lwd=2)
   }

oldpar <- par(cex.main=1.5,font.main=4)
plot(x,main="Carbon Inventory ${EXP} (Gt C)", panel=panel.fun)
par(oldpar); oldpar <- par(usr=c(0,1,0,1),mar=c(0,0,0,0))
legend(0.5,-0.1,xjust=0.5,col=c("black","red"),lwd=c(1,2),legend=c("monthly",paste(run, "yr running mean")),cex=0.8,bty="n")
par(oldpar)
dev.off()
EOF

$R CMD BATCH $SCRIPT
