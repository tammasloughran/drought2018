# -*- mode: sh -*-
#
# Shell module to load software configuration and request specific programs
#

# Check -u option

set_u=:
unset_u=:
case "$-" in
    *u*) set_u='set -u'; unset_u='set +u';;
esac

# Load profile including module definitions

if ! type module > /dev/null 2>&1
then
    $unset_u
    . $MODULESHOME/init/ksh
    $set_u
    if ! type module > /dev/null 2>&1
    then
        echo "Sorry: cannot find module definitions" >&2
        exit 1
    fi
fi

# Provide shell function to probe a list of modules until a program is found

function use { # <program> <module> [...]
    typeset MODULE
    typeset TOOL=$1
    shift
    PATH=$(echo $PATH | sed 's|/client/bin|__DISABLED__|g;')
    while ! type $TOOL > /dev/null 2>&1 && [ "$1" ]
    do
       MODULE=$1
       shift
       module add $MODULE > /dev/null 2>&1
    done
    PATH=$(echo $PATH | sed 's|__DISABLED__|/client/bin|g;')
    if ! type $TOOL > /dev/null 2>&1
    then
        echo "Sorry: cannot find '$TOOL'" >&2
        exit 1
    fi
}

