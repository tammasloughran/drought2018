#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 16:18:35 2019

@author: tammas
"""

import numpy as np
import xarray as xa
import netCDF4 as nc

gridSpacing = 0.25
domain = ((0,360),(89.75,-90))

lons = np.arange(domain[0][0],domain[0][1],gridSpacing)
lats = np.arange(domain[1][0],domain[1][1],-gridSpacing)

ncinfile = nc.Dataset('/home/tammas/data/ERA5/era5_europe_S2.1_1980.nc')

ncmask = nc.Dataset('mask_europe.nc')
slm = ncmask.variables['slm'][:]
newslm = np.ones((366,)+slm.shape)
for i in range(366):
    newslm[i,...] = slm
np.save('newslm.npy',newslm)

ncout = nc.Dataset('newmask.nc','w')
ncout.createDimension('time')
ncout.createDimension('lat',size=newslm.shape[1])
ncout.createDimension('lon',size=newslm.shape[2])
timeout = ncout.createVariable('time',float,dimensions=('time'))
latout = ncout.createVariable('lat',float,dimensions=('lat'))
lonout = ncout.createVariable('lon',float,dimensions=('lon'))
timeout[:] = ncinfile.variables['time'][:]
latout[:] = ncinfile.variables['lat'][:]
lonout[:] = ncinfile.variables['lon'][:]
lsmout = ncout.createVariable('slm',float,dimensions=('time','lat','lon'))
lsmout[:] = newslm
ncout.close()


