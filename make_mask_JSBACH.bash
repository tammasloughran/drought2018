#!/bin/bash
cd /home/tammas/work-mistral/drought2018/indata
cdo gtc,0.5 -selvar,slm jsbach_0.25TP04_11tiles_5layers_2010_no-dynveg.nc mask.nc
cdo sellonlatbox,-11,65,75,32 mask.nc mask_europe.nc
