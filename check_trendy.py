#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 15:57:28 2019

@author: tammas

Compare carbon pools from two periods (cycles) 
"""


import netCDF4 as nc
import glob
import matplotlib.pyplot as plt
import numpy as np
from cdo import Cdo
cdo = Cdo()

# Model output
#outputDir = '/scratch/m/m300719/drought2018/mpiesm-landveg/experiments/spinup_T255_ERA5/outdata/jsbach/'
outputDir = '/scratch/m/m300719/drought2018/mpiesm-landveg/experiments/TRENDY/outdata/jsbach/'
# You should create a file containg the cell area of the grid
areaFile = 'gridarea.nc'

# Load cell area
ncin2 = nc.Dataset(outputDir+areaFile,'r')
cell_area = ncin2.variables['cell_area'][...] # (lat, lon)
cell_area = cell_area[None,...]
lons = ncin2.variables['lon'][:]
lats = ncin2.variables['lat'][:]
xx,yy = np.meshgrid(lons,lats)
europe = (yy>32)&(yy<75)&((xx>349)|(xx<65))

# List of files for cycle1 and cycle2
cycle_comp = False
if cycle_comp:
    filesCycle1 = glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_320?.nc') \
        + glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_321?.nc')
    filesCycle2 = glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_322?.nc') \
        + glob.glob(outputDir+'spinup_T255_ERA5_jsbach_veg_323?.nc')

    ncin1 = nc.MFDataset(filesCycle1)
    ncin2 = nc.MFDataset(filesCycle2)

    poolVars = ['boxC_green',
            'boxC_woods',
            'boxC_reserve',
            'box_Cpools_total',
            'boxC_crop_harvest']
    for var in poolVars:
        varCycle1 = ncin1.variables[var][0:-1:12,...].mean(axis=0)*cell_area
        varCycle2 = ncin2.variables[var][0:-1:12,...].mean(axis=0)*cell_area
        change = varCycle2.sum() - varCycle1.sum()
        # 12.0111 = molar mass of carbon. 1e15 = grams->petagrams
        print(var+' change: '+str(change*12.0111/1e15) + ' petagrams')
    
allFiles = glob.glob(outputDir+'TRENDY_jsbach_veg_????*.nc')
allFiles.sort()

series2 = []
series2e = []
series3 = []
series3e = []
time = []
var = 'boxC_woods'
var2 = 'box_Cpools_total'
var3 = 'box_NPP_yDayMean'
for file in allFiles[:-1]:
    print(file[-10:])
    ncin = nc.Dataset(file)
    data = ncin.variables[var][0,...]*cell_area
    series = data.sum()
    series2 = np.append(series2,series*12.0111/1e15)
    series = data.sum(axis=0)[europe].sum()
    series2e = np.append(series2e,series*12.0111/1e15)
    data = ncin.variables[var2][0,...]*cell_area
    series = data.sum(axis=0)[europe].sum()
    series3e = np.append(series3e,series*12.0111/1e15)
    series = data.sum()
    series3 = np.append(series3,series*12.0111/1e15)
    date = ncin.variables['time'][0]
    time = np.append(time,nc.num2date(date, ncin.variables['time'].units))
plt.scatter(time,series2e)
plt.title(var)
plt.ylabel('Petagrams C')
plt.show()
plt.figure()
plt.scatter(time,series3e)
plt.title(var2)
plt.ylabel('Petagrams C')
plt.show()

plt.scatter(time,series2)
plt.title(var)
plt.ylabel('Petagrams C')
plt.show()
plt.figure()
plt.scatter(time,series3)
plt.title(var2)
plt.ylabel('Petagrams C')
plt.show()

allFiles = glob.glob(outputDir+'TRENDY_jsbach_nitro_????*.nc')
allFiles.sort()
plt.figure()
series2 = []
time = []
var = 'box_Npools_total'
for file in allFiles[:-1]:
    print(file[-10:])
    ncin = nc.Dataset(file)
    data = ncin.variables[var][0,...]*cell_area
    series = data.sum()
    series2 = np.append(series2,series*14.006747/1e15)
    date = ncin.variables['time'][0]
    time = np.append(time,nc.num2date(date, ncin.variables['time'].units))
plt.scatter(time,series2)
plt.title(var)
plt.ylabel('Petagrams N')
plt.show()

allFiles = glob.glob(outputDir+'TRENDY_jsbach_yasso_????*.nc')
allFiles.sort()
plt.figure()
series2 = []
series3 = []
time = []
var = 'boxYC_humus_2'
var2 = 'boxYC_humus_1'
for file in allFiles[:-1]:
    print(file[-10:])
    ncin = nc.Dataset(file)
    data = ncin.variables[var][0,...]*cell_area
    data2 = ncin.variables[var2][0,...]*cell_area
    series = data.sum()
    series2 = np.append(series2,series*12.0111/1e15)
    series = data2.sum()
    series3 = np.append(series3,series*12.0111/1e15)
    date = ncin.variables['time'][0]
    time = np.append(time,nc.num2date(date, ncin.variables['time'].units))
plt.scatter(time,series2)
plt.title(var)
plt.ylabel('Petagrams C')
plt.show()
plt.figure()
plt.scatter(time,series3)
plt.title(var2)
plt.ylabel('Petagrams C')
plt.show()